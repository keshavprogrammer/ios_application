
//
//  UserModel.swift
//  Kakehashi
//
//  Created by Keshav MacBook Pro on 19/05/18.
//  Copyright © 2018 Kakehashi. All rights reserved.
//

import UIKit

class UserModel: NSObject, NSCoding {
    
    var user_id : String?
    var device_id : String?
    var fullname : String?
    var firstname : String?
    var lastname : String?
    var email : String?
    var password : String?
    var profile_image : String?
    var birthdate : String?
    var gender: String?
    var address: String?
    var zipcode: String?
    var country: String?
    var city: String?
    var user_type: String? //customer,rider
    var login_type: String? //Facebook,Twitter,Email
    
    var cart_id: String?
    
    var number : String?
    var street : String?
    var houser_number : String?
    var state : String?
    var dateofbirth : String?
    var height : String?
    var weight : String?
    var colorofhair : String?
    var clothingsize : String?
    var cupsize : String?
    var chestcircumference: String?
    var waistsize: String?
    var shoesize: String?
    var hipcircumference: String?
    var Additionalinfocomments: String?
    var companyname: String?
    var bankAccount: String?
    var bookingId: String?
    var bookingType: String?
    var type: String?
    var deviceToken: String?
    var age: String?
    
    static var userModel: UserModel?
    static func sharedInstance() -> UserModel {
        if UserModel.userModel == nil {
            if let data = UserDefaults.standard.value(forKey: "UserModel") as? Data {
                let retrievedObject = NSKeyedUnarchiver.unarchiveObject(with: data)
                if let objUserModel = retrievedObject as? UserModel {
                    UserModel.userModel = objUserModel
                    return objUserModel
                }
            }
            
            if UserModel.userModel == nil {
                UserModel.userModel = UserModel.init()
            }
            return UserModel.userModel!
        }
        return UserModel.userModel!
    }
    
    override init() {
        
    }
    
    
    func synchroniseData(){
        let data : Data = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(data, forKey: "UserModel")
        UserDefaults.standard.synchronize()
    }
    
    func removeData() {
        UserModel.userModel = nil
        UserDefaults.standard.removeObject(forKey: "UserModel")
        UserDefaults.standard.synchronize()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        
        self.user_id = aDecoder.decodeObject(forKey: "user_id") as? String
        self.profile_image = aDecoder.decodeObject(forKey: "profile_image") as? String
        self.fullname = aDecoder.decodeObject(forKey: "fullname") as? String
        self.firstname = aDecoder.decodeObject(forKey: "firstname") as? String
        self.lastname = aDecoder.decodeObject(forKey: "lastname") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.password = aDecoder.decodeObject(forKey: "password") as? String
        self.birthdate = aDecoder.decodeObject(forKey: "birthdate") as? String
        self.gender = aDecoder.decodeObject(forKey: "gender") as? String
        self.address = aDecoder.decodeObject(forKey: "address") as? String
        self.zipcode = aDecoder.decodeObject(forKey: "zipcode") as? String
        self.city = aDecoder.decodeObject(forKey: "city") as? String
        self.country = aDecoder.decodeObject(forKey: "country") as? String
        self.user_type = aDecoder.decodeObject(forKey: "user_type") as? String
        self.login_type = aDecoder.decodeObject(forKey: "login_type") as? String
        self.cart_id = aDecoder.decodeObject(forKey: "cart_id") as? String
        self.number = aDecoder.decodeObject(forKey: "number") as? String
        self.street = aDecoder.decodeObject(forKey: "street") as? String
        self.houser_number = aDecoder.decodeObject(forKey: "houser_number") as? String
        self.state = aDecoder.decodeObject(forKey: "state") as? String
        self.dateofbirth = aDecoder.decodeObject(forKey: "dateofbirth") as? String
        self.height = aDecoder.decodeObject(forKey: "height") as? String
        self.weight = aDecoder.decodeObject(forKey: "weight") as? String
        self.colorofhair = aDecoder.decodeObject(forKey: "colorofhair") as? String
        self.clothingsize = aDecoder.decodeObject(forKey: "clothingsize") as? String
        self.cupsize = aDecoder.decodeObject(forKey: "cupsize") as? String
        self.chestcircumference = aDecoder.decodeObject(forKey: "chestcircumference") as? String
        self.waistsize = aDecoder.decodeObject(forKey: "waistsize") as? String
        self.shoesize = aDecoder.decodeObject(forKey: "shoesize") as? String
        self.hipcircumference = aDecoder.decodeObject(forKey: "hipcircumference") as? String
        self.Additionalinfocomments = aDecoder.decodeObject(forKey: "Additionalinfocomments") as? String
        self.companyname = aDecoder.decodeObject(forKey: "companyname") as? String
        self.bankAccount = aDecoder.decodeObject(forKey: "bankAccount") as? String
        self.bookingId = aDecoder.decodeObject(forKey: "bookingId") as? String
        self.bookingType = aDecoder.decodeObject(forKey: "bookingType") as? String
        self.device_id = aDecoder.decodeObject(forKey: "device_id") as? String
        self.type = aDecoder.decodeObject(forKey: "type") as? String
        self.deviceToken = aDecoder.decodeObject(forKey: "deviceToken") as? String
        self.age = aDecoder.decodeObject(forKey: "age") as? String
        
    }
    
    func encode(with aCoder: NSCoder) {

        aCoder.encode(self.user_id, forKey: "user_id")
        aCoder.encode(self.profile_image, forKey: "profile_image")
        aCoder.encode(self.fullname, forKey: "fullname")
        aCoder.encode(self.firstname, forKey: "firstname")
        aCoder.encode(self.lastname, forKey: "lastname")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.password, forKey: "password")
        aCoder.encode(self.birthdate, forKey: "birthdate")
        aCoder.encode(self.gender, forKey: "gender")
        aCoder.encode(self.address, forKey: "address")
        aCoder.encode(self.zipcode, forKey: "zipcode")
        aCoder.encode(self.city, forKey: "city")
        aCoder.encode(self.country, forKey: "country")
        aCoder.encode(self.user_type, forKey: "user_type")
        aCoder.encode(self.login_type, forKey: "login_type")
        aCoder.encode(self.cart_id, forKey: "cart_id")
        aCoder.encode(self.number, forKey: "number")
        aCoder.encode(self.street, forKey: "street")
        aCoder.encode(self.houser_number, forKey: "houser_number")
        aCoder.encode(self.state, forKey: "state")
        aCoder.encode(self.dateofbirth, forKey: "dateofbirth")
        aCoder.encode(self.height, forKey: "height")
        aCoder.encode(self.weight, forKey: "weight")
        aCoder.encode(self.colorofhair, forKey: "colorofhair")
        aCoder.encode(self.clothingsize, forKey: "clothingsize")
        aCoder.encode(self.cupsize, forKey: "cupsize")
        aCoder.encode(self.chestcircumference, forKey: "chestcircumference")
        aCoder.encode(self.waistsize, forKey: "waistsize")
        aCoder.encode(self.shoesize, forKey: "shoesize")
        aCoder.encode(self.hipcircumference, forKey: "hipcircumference")
        aCoder.encode(self.Additionalinfocomments, forKey: "Additionalinfocomments")
        aCoder.encode(self.companyname, forKey: "companyname")
        aCoder.encode(self.bankAccount, forKey: "bankAccount")
        aCoder.encode(self.bookingId, forKey: "bookingId")
        aCoder.encode(self.bookingType, forKey: "bookingType")
        aCoder.encode(self.device_id, forKey: "device_id")
        aCoder.encode(self.type, forKey: "type")
        aCoder.encode(self.deviceToken, forKey: "deviceToken")
        aCoder.encode(self.age, forKey: "age")
        
    }
}
