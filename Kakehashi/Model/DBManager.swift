//
//  DBManager.swift
//  Kakehashi
//
//  Created by Keshav MacBook Pro on 19/05/18.
//  Copyright © 2018 Kakehashi. All rights reserved.
//

import UIKit
import FMDB

struct ConstantDB {
    struct Path {
        static let Documents = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
         static let kDB_NAME = "Kakehashi.sqlite"
        static let kSHARED_INSTANCE = DBManager.sharedDbManager()
    }
   
    static let kDB_PATH = Path.Documents.appending("/\(Path.kDB_NAME)")
    static let kDATABASE = Path.kSHARED_INSTANCE.database
    
    static let field_AddDate = "add_date"
    
    //Recent search DB
    static let field_ID = "id"
    static let field_Title = "title"
    static let field_SubTitle = "subtitle"
    static let field_No_of_song = "no_of_song"
    static let field_Type = "type"
    static let field_Date = "date_and_time"
    static let field_Image = "image"
}

class DBManager: NSObject {
    
    var database: FMDatabase!
    
    class func sharedDbManager() -> DBManager {
        var sharedDbManager: DBManager?
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            if sharedDbManager == nil {
                sharedDbManager = DBManager()
            }
        }
        return sharedDbManager!
    }
    
    override init() {
        super.init()
        
        if !(database != nil) {
            database = FMDatabase(path: ConstantDB.kDB_PATH)
        }
        
        copyDatabaseToPath()
        openDb()
    }
    
    func copyDatabaseToPath() {
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: ConstantDB.kDB_PATH) {
            let fromPath: String = URL(fileURLWithPath: (Bundle.main.resourcePath)!).appendingPathComponent(ConstantDB.Path.kDB_NAME).absoluteString
            try? fileManager.copyItem(atPath: fromPath, toPath: ConstantDB.kDB_PATH)
        }
    }
    
    func openDb() {
        if !database.open() {
            print("Could not open db.")
        }
    }
}
