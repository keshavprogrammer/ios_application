//
//  constant.swift
//  Kakehashi
//
//  Created by Keshav MacBook Pro on 19/05/18.
//  Copyright © 2018 Kakehashi. All rights reserved.
//

import Foundation
import UIKit

var isAccpetRequest = false

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 580.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}


class Constant
{
    let defaults:UserDefaults = UserDefaults.standard
    
    let screenSize: CGRect = UIScreen.main.bounds
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    static var googleID=""
    static var SegoeFont=""
    static var web_url="http://keshavinfotechdemo.com/KESHAV/KG2/mladen/webservices/"
    static var web_url_content=""
    static var web_url_common=""
    static var PROJECT_NAME = "Kakehashi" as NSString
    
    static func showHUD(view:UIView)
    {
        MBProgressHUD.showAdded(to: view, animated: true)
        //KVNProgress.show()
    }
    
    static func hideHUD(view:UIView)
    {
        MBProgressHUD.hideAllHUDs(for: view, animated: true)
        MBProgressHUD.hide(for: view, animated: true)
        //KVNProgress.dismiss()
    }
}


