//
//  commonfunction.swift
//  Anaqa
//
//  Created by Keshav MacBook Pro on 19/05/18.
//  Copyright © 2018 Kakehashi. All rights reserved.
//

import Foundation
import UIKit
import Toaster

// MARK: - UITextField extension

public enum PaddingType {
    case leftSide
    case rightSide
}

extension UITextField: UITextFieldDelegate {

    public func setPaddingImage(_ image1: UIImage, SetPaddingType:PaddingType) {

        switch SetPaddingType {

        case .leftSide:

            textAlignment = .left
            let imageView = UIImageView(image: image1)
            imageView.contentMode = .center
            self.leftView = imageView
            self.leftView?.frame.size = CGSize(width: 25 + 15 , height: 18)
            self.leftViewMode = UITextFieldViewMode.always

            break

        case .rightSide:

            textAlignment = .left
            let imageView = UIImageView(image: image1)
            imageView.contentMode = .center
            self.rightView = imageView
            self.rightView?.frame.size = CGSize(width: 25 + 15 , height: 18)
            self.rightViewMode = UITextFieldViewMode.always

            break
        }
    }
}

extension UIImage {
    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}

public extension UISearchBar {
    public func setStyleColor(_ color: UIColor) {
        tintColor = color
        guard let tf = (value(forKey: "searchField") as? UITextField) else { return }
        tf.textColor = color
        tf.layer.borderWidth = 1.0
        tf.layer.borderColor = color.cgColor
        
        DispatchQueue.main.async {
            tf.layer.cornerRadius = tf.frame.size.height/2
        }
        
        if let glassIconView = tf.leftView as? UIImageView, let img = glassIconView.image {
//            let newImg = img.blendedByColor(color)
//            glassIconView.image = newImg
        }
        if let clearButton = tf.value(forKey: "clearButton") as? UIButton {
            clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
            clearButton.tintColor = color
        }
    }
}

extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

extension String {

    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }

    var removeFirstChar : String {
        mutating get {
            self.remove(at: self.startIndex)
            return self
        }
    }

    var URLEncoded:String {
        let unreservedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~"
        let unreservedCharset = NSCharacterSet(charactersIn: unreservedChars) as CharacterSet
        let encodedString = self.addingPercentEncoding(withAllowedCharacters: unreservedCharset)
        return encodedString ?? self
    }

    func convertDateFormater(date: String,inputDateFormate: String , outputDateFormate: String) -> String
    {
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        //dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        dateFormatter.dateFormat = inputDateFormate


        let date = dateFormatter.date(from: date)

        //dateFormatter.dateFormat = "yyyy MMM EEEE HH:mm"
        //dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        dateFormatter.dateFormat = outputDateFormate
        var timeStamp = ""
        if (date != nil)
        {
            timeStamp = dateFormatter.string(from: date!)
        }
        return timeStamp
    }

    func getCurrentTime() -> String{

        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)

        let hour = components.hour
        let minute = components.minute
        let second = components.second

        let today_string = String(hour!)  + ":" + String(minute!) + ":" +  String(second!)

        return today_string

    }
}

extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Avenir-Light", size: 18)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
    }
    
    func restore() {
        self.backgroundView = nil
    }
}

extension UICollectionView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Avenir-Light", size: 18)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel;
    }

    func restore() {
        self.backgroundView = nil
    }
}

extension UIViewController
{
    func showToastMessage(_ message: String) {
        
        ToastCenter.default.cancelAll()
        Toast(text: message, duration: Delay.long).show()
    }
}

extension String {
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}

extension Date {
    
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
}

class CommonFunction
{
//    func setButtonWidth(Button : UIButton) -> UIButton{
//        
//        Button.contentHorizontalAlignment = .center;
//        //        let labelString = NSString(string: Button.titleLabel!.text!)
//        //        let titleSize = labelString.size(attributes: [NSFontAttributeName: Button.titleLabel!.font])
//        //        Button.imageEdgeInsets = UIEdgeInsetsMake(0, titleSize.width , 0,0)
//        
//        return Button
//    }
//    

    func checkIsEmpty( s:String)->Bool
    {
        var s = s
        if(s.isEmpty)
        {
            return true
        }
        
        
        if (s.isEmpty)
        {
            s = ""
        }
        else if(s == "<nil>")
        {
            s = ""
        }
        
        return s == "" ? true :false
    }

    func isValidPassword(testStr:String) -> Bool {
        let passwordRegex = "[A-Za-z0-9-!@#$%&*ˆ+=_]{6,30}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: testStr)
    }    

}


