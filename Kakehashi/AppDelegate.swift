//
//  AppDelegate.swift
//  Kakehashi
//
//  Created by Keshav on 16/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import IQKeyboardManager
import FBSDKCoreKit
import FBSDKLoginKit
import TwitterKit

extension UIScrollView
{
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        super.touchesBegan(touches, with: event)
        self.superview!.touchesBegan(touches, with: event)
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var navigationController : UINavigationController?
    var window: UIWindow?


    //MARK:- sidemenu set up methods
    func sidemenu() -> MFSideMenu
    {
        self.navigationController = NavigationController()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC1 = storyboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        let sidemenu = MFSideMenu .init(navigationController: navigationController, leftSideMenuController: VC1, rightSideMenuController:nil, panMode:MFSideMenuPanModeSideMenu)
        VC1.leftSide = sidemenu
        
        return sidemenu!
    }
    
    func NavigationController() -> UINavigationController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if UserModel.sharedInstance().user_id != nil
        {
            if UserModel.sharedInstance().user_type! == "provider"
            {
                let VC2 = storyboard.instantiateViewController(withIdentifier: "AlbumlistViewController") as! AlbumlistViewController
                navigationController = UINavigationController .init(rootViewController: VC2)
            }
            else
            {
                let VC2 = storyboard.instantiateViewController(withIdentifier: "YoutubeWithLabelExampleViewController") as! YoutubeWithLabelExampleViewController
                navigationController = UINavigationController .init(rootViewController: VC2)
            }
        }
        else
        {
            let VC2 = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            navigationController = UINavigationController .init(rootViewController: VC2)
        }
        //self.window?.rootViewController = navigationController
        return navigationController!
    }
    
    func setSlideRootNavigation()
    {
        self.window?.rootViewController = sidemenu().navigationController
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        IQKeyboardManager.shared().isEnabled = true

        IQKeyboardManager.shared().isEnableAutoToolbar = false

        TWTRTwitter.sharedInstance().start(withConsumerKey: "HgkzGYttnraP554CmeukDsCZY", consumerSecret: "pgbheILeNbP6NXr5AjBGaGjqCzWc8axgZ6ngu0nxHXV34UX2jV")
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        setSlideRootNavigation()

        //Create Recetn Table
        let createRecetnTableQuery = "create table recentSearchData (\(ConstantDB.field_ID) integer primary key autoincrement not null, \(ConstantDB.field_Title) text , \(ConstantDB.field_SubTitle) text , \(ConstantDB.field_No_of_song) text , \(ConstantDB.field_Type) text , \(ConstantDB.field_Date) text , \(ConstantDB.field_Image) text)"
        do {
            try ConstantDB.kDATABASE?.executeUpdate(createRecetnTableQuery, values: nil)
        }
        catch {
            print("Could not create article table.")
            print(error.localizedDescription)
        }

        print(ConstantDB.kDB_PATH)

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if TWTRTwitter.sharedInstance().application(app, open:url, options: options) {
            return true
        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }

}

