//
//  WebserviceHandler.swift
//  Daisy
//
//  Created by Keshav on 28/02/18.
//  Copyright © 2018 keshav. All rights reserved.
//

import UIKit
import Alamofire

class WebserviceHandler: NSObject {

    // MARK: - Singleton Declaration
    class var sharedInstance : WebserviceHandler
    {
        struct Singleton
        {
            static let instance = WebserviceHandler ()
        }
        return Singleton .instance
    }
    
    // MARK: - Service Root URL
    static var serviceRootURL="http://keshavinfotechdemo.com/KESHAV/KG2/mladen/webservices/"
    
    // MARK: - Internet alert message
    static var alertInternetMessage   = "Your device is not connected to the internet..."
    
    class func callWebService (viewcontroller : UIViewController, parameters : String, onCompletion : @escaping (_ serviceResponse : [[String:AnyObject]]) -> Void, failure: @escaping (Error?) -> Void)
    {
        if CheckReachability.isConnectedToNetwork() == true
        {
            MBProgressHUD.showAdded(to: viewcontroller.view, animated: true)
            let URLString = "\(serviceRootURL)\(parameters)" as String
            Alamofire.request(URLString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    print("Error when calling service")
                    print(response.result.error!)
                    failure(response.result.error)
                    MBProgressHUD.hide(for:viewcontroller.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    failure(response.result.error)
                    MBProgressHUD.hide(for:viewcontroller.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        //SharedInstance.alertViewController(message: strMessage, inViewController: viewcontroller)

                        MBProgressHUD.hideAllHUDs(for: viewcontroller.view, animated: true)
                    }
                    else
                    {
                        let responseData = json["responseData"] as! [[String:AnyObject]]
                        print("Response Data : \(responseData)")
                        onCompletion(responseData)
                    }
                    
                    MBProgressHUD.hideAllHUDs(for: viewcontroller.view, animated: true)
                }
            }
        }
        else
        {
            MBProgressHUD.hide(for:viewcontroller.view, animated: true)
            SharedInstance.alertViewController(message: alertInternetMessage, inViewController: viewcontroller)
        }
    }
    
    class func callWebServiceWithImage (viewcontroller : UIViewController, parameters : [String : AnyObject], image : UIImage, onCompletion : @escaping (_ serviceResponse : [String:AnyObject]) -> Void, failure: @escaping (Error?) -> Void)
    {
        if CheckReachability.isConnectedToNetwork() == true
        {
            MBProgressHUD.showAdded(to: viewcontroller.view, animated: true)
            let imgData = UIImageJPEGRepresentation(image, 1)

            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData!, withName: "image_path",fileName: "image_post.png", mimeType: "image/png")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
                             to:"\(serviceRootURL)/submit_image.php")
            { (result) in
                switch result {
                    
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        
                        guard let json = response.result.value as? [String: Any] else {
                            print("didn't get todo object as JSON from API")
                            print("Error: \(String(describing: response.result.error))")
                            failure(response.result.error)
                            return
                        }
                        
                        if let strResult = json["status"] as? String
                        {
                            if strResult == "0"
                            {
                                print(strResult)
                                let strMessage = json["message"] as! String
                                SharedInstance.alertViewController(message: strMessage, inViewController: viewcontroller)
                            }
                            else
                            {
                                let responseData = json["responseData"] as! [String:AnyObject]
                                print("Response Data : \(responseData)")
                                onCompletion(responseData)
                            }
                            
                            MBProgressHUD.hide(for:viewcontroller.view, animated: true)
                        }
                    }
                case .failure(_):
                    
                    MBProgressHUD.hide(for:viewcontroller.view, animated: true)
                }
            }
        }
        else
        {
            MBProgressHUD.hide(for:viewcontroller.view, animated: true)
            SharedInstance.alertViewController(message: alertInternetMessage, inViewController: viewcontroller)
        }
    }
}
