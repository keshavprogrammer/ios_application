//
//  SharedInstance.swift
//  Daisy
//
//  Created by Keshav on 28/02/18.
//  Copyright © 2018 keshav. All rights reserved.
//

import UIKit

enum dateFormat: String
{
    case type1 = "EEEE,MMM,d,YYYY"
    case type2 = "MM/dd/YYYY"
    case type3 = "MM-dd-YYYY"
    case type4 = "MM-dd-YYYY HH:mm"
    case type5 = "MMM d, YYYY"
    case type6 = "YYYY/MM/dd"
    case type7 = "YYYY-MM-dd"
    case type8 = "YYYY-MM-dd HH:mm:ss"
    case type9 = "dd MMMM,YYYY"
    case type10 = "dd/MM/YYYY"
}

class SharedInstance: NSObject {
    
    // MARK: - Singleton Declaration
    class var sharedInstance : SharedInstance
    {
        struct Singleton
        {
            static let instance = SharedInstance ()
        }
        return Singleton .instance
    }

    // MARK: - AlertView
    class func alertViewController(message : String , inViewController : UIViewController)
    {
        let alert = UIAlertController(title: "Kakehashi", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction (title: "OK", style: UIAlertActionStyle.cancel, handler: { action -> Void in
            //Just dismiss the action sheet
        })
        
        alert.addAction(okAction)
        
        inViewController.present(alert, animated: true, completion: nil)

    }
    
    class func alertview(message : String)
    {
        let alert = UIAlertView()
        alert.title = NSLocalizedString("Alert", comment: "")
        alert.message = message as String
        alert.addButton(withTitle: NSLocalizedString("ok", comment: ""))
        alert.show()
    }
    
    // MARK: - Methods
    class func paddingView(textFieldArray : NSArray)
    {
        for case let textField as UITextField in textFieldArray
        {
            let frame = CGRect(x: 0, y: 0, width: 15, height: textField.frame.size.height)
            let paddingView = UIView(frame: frame)
            textField.leftView = paddingView
            textField.leftViewMode = UITextFieldViewMode.always
        }
    }
    
    // MARK: - Check valid Email
    class func isValidEmail(withemail: String) -> Bool
    {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: withemail)
    }
    
    // MARK: - AppDelegate Reference
    class func appDelegate () -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    // MARK: - getDate From String
    class func getDateFromString(dateString : String) -> Date
    {
        
        //let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "MMM yyyy"
        //return  dateFormatter.date(from: dateString)!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        //dateFormatter.calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.ISO8601) as Calendar!
        //dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale!
        
        // dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.locale = Locale(identifier: "en")
        return  (dateFormatter.date(from: dateString))!
        
    }
    
    class func setNavigation(title: String, backButton: String, rightButtons: [String], barcolor: UIColor, controller: UIViewController, returnLeftButton : @escaping (_ leftNavigationButton : UIButton) -> Void,  returnRightButtons : @escaping (_ rightNavigationButtons : [UIButton]) -> Void)
    {
        controller.navigationController?.navigationBar.isTranslucent = false
        controller.navigationItem.hidesBackButton = true
        controller.navigationController?.isNavigationBarHidden=false
        controller.navigationController?.navigationBar.barTintColor = barcolor
        
        
        if title.hasSuffix("jpg") || title.hasSuffix("png")
        {
            print("sent image")
        }
        else
        {
            print("sent text")
            let lblHeader = UILabel(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
            lblHeader.textColor = UIColor.white
            lblHeader.text = title
            controller.navigationItem.titleView = lblHeader
        }
        
        if backButton.hasSuffix("jpg") || backButton.hasSuffix("png")
        {
            print("sent image")
            let leftBtn = UIButton()
            leftBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 18)
            leftBtn.backgroundColor = UIColor.clear
            leftBtn.setImage(UIImage(named: backButton) as UIImage?, for: [UIControlState.normal])
            
            let leftBarBtn = UIBarButtonItem()
            leftBarBtn.customView = leftBtn
            
            controller.navigationItem.leftBarButtonItems = [leftBarBtn]
            
            returnLeftButton(leftBtn)
        }
        else
        {
            print("sent text")
            let leftBtn = UIButton()
            leftBtn.frame = CGRect(x: 0, y: 0, width: 50, height: 18)
            leftBtn.backgroundColor = UIColor.clear
            leftBtn.setTitle(backButton, for: .normal)
            //leftBtn.setImage(UIImage(named: "leftMenu") as UIImage?, for: [UIControlState.normal])
            
            let leftBarBtn = UIBarButtonItem()
            leftBarBtn.customView = leftBtn
            controller.navigationItem.leftBarButtonItems = [leftBarBtn]
            
            returnLeftButton(leftBtn)
        }
        
        let NoOfRightButton = rightButtons.count
        
        if NoOfRightButton == 1
        {
            if rightButtons[0].hasSuffix("jpg") || rightButtons[0].hasSuffix("png")
            {
                print("sent image")
                let rightBtn = UIButton()
                rightBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 18)
                rightBtn.backgroundColor = UIColor.clear
                rightBtn.setImage(UIImage(named: rightButtons[0]) as UIImage?, for: [UIControlState.normal])
                
                let rightBarBtn = UIBarButtonItem()
                rightBarBtn.customView = rightBtn
                
                controller.navigationItem.rightBarButtonItems = [rightBarBtn]
                
                returnRightButtons([rightBtn])
            }
            else
            {
                print("sent text")
                let rightBtn = UIButton()
                rightBtn.frame = CGRect(x: 0, y: 0, width: 50, height: 18)
                rightBtn.backgroundColor = UIColor.clear
                rightBtn.setTitle(rightButtons[0], for: .normal)
                //rightBtn.setImage(UIImage(named: "leftMenu") as UIImage?, for: [UIControlState.normal])
                
                let rightBarBtn = UIBarButtonItem()
                rightBarBtn.customView = rightBtn
                controller.navigationItem.rightBarButtonItems = [rightBarBtn]
                
                returnRightButtons([rightBtn])
            }
        }
        else if NoOfRightButton == 2
        {
            let rightBtn1 = UIButton()
            let rightBtn2 = UIButton()
            
            let rightBarBtn1 = UIBarButtonItem()
            let rightBarBtn2 = UIBarButtonItem()
            
            if rightButtons[0].hasSuffix("jpg") || rightButtons[0].hasSuffix("png")
            {
                print("sent image")
                
                rightBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 18)
                rightBtn1.backgroundColor = UIColor.clear
                rightBtn1.setImage(UIImage(named: rightButtons[0]) as UIImage?, for: [UIControlState.normal])
                
                rightBarBtn1.customView = rightBtn1
                
            }
            else
            {
                print("sent text")
                rightBtn1.frame = CGRect(x: 0, y: 0, width: 50, height: 18)
                rightBtn1.backgroundColor = UIColor.clear
                rightBtn1.setTitle(rightButtons[0], for: .normal)
                //rightBtn.setImage(UIImage(named: "leftMenu") as UIImage?, for: [UIControlState.normal])
                
                rightBarBtn1.customView = rightBtn1
                
            }
            
            if rightButtons[1].hasSuffix("jpg") || rightButtons[1].hasSuffix("png")
            {
                print("sent image")
                
                rightBtn2.frame = CGRect(x: 0, y: 0, width: 30, height: 18)
                rightBtn2.backgroundColor = UIColor.clear
                rightBtn2.setImage(UIImage(named: rightButtons[1]) as UIImage?, for: [UIControlState.normal])
                
                rightBarBtn2.customView = rightBtn2
                
            }
            else
            {
                print("sent text")
                rightBtn2.frame = CGRect(x: 0, y: 0, width: 50, height: 18)
                rightBtn2.backgroundColor = UIColor.clear
                rightBtn2.setTitle(rightButtons[1], for: .normal)
                //rightBtn.setImage(UIImage(named: "leftMenu") as UIImage?, for: [UIControlState.normal])
                
                rightBarBtn2.customView = rightBtn1
    
            }
            
            controller.navigationItem.rightBarButtonItems = [rightBarBtn1,rightBarBtn2]
            returnRightButtons([rightBtn1,rightBtn2])
        }
    }

    class func ShowStringPicker(id : AnyObject, Title : String, rows : NSArray, initialSelection : NSInteger, onCompletion : @escaping (_ indexNumber : NSInteger, _ indexValue : String ) -> Void)
    {
        ActionSheetStringPicker.show(withTitle: Title, rows: rows as! [Any], initialSelection: 0, doneBlock: { picker, indexes, values in
            onCompletion(indexes,values as! String)
            return
        }, cancel: { ActionStringCancelBlock in

            return
        }, origin: id)
    }

    class func showDatePicker(format : dateFormat, target : AnyObject, title : String, defaultDate : Date, maximumDate : Date, onCompletion :  @escaping (_ indexValue : String) -> Void)
    {
        let date_picker = ActionSheetDatePicker(title: title, datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: { icker, value, index in
            
//        let date_picker = ActionSheetDatePicker.show(withTitle: title, datePickerMode: .date, selectedDate: defaultDate, doneBlock: { picker, value, index in
            
            //picker?.maximumDate = maximumDate

            let dateFormatter = DateFormatter()
            print(format.rawValue)
            dateFormatter.dateFormat = format.rawValue
            let str = dateFormatter.string(from: value!)
            onCompletion(str)
            //            print("value = \(value)")
            //            print("index = \(index)")
            //            print("picker = \(picker)")
            return
            
        }, cancel: {
            ActionStringCancelBlock in return
            
        }, origin: target as! UIView)
        
        date_picker?.maximumDate = maximumDate
        date_picker?.show()
    }
}

// MARK: - Device Chart Properties
extension SharedInstance
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(SCREEN_WIDTH, SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(SCREEN_WIDTH, SCREEN_HEIGHT)
    static let IPHONE_4  = UIDevice.current.userInterfaceIdiom == .phone && SCREEN_MAX_LENGTH < 568.0
    static let IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && SCREEN_MAX_LENGTH == 568.0
    static let IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && SCREEN_MAX_LENGTH == 667.0
    static let IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && SCREEN_MAX_LENGTH == 736.0
    static let IPAD              = UIDevice.current.userInterfaceIdiom == .pad && SCREEN_MAX_LENGTH == 1024.0
    static let IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && SCREEN_MAX_LENGTH == 1366.0
    
}
