//
//  ImagePicker.swift
//  ApplicationLibrary
//
//  Created by Keshav on 07/04/18.
//  Copyright © 2018 keshav. All rights reserved.
//

import UIKit

class ImagePicker: NSObject {

    fileprivate var imgView: UIImageView!
    static let shared = ImagePicker()
    
    var pickedImage: ((UIImage) -> Void)?
    
    func showImagePicker(viewController1: UIViewController, imageView: UIImageView)
    {
        imgView = imageView
        let optionMenu = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        
        if let action = self.pickerControllerActionFor(for: .camera, title: "Take photo", viewController: viewController1) {
            optionMenu.addAction(action)
        }
        if let action = self.pickerControllerActionFor(for: .photoLibrary, title: "Select from library", viewController: viewController1) {
            optionMenu.addAction(action)
        }
        
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverPresentationController = optionMenu.popoverPresentationController {
            popoverPresentationController.sourceView = viewController1.view
            popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirection()
            popoverPresentationController.sourceRect = CGRect(x: viewController1.view.bounds.midX, y: viewController1.view.bounds.midY, width: 0, height: 0)
        }
        viewController1.present(optionMenu, animated: true)
    }
    
    func pickerControllerActionFor(for type: UIImagePickerControllerSourceType, title: String, viewController: UIViewController) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            let pickerController           = UIImagePickerController()
            pickerController.delegate      = ImagePicker.shared
            pickerController.sourceType    = type
            pickerController.allowsEditing = true
            viewController.present(pickerController, animated: true)
        }
    }
    
//    func picker(picker: UIImagePickerController, selectedImage data: Data?) {
//        picker.dismiss(animated: true, completion: nil)
//
//        self.delegate?.selectedImage(imgdata: data)
//        self.delegate = nil
//        self.dispose()
//    }
    
    class func selectedImage(_onCompletion: @escaping(_ selectedImage : Data) -> Void){
        
//        let optionMenu = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
//
//        if let action = self.pickerControllerActionFor(for: .camera, title: "Take photo") {
//            optionMenu.addAction(action)
//        }
//        if let action = self.pickerControllerActionFor(for: .photoLibrary, title: "Select from library") {
//            optionMenu.addAction(action)
//        }
//
//        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//
//        if let popoverPresentationController = optionMenu.popoverPresentationController {
//            popoverPresentationController.sourceView = self.view
//            popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirection()
//            popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//        }
//        self.present(optionMenu, animated: true)
        
    }
}

extension ImagePicker: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        //self.picker(picker: picker, selectedImage: nil)
        picker.dismiss(animated: true, completion: nil)
        print("imagePickerControllerDidCancel")
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        //var imgData = UIImage()
        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            imgView.image = img
            //pickedImage?(img)
        }
        
        print("didFinishPickingMediaWithInfo")
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        
        picker.dismiss(animated: true, completion: nil)
        
        pickedImage?(image)
        
        print("didFinishPickingImage")
    }
}
