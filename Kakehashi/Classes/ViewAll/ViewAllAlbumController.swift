//
//  PlaylistViewController.swift
//  Kakehashi
//
//  Created by keshav on 5/21/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire

class ViewAllAlbumlistCell: UICollectionViewCell {

    @IBOutlet var lblSubTitle: UILabel!
    @IBOutlet weak var ImgThumb: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNo_of_song: UILabel!
}

class ViewAllAlbumController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var PopupFilterVw: UIView!
    @IBOutlet weak var btnChangeType: UIButton!
    @IBOutlet var tblAll: UITableView!
    @IBOutlet weak var AllAlbumCollectionView: UICollectionView!
    @IBOutlet weak var lblScreenTitle: UILabel!

    let columnLayout = ViewAllColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )

    let reuseIdentifier = "ViewAllAlbumCell"
    var viewType = String()
    var sortType = String()
    var arrAlbumList = Array<Dictionary<String, AnyObject>>()
    var startcounter = Int()
    
    //MARK:- view lifecycle method
    override func viewDidLoad() {
        super.viewDidLoad()

        if viewType == "new"
        {

            lblScreenTitle.text = "New Releases"
        }
        else{

            lblScreenTitle.text = "Most Popular"
        }

        tblAll.isHidden = true
        btnChangeType.setImage(#imageLiteral(resourceName: "listView"), for: .normal)
        tblAll.register(UINib(nibName: "ListDetailCell", bundle: nil), forCellReuseIdentifier: "listDetail")
        tblAll.tableFooterView = UIView()
        
        startcounter = 0
        sortType = ""
        self.GetAlbumlist(type: viewType, start: startcounter, sortType: sortType)
        
        let weakSelf:ViewAllAlbumController = self
        AllAlbumCollectionView.addInfiniteScrolling(actionHandler: {() -> Void in
            weakSelf.insertRowAtBottomOfSearch()
        })
        tblAll.addInfiniteScrolling(actionHandler: {() -> Void in
            weakSelf.insertRowAtBottomOfSearch()
        })

        if #available(iOS 11.0, *) {

            AllAlbumCollectionView.collectionViewLayout = columnLayout
            AllAlbumCollectionView.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
        }
        
        self.btnChangeType.layer.cornerRadius = self.btnChangeType.frame.size.height/2
        self.btnChangeType.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- pagination methods
    func insertRowAtBottomOfSearch()
    {
        startcounter += 20
        
        self.GetAlbumlist(type: viewType, start: startcounter, sortType: sortType)
        self.AllAlbumCollectionView.infiniteScrollingView.stopAnimating()
        self.tblAll.infiniteScrollingView.stopAnimating()
    }
    
    //MARK:- UIButton method
    @IBAction func btnChangeViewTap(_ sender: Any) {
        
        if tblAll.isHidden
        {
            tblAll.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
            AllAlbumCollectionView.isHidden = true
            tblAll.isHidden = false
            btnChangeType.setImage(#imageLiteral(resourceName: "gridView"), for: .normal)
        }
        else
        {
            AllAlbumCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
            AllAlbumCollectionView.isHidden = false
            tblAll.isHidden = true
            btnChangeType.setImage(#imageLiteral(resourceName: "listView"), for: .normal)
        }
    }
    
    @IBAction func BtnFilterTap(_ sender: Any) {
        
        if PopupFilterVw.isHidden
        {
            self.imgBlur.isHidden = false
            PopupFilterVw.isHidden = false
        }
        else{
            
            self.imgBlur.isHidden = true
            PopupFilterVw.isHidden = true
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAtoZTap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        sortType = "atoz"
        startcounter = 0
        self.GetAlbumlist(type: viewType, start: startcounter, sortType: sortType)
    }
    
    @IBAction func btnZtoATap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        sortType = "ztoa"
        startcounter = 0
        self.GetAlbumlist(type: viewType, start: startcounter, sortType: sortType)
    }

    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if (self.arrAlbumList.count == 0) {
            collectionView.setEmptyMessage("")
        } else {
            collectionView.restore()
        }
        return self.arrAlbumList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = AllAlbumCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ViewAllAlbumlistCell

        let thumImgUrl = arrAlbumList[indexPath.row]["album_image"] as! String

        cell.ImgThumb.sd_setImage(with: NSURL(string: thumImgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)

        let no_of_song = arrAlbumList[indexPath.row]["no_of_songs"] as! Int

        if no_of_song == 0
        {
            cell.lblNo_of_song.text = "0 Video"
        }
        else if no_of_song == 1
        {
            cell.lblNo_of_song.text = "\(no_of_song) Video"
        }
        else
        {
            cell.lblNo_of_song.text = "\(no_of_song) Videos"
        }

        cell.lblSubTitle.text = "by \(arrAlbumList[indexPath.row]["artist_name"]!)"
        cell.lblTitle.text = arrAlbumList[indexPath.row]["album_name"] as? String

        return cell
    }

    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        print("You selected cell #\(indexPath.item)!")

        self.performSegue(withIdentifier: "videoShowfromAll", sender: indexPath)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if (segue.identifier == "videoShowfromAll") {

            let controller =  segue.destination as! VideoViewController
            let row = (sender as! NSIndexPath).row
            controller.AlbumId = (arrAlbumList[row]["album_id"] as? String)!
            controller.screenType = ""
            controller.screen_title = (arrAlbumList[row]["album_name"] as? String)!
        }
    }

    // MARK: - webservice called
    func GetAlbumlist(type:String, start:Int, sortType:String){
        
        if start == 0
        {
            self.arrAlbumList.removeAll()
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        
        let url = "albums_list.php?type=\(type)&start=\(start)&sort=\(sortType)"

        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET login.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        if start == 0
                        {
                            SharedInstance.alertViewController(message: strMessage, inViewController: self)
                            
                            self.arrAlbumList.removeAll()
                        }
                        
                    }
                    else
                    {
                        let responseData = json["responseData"] as! [[String:AnyObject]]

                        for i in 0..<responseData.count
                        {
                            self.arrAlbumList.append(responseData[i])
                        }
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                    }

                    self.AllAlbumCollectionView.reloadData()
                    self.tblAll.reloadData()

                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }
}

class ViewAllColumnFlowLayout: UICollectionViewFlowLayout {

    let cellsPerRow: Int

    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()

        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepare() {
        super.prepare()

        guard let collectionView = collectionView else { return }

        if #available(iOS 11.0, *) {
            let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)

            let _: CGFloat = collectionView.superview!.frame.size.height

            itemSize = CGSize(width: itemWidth, height: itemWidth*0.6+120)
        } else {
            // Fallback on earlier versions
        }
    }

    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }
}

extension ViewAllAlbumController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAlbumList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblAll.dequeueReusableCell(withIdentifier: "listDetail", for: indexPath) as! ListDetailCell
        
        let thumImgUrl = arrAlbumList[indexPath.row]["album_image"] as! String
        
        cell.imgThumb.sd_setImage(with: NSURL(string: thumImgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)
        
        let no_of_song = arrAlbumList[indexPath.row]["no_of_songs"] as! Int
        
        if no_of_song == 0
        {
            cell.lblNoSong.text = "0 Video"
        }
        else if no_of_song == 1
        {
            cell.lblNoSong.text = "\(no_of_song) Video"
        }
        else
        {
            cell.lblNoSong.text = "\(no_of_song) Videos"
        }
        
        cell.lblTitle.text = arrAlbumList[indexPath.row]["album_name"] as? String
        cell.lblSubTitle.text = "by \(arrAlbumList[indexPath.row]["artist_name"]!)"
        
        cell.isCheckBtn.isHidden = true
        cell.btnPlayTap.isHidden = true
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("You selected cell #\(indexPath.item)!")
        self.performSegue(withIdentifier: "videoShowfromAll", sender: indexPath)
    }
}
