//
//  ForgotPasswordVC.swift
//  Kakehashi
//
//  Created by Keshav Infotech on 21/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVC: UIViewController, UITextFieldDelegate {

    //MARK:- Outlets
    @IBOutlet var tfEmailAddress: UITextField!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- textfield delegate method
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        self.view.endEditing(true)
    }

    //MARK:- Other Methods
    func setLayout(){
        
        tfEmailAddress.layer.cornerRadius = 5
        tfEmailAddress.clipsToBounds = true

        tfEmailAddress.delegate = self

        setBorder(tfEmailAddress, 1.0, UIColor(red: 231/255, green: 229/255, blue: 230/255, alpha: 1.0))
        setLeftPadding(tfEmailAddress)
        
        btnSubmit.layer.cornerRadius = 5
        btnSubmit.clipsToBounds = true

        btnCancel.layer.cornerRadius = 5
        btnCancel.clipsToBounds = true
    }
    
    func setBorder(_ objName:AnyObject, _ borderWidth:CGFloat, _ borderColor:UIColor){
        objName.layer.borderColor = borderColor.cgColor
        objName.layer.borderWidth = borderWidth
    }
    
    func setLeftPadding(_ textField:UITextField){
        
        let ivLeft = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: textField.frame.size.height))
        textField.leftView = ivLeft
        textField.leftViewMode = .always
    }
    
    //MARK:- Button Action
    @IBAction func btnSubmit_Action(_ sender: Any) {

        if tfEmailAddress.text != ""{
            if (tfEmailAddress.text?.isEmail)!{
                forgotPassword()
            }
            else
            {
                SharedInstance.alertViewController(message: "Please enter valid email address", inViewController: self)
            }
        }else
        {
            SharedInstance.alertViewController(message: "Please enter email address", inViewController: self)
        }
    }

    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnCancelTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:- webservice method
    func forgotPassword()
    {
        view.endEditing(true)

        if CheckReachability.isConnectedToNetwork() == true
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let URLString = "\(Constant.web_url)forgot_password.php?email=\(tfEmailAddress.text!)"
            Alamofire.request(URLString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    print("Error when calling service")
                    print(response.result.error!)

                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")

                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }
                    else
                    {
                        print("message sent successfully")
                        //self.navigationController?.popViewController(animated: true)

                        let Alert = UIAlertController(title: Constant.PROJECT_NAME as String, message: "Password sent successfully to your email", preferredStyle: UIAlertControllerStyle.alert)

                        Alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in

                            self.navigationController?.popViewController(animated: true)

                        }))

                        self.present(Alert, animated: true, completion: nil)

                    }

                    MBProgressHUD.hide(for:self.view, animated: true)
                }
            }
        }
        else
        {
            MBProgressHUD.hide(for:self.view, animated: true)
            SharedInstance.alertViewController(message: "Please check your internet connection", inViewController: self)
        }
    }
}
