//
//  ListDetailCell.swift
//  Kakehashi
//
//  Created by Keshav on 27/06/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit

class ListDetailCell: UITableViewCell {

    @IBOutlet weak var btnPlayTap: UIButton!
    @IBOutlet weak var isCheckBtn: UIButton!
    @IBOutlet var lblNoSong: UILabel!
    @IBOutlet var imgThumb: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
