//
//  AlbumlistViewController.swift
//  Kakehashi
//
//  Created by keshav on 5/24/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import Alamofire

class AlbumlistTbleCell: UITableViewCell {

    @IBOutlet weak var ImgThumb: UIImageView!
    @IBOutlet weak var lblPlaylistTitle: UILabel!
    @IBOutlet weak var lblNoSong: UILabel!
}

class AlbumlistCollectionViewCell: UICollectionViewCell {

    @IBOutlet var lblSubTitle: UILabel!
    @IBOutlet weak var ImgThumb: UIImageView!
    @IBOutlet weak var lblPlaylistTitle: UILabel!
    @IBOutlet weak var lblNoSong: UILabel!
}

class AlbumlistViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource{

    // outlet's
    @IBOutlet weak var tableListing: UITableView!
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var PopupFilterVw: UIView!
    @IBOutlet weak var AlbumCollectionView: UICollectionView!
    @IBOutlet weak var btnChangeType: UIButton!
    var vwDropdown: DropDownView!
    let FilterTypes = ["New","Popular"]
    var arrAlbumList = Array<Dictionary<String, AnyObject>>()
    var startcounter = Int()
    var filter = String()

    let columnLayout = AlbumlistColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )

    let reuseIdentifier = "AlbumCell"
    
    // MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()


        if #available(iOS 11.0, *) {
            AlbumCollectionView?.collectionViewLayout = columnLayout
            AlbumCollectionView?.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
        }

        self.setLayout()
    
        tableListing.register(UINib(nibName: "ListDetailCell", bundle: nil), forCellReuseIdentifier: "listDetail")
        tableListing.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {

        startcounter = 0
        filter = "new"
        GetAlbumList(filterType: filter, start: startcounter)
        
        let weakSelf:AlbumlistViewController = self
        
        AlbumCollectionView.addInfiniteScrolling(actionHandler: {() -> Void in
            weakSelf.insertRowAtBottom()
        })
        
        tableListing.addInfiniteScrolling(actionHandler: {() -> Void in
            weakSelf.insertRowAtBottom()
        })
        
        self.navigationController?.isNavigationBarHidden = true
    }

    //MARK:- pagination methods
    func insertRowAtBottom()
    {
        startcounter += 20
        
        GetAlbumList(filterType: filter, start: startcounter)
        self.AlbumCollectionView.infiniteScrollingView.stopAnimating()
        self.tableListing.infiniteScrollingView.stopAnimating()
    }
    
    func setLayout() {

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageBlurTapped(_:)))
        imgBlur.isUserInteractionEnabled = true
        imgBlur.addGestureRecognizer(tapGesture)
        
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
        
        self.btnChangeType.layer.cornerRadius = self.btnChangeType.frame.size.height/2
        self.btnChangeType.clipsToBounds = true
        
    }

    @objc func imageBlurTapped(_ sender: UITapGestureRecognizer)
    {
        self.imgBlur.isHidden = true

        if self.PopupFilterVw.isHidden==false {
            self.PopupFilterVw.isHidden = true
        }
    }

    // MARK: - UICollectionViewDataSource & UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if (self.arrAlbumList.count == 0) {
            self.AlbumCollectionView.setEmptyMessage("")
        } else {
            self.AlbumCollectionView.restore()
        }
        return self.arrAlbumList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = AlbumCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! AlbumlistCollectionViewCell

        let thumImgUrl = arrAlbumList[indexPath.row]["album_image"] as! String

        cell.ImgThumb.sd_setImage(with: NSURL(string: thumImgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)

        let no_of_song = arrAlbumList[indexPath.row]["no_of_songs"] as! Int
        
        if no_of_song == 0
        {
            cell.lblNoSong.text = "0 Video"
        }
        else if no_of_song == 1
        {
            cell.lblNoSong.text = "\(no_of_song) Video"
        }
        else
        {
            cell.lblNoSong.text = "\(no_of_song) Videos"
        }
        
        cell.lblPlaylistTitle.text = arrAlbumList[indexPath.row]["album_name"] as? String
        cell.lblSubTitle.text = "by \(UserModel.sharedInstance().fullname!)"

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        self.performSegue(withIdentifier: "video_List1", sender: indexPath)

    }

    // MARK: - Uibutton Actions
    @IBAction func btnSearchAction(_ sender: Any) {
        self.performSegue(withIdentifier: "CpAlbumToSearch", sender: nil)
        
    }
    
    @IBAction func btnChangeViewTap(_ sender: Any) {
        
        if tableListing.isHidden
        {
            tableListing.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
            AlbumCollectionView.isHidden = true
            tableListing.isHidden = false
            
            btnChangeType.setImage(#imageLiteral(resourceName: "gridView"), for: .normal)
        }
        else
        {
            AlbumCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
            AlbumCollectionView.isHidden = false
            tableListing.isHidden = true
            btnChangeType.setImage(#imageLiteral(resourceName: "listView"), for: .normal)
        }
    }

    @IBAction func btnMenuTap(_ sender: Any) {

        self.navigationController?.sideMenu.toggleLeftSideMenu()
    }

    @IBAction func BtnFilterTap(_ sender: Any) {

        if PopupFilterVw.isHidden
        {
            self.imgBlur.isHidden = false
            PopupFilterVw.isHidden = false
        }
        else{

            self.imgBlur.isHidden = true
            PopupFilterVw.isHidden = true
        }
    }

    @IBAction func btnNewToOldTap(_ sender: Any) {

        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true

        startcounter = 0
        filter = "new"
        GetAlbumList(filterType: filter, start: startcounter)
    }

    @IBAction func BtnOldToNewTap(_ sender: Any) {

        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        
        startcounter = 0
        filter = "old"
        GetAlbumList(filterType: filter, start: startcounter)
    }
    
    @IBAction func btnAtoZTap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        
        startcounter = 0
        filter = "atoz"
        GetAlbumList(filterType: filter, start: startcounter)
    }
    
    @IBAction func btnZtoATap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        
        startcounter = 0
        filter = "ztoa"
        GetAlbumList(filterType: filter, start: startcounter)
    }
    
    //MARK:- table view delegate methods
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (self.arrAlbumList.count == 0) {
            tableView.setEmptyMessage("")
        } else {
            tableView.restore()
        }
        return self.arrAlbumList.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableListing.dequeueReusableCell(withIdentifier: "listDetail", for: indexPath) as! ListDetailCell
        //let cell:AlbumlistTbleCell = tableView.dequeueReusableCell(withIdentifier: "albumlisttableCell") as! AlbumlistTbleCell
        
        let thumImgUrl = arrAlbumList[indexPath.row]["album_image"] as! String
        
        cell.imgThumb.sd_setImage(with: NSURL(string: thumImgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)
        
        let no_of_song = arrAlbumList[indexPath.row]["no_of_songs"] as! Int
        
        if no_of_song == 0
        {
            cell.lblNoSong.text = "0 Video"
        }
        else if no_of_song == 1
        {
            cell.lblNoSong.text = "\(no_of_song) Video"
        }
        else
        {
            cell.lblNoSong.text = "\(no_of_song) Videos"
        }
        
        cell.btnPlayTap.isHidden = true
        cell.isCheckBtn.isHidden = true
        cell.lblTitle.text = arrAlbumList[indexPath.row]["album_name"] as? String
        cell.lblSubTitle.text = "by \(UserModel.sharedInstance().fullname!)"
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
        self.performSegue(withIdentifier: "video_List1", sender: indexPath)
    }
    
    // navigationbar methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "video_List1") {
            
            let controller =  segue.destination as! VideoViewController
            let row = (sender as! NSIndexPath).row
            let Id = arrAlbumList[row]["album_id"] as? String
            controller.AlbumId = Id!
            controller.screenType = ""
            controller.screen_title = (arrAlbumList[row]["album_name"] as? String)!
        }
    }
    
    //MARK:- Web service called
    func GetAlbumList(filterType:String, start: Int){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if start == 0
        {
            self.arrAlbumList.removeAll()
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        
        let url = "get_provider_video_page.php?provider_id=\(UserModel.sharedInstance().user_id!)&type=\(filterType)&start=\(start)"
        
        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET login.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                        if start == 0
                        {
                            SharedInstance.alertViewController(message: strMessage, inViewController: self)
                        }
                        
                        //self.arrAlbumList.removeAll()
                    }
                    else
                    {
                        
                        let responseData = json["responseData"] as! [[String:AnyObject]]
                        
                        self.arrAlbumList = responseData
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                    }
                    
                    self.AlbumCollectionView.reloadData()
                    self.tableListing.reloadData()
                    
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }
}


class AlbumlistColumnFlowLayout: UICollectionViewFlowLayout {

    let cellsPerRow: Int

    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()

        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepare() {
        super.prepare()

        guard let collectionView = collectionView else { return }

        if #available(iOS 11.0, *) {
            let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)

            let _: CGFloat = collectionView.superview!.frame.size.height

            itemSize = CGSize(width: itemWidth, height: itemWidth*0.9+100)
        } else {
            // Fallback on earlier versions
        }
    }

    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }
}
