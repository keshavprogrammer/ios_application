//
//  SideMenuVC.swift
//  Kakehashi
//
//  Created by Keshav on 21/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tblMenu: UITableView!
    var arrMenuText = [String]()
    var arrMenuImage = [String]()
    var leftSide : MFSideMenu?
    
    func willUpdateUserData(data: ProfileVC) {

        if UserModel.sharedInstance().user_id != nil
        {
            let UserimgUrl = UserModel.sharedInstance().profile_image
            
            self.imgProfile.sd_setImage(with: URL(string: (UserimgUrl?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!)!), placeholderImage: UIImage(named:"placeholder"), options: .progressiveDownload)
            
        }

        self.view.layoutIfNeeded()
        
    }
    
    //MARK:- view lifecycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let UserimgUrl = UserModel.sharedInstance().profile_image

        DispatchQueue.main.async {

            self.view.layoutIfNeeded()
            self.view.setNeedsLayout()

            self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.height/2
            self.imgProfile.clipsToBounds = true
            self.imgProfile.layer.borderWidth = 2.0
            self.imgProfile.layer.borderColor = UIColor.gray.cgColor
            self.imgProfile.contentMode = .scaleAspectFill
        }
        
        if UserModel.sharedInstance().user_id != nil
        {
            
            self.imgProfile.sd_setImage(with: URL(string: (UserimgUrl?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!)!), placeholderImage: UIImage(named:"placeholder"), options: .progressiveDownload)
            
        }
        
        setMenuFrame()
        
        if UserModel.sharedInstance().user_type == "provider"
        {
            arrMenuText = ["Home", "Profile", "Settings", "Logout"]
            arrMenuImage = ["home", "profile", "setting", "logout"]
        }
        else{
            
            arrMenuText = ["Home", "Profile", "Settings", "Logout"]
            arrMenuImage = ["home", "profile", "setting", "logout"]
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        let UserimgUrl = UserModel.sharedInstance().profile_image

        if UserModel.sharedInstance().user_id != nil
        {
            self.imgProfile.sd_setImage(with: URL(string: (UserimgUrl?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!)!), placeholderImage: UIImage(named:"placeholder"), options: .progressiveDownload)
        }
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }

    //MARK:- table view delegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblMenu.dequeueReusableCell(withIdentifier: "sidemenu", for: indexPath as IndexPath) as! SideMenuTableViewCell
        
        cell.imgOption.image = UIImage(named: arrMenuImage[indexPath.row])
        cell.lblOption.text = arrMenuText[indexPath.row]
        
        tableView.tableFooterView = UIView()
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {

            if UserModel.sharedInstance().user_type! == "provider"
            {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "AlbumlistViewController") as! AlbumlistViewController
                let controllers: [Any] = [vc]
                leftSide?.navigationController.viewControllers = controllers as! [UIViewController]
                leftSide?.menuState = MFSideMenuStateClosed

            }
            else
            {

                let vc = self.storyboard!.instantiateViewController(withIdentifier: "YoutubeWithLabelExampleViewController") as! YoutubeWithLabelExampleViewController
                let controllers: [Any] = [vc]
                leftSide?.navigationController.viewControllers = controllers as! [UIViewController]
                leftSide?.menuState = MFSideMenuStateClosed


            }

        }
        else if indexPath.row ==  1
        {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            let controllers: [Any] = [vc]
            leftSide?.navigationController.viewControllers = controllers as! [UIViewController]
            leftSide?.menuState = MFSideMenuStateClosed
        }
        else if indexPath.row == 2
        {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
            let controllers: [Any] = [vc]
            leftSide?.navigationController.viewControllers = controllers as! [UIViewController]
            leftSide?.menuState = MFSideMenuStateClosed
        }
        else if indexPath.row == 3
        {

            let logoutAlert = UIAlertController(title: Constant.PROJECT_NAME as String, message: "Are You Sure to Log Out ? ", preferredStyle: UIAlertControllerStyle.alert)

            logoutAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in

                UserModel.sharedInstance().removeData()
                UserModel.sharedInstance().synchroniseData()

                SharedInstance.appDelegate().setSlideRootNavigation()
            }))

            logoutAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in

                // refreshAlert .dismissViewControllerAnimated(true, completion: nil)

            }))

            present(logoutAlert, animated: true, completion: nil)
        }
    }
    
    //MARK:- other method
    func setMenuFrame()
    {
        leftSide?.menuWidth = (50*self.view.frame.width)/100
    }
}
