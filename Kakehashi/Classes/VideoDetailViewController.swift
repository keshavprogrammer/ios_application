//
//  VideoDetailViewController.swift
//  Kakehashi
//
//  Created by keshav on 5/24/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import Alamofire
import KMPlaceholderTextView
import AVKit

class VideoDetailViewController: UIViewController {

    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var VideoPopup: UIView!
    @IBOutlet weak var lbluserNm: UILabel!
    @IBOutlet weak var lblpublished: UILabel!
    @IBOutlet weak var txtdesc: UITextView!
    @IBOutlet weak var ImgofUser: UIImageView!
    @IBOutlet weak var ImgPreview: UIImageView!
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    @IBOutlet weak var btnReportOutlet: UIButton!
    @IBOutlet weak var PopupreRportView: UIView!
    @IBOutlet weak var txtReport: KMPlaceholderTextView!
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var btnSubmitOutlet: UIButton!
    @IBOutlet weak var btnCancelOutlet: UIButton!
    @IBOutlet weak var lblVideoTitle: UILabel!

    var videodetail = [String:AnyObject]()
    var arrPlaylist = Array<Dictionary<String, AnyObject>>()
    var AllPlaylist = [String]()
    var AddplaylistName = String()
    var selectplaylist_id = String()
    var isFromSearch = String()
    var videoId = String()

    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        Dataload()
        setLayout()

        if UserModel.sharedInstance().user_type! == "provider"
        {
            btnDeleteOutlet.setImage(#imageLiteral(resourceName: "Delete"), for: .normal)
        }
        else
        {
            btnDeleteOutlet.setImage(#imageLiteral(resourceName: "Options"), for: .normal)

            GetPlayList()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Other Method
    func setLayout(){
        PopupreRportView.layer.cornerRadius = 12
        PopupreRportView.clipsToBounds = true

        txtReport.layer.cornerRadius = 8
        txtReport.clipsToBounds = true

        btnSubmitOutlet.layer.cornerRadius = 8
        btnSubmitOutlet.clipsToBounds = true

        btnCancelOutlet.layer.cornerRadius = 8
        btnCancelOutlet.clipsToBounds = true

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageBlurTapped(_:)))
        imgBlur.isUserInteractionEnabled = true
        imgBlur.addGestureRecognizer(tapGesture)
    }

    @objc func imageBlurTapped(_ sender: UITapGestureRecognizer)
    {
        self.imgBlur.isHidden = true

        if self.PopupreRportView.isHidden==false {
            self.PopupreRportView.isHidden = true
        }
        
        if VideoPopup.isHidden == false
        {
            VideoPopup.isHidden = true
        }
    }

    func Dataload() {

        if isFromSearch == "yes"
        {
            videoId = videodetail["id"] as! String
            let imgUrl = videodetail["image"] as! String
            
            ImgPreview.sd_setShowActivityIndicatorView(true)
            ImgPreview.sd_setIndicatorStyle(.gray)
            ImgPreview.sd_setImage(with: URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!)) { (image, error, imageCacheType, imageUrl) in
                if image != nil {
                    print("image found")
                }else
                {
                    print("image not found")
                }
                self.ImgPreview.sd_setShowActivityIndicatorView(false)
            }
            
            DispatchQueue.main.async {
                
                self.view.layoutIfNeeded()
                self.view.setNeedsLayout()
                
                self.ImgofUser.layer.cornerRadius = self.ImgofUser.frame.size.height/2
                self.ImgofUser.clipsToBounds = true
                self.ImgofUser.layer.borderWidth = 0
                self.ImgofUser.contentMode = .scaleAspectFill
            }
            
            self.lblVideoTitle.text = videodetail["name"] as? String
            txtdesc.text = videodetail["detail"] as! String
            lbluserNm.text = videodetail["artist_name"] as? String
            
            if let _ = videodetail["artist_image"] as? String
            {
                let UserimgUrl = videodetail["artist_image"] as! String
                
                ImgofUser.sd_setImage(with: NSURL(string: (UserimgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!)) as URL?, placeholderImage: UIImage(named: "Dummy_User1"), options: .progressiveDownload)
            }
            
            if let _ = videodetail["artist_profile_image"] as? String
            {
                let UserimgUrl = videodetail["artist_profile_image"] as! String
                
                ImgofUser.sd_setImage(with: NSURL(string: (UserimgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!)) as URL?, placeholderImage: UIImage(named: "Dummy_User1"), options: .progressiveDownload)
            }
            
            if let _ = videodetail["published_date"] as? String
            {
                let publishDateString = videodetail["published_date"] as! String
                
                lblpublished.text = "Published on \( publishDateString.convertDateFormater(date: publishDateString, inputDateFormate: "dd/MM/yyyy", outputDateFormate: "MMM d, yyyy"))"
            }
            
            if let _ = videodetail["date"] as? String
            {
                let publishDateString = videodetail["date"] as! String
                
                lblpublished.text = "Published on \( publishDateString.convertDateFormater(date: publishDateString, inputDateFormate: "yyyy-MM-dd HH:mm:ss", outputDateFormate: "MMM d, yyyy"))"
            }
            
            addToRecentSearch(post_id: videoId)
        }
        else{
            
            videoId = videodetail["video_id"] as! String
            let imgUrl = videodetail["video_image"] as! String
            
            ImgPreview.sd_setShowActivityIndicatorView(true)
            ImgPreview.sd_setIndicatorStyle(.gray)
            ImgPreview.sd_setImage(with: URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!)) { (image, error, imageCacheType, imageUrl) in
                if image != nil {
                    print("image found")
                }else
                {
                    print("image not found")
                }
                self.ImgPreview.sd_setShowActivityIndicatorView(false)
            }
            
            DispatchQueue.main.async {
                
                self.view.layoutIfNeeded()
                self.view.setNeedsLayout()
                
                self.ImgofUser.layer.cornerRadius = self.ImgofUser.frame.size.height/2
                self.ImgofUser.clipsToBounds = true
                self.ImgofUser.layer.borderWidth = 0
                self.ImgofUser.contentMode = .scaleAspectFill
            }
            
            self.lblVideoTitle.text = videodetail["video_title"] as? String
            txtdesc.text = videodetail["description"] as! String
            lbluserNm.text = videodetail["provider_name"] as! String
            
            let UserimgUrl = videodetail["provider_image_path"] as! String
            
            ImgofUser.sd_setImage(with: NSURL(string: (UserimgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!)) as URL?, placeholderImage: UIImage(named: "Dummy_User1"), options: .progressiveDownload)
            
            let publishDateString = videodetail["published_date"] as! String
            
            lblpublished.text = "Published on \( publishDateString.convertDateFormater(date: publishDateString, inputDateFormate: "yyyy-MM-dd HH:mm:ss", outputDateFormate: "MMM d, yyyy"))"
        }
    }

    // MARK: - Uibutton Actions
    @IBAction func btnAddtoPlaylist(_ sender: Any) {

        self.imgBlur.isHidden = true
        self.VideoPopup.isHidden = true

        SharedInstance.ShowStringPicker(id: sender as AnyObject, Title: "My Playlist", rows: AllPlaylist as NSArray, initialSelection: 0, onCompletion: {(indexNumber,indexValue)  in
            DispatchQueue.main.async{
                print("value of index and value",indexNumber,indexValue)

                if indexValue == "Create Playlist"
                {
                    let alert = UIAlertController(title: "New playlist name", message: "Enter Playlist name", preferredStyle: UIAlertControllerStyle.alert)

                    alert.addTextField(configurationHandler: self.configurationTextField)

                    alert.addAction(UIAlertAction(title: "Add", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in

                        let firstTextField = alert.textFields![0] as UITextField

                        self.AddplaylistName = firstTextField.text!

                        if !self.AddplaylistName.isEmpty
                        {
                            self.CreatePlayList()
                        }
                        else
                        {
                            SharedInstance.alertViewController(message: "Please enter playlist name", inViewController: self)
                        }
                    }))

                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in

                    }))

                    self.present(alert, animated: true, completion: {
                        print("completion block")


                    })
                }
                else
                {
                    self.selectplaylist_id = (self.arrPlaylist[indexNumber] )["playlist_id"] as! String

                    if !self.selectplaylist_id.isEmpty
                    {
                        self.AddToPlayList()
                    }
                }
            }
        })
    }

    func configurationTextField(textField: UITextField!){

        textField.placeholder = "Playlist"
    }

    @IBAction func btnReportVideo(_ sender: Any) {

        VideoPopup.isHidden = true

        imgBlur.isHidden = false
        PopupreRportView.isHidden = false
    }

    @IBAction func btnCancelReport(_ sender: Any) {

        imgBlur.isHidden = true
        PopupreRportView.isHidden = true
    }

    @IBAction func btnSubmitReport(_ sender: Any) {

        if txtReport.text != ""
        {
            imgBlur.isHidden = true
            PopupreRportView.isHidden = true
            self.ReportVideos(Video_id: videodetail["video_id"] as! String)
        }
        else
        {
            SharedInstance.alertViewController(message: "Please enter message", inViewController: self)
        }
    }

    @IBAction func btnbackTap(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnDeleteTap(_ sender: Any) {

        if UserModel.sharedInstance().user_type! == "provider"
        {

            let logoutAlert = UIAlertController(title: Constant.PROJECT_NAME as String, message: "Are you sure that you want to delete the selected Video?", preferredStyle: UIAlertControllerStyle.alert)

            logoutAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in

                self.DeleteVideos(Video_id: self.videodetail["video_id"] as! String)

            }))

            logoutAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in

                // refreshAlert .dismissViewControllerAnimated(true, completion: nil)

            }))

            present(logoutAlert, animated: true, completion: nil)


        }
        else
        {
            if self.VideoPopup.isHidden
            {
                imgBlur.isHidden = false
                VideoPopup.isHidden = false
            }
            else{
                imgBlur.isHidden = true
                VideoPopup.isHidden = true
            }
        }
    }

    @IBAction func btnPayVideo(_ sender: Any) {

        view_video()
        let videoURL = NSURL(string: "\(videodetail["video_file"] as! String)")
        let playerAV = AVPlayer(url: videoURL! as URL)

        let playerController = AVPlayerViewController()
        playerController.player = playerAV
        present(playerController, animated: true) {
            playerAV.play()
        }
    }

    // MARK: - Webservice
    func view_video(){
        
        let url = "view_video.php?video_id=\(videoId)"
        
        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET view_video.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        //let strMessage = json["message"] as! String
                        
                    }
                    else
                    {
                        
                        print(strResult)
                        //let strMessage = json["message"] as! String
                        
                    }
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }
    
    func GetPlayList(){

        MBProgressHUD.showAdded(to: self.view, animated: true)

        let filter = "new"
        let url = "get_user_playlist.php?user_id=\(UserModel.sharedInstance().user_id!)&type=\(filter)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!

        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET login.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {

                        self.AllPlaylist.append("Create Playlist")

                    }
                    else
                    {
                        self.arrPlaylist.removeAll()

                        let responseData = json["responseData"] as! [[String:AnyObject]]

                        self.arrPlaylist = responseData
                        for i in 0..<self.arrPlaylist.count
                        {
                            self.AllPlaylist.append("\((self.arrPlaylist[i] )["title"] as! String)")
                        }

                        self.AllPlaylist.append("Create Playlist")
                    }

                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }

    func CreatePlayList(){

        MBProgressHUD.showAdded(to: self.view, animated: true)

        let url = "create_playlist.php?&title=\(AddplaylistName)&user_id=\(UserModel.sharedInstance().user_id!)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!

        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET login.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String

                        SharedInstance.alertViewController(message: strMessage, inViewController: self)

                    }
                    else
                    {
                        let strMessage =
                            self.selectplaylist_id = json["playlist_id"] as! String

                        if !self.selectplaylist_id.isEmpty
                        {
                            self.AddToPlayList()
                        }
                    }

                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }

    func AddToPlayList(){

        MBProgressHUD.showAdded(to: self.view, animated: true)

        let url = "add_to_playlist.php?video_id=\(videoId)&user_id=\(UserModel.sharedInstance().user_id!)&playlist_id=\(selectplaylist_id)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!

        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET login.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    let strMessage = json["message"] as! String

                    if strResult == "0"
                    {
                        print(strResult)


                        SharedInstance.alertViewController(message: strMessage, inViewController: self)

                    }
                    else
                    {
                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }

                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }

    func DeleteVideos(Video_id:String){

        MBProgressHUD.showAdded(to: self.view, animated: true)

        let url = "delete_video.php?video_id=\(Video_id)&user_id=\(UserModel.sharedInstance().user_id!)"

        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET login.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }
                    else
                    {
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        //SharedInstance.alertViewController(message: strMessage, inViewController: self)

                        self.navigationController?.popViewController(animated: true)
                    }

                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }

    }

    func ReportVideos(Video_id:String){

        MBProgressHUD.showAdded(to: self.view, animated: true)

        let url = "report_video.php?video_id=\(Video_id)&message=\(txtReport.text!)&user_id=\(UserModel.sharedInstance().user_id!)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!

        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET login.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }
                    else
                    {
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }

                    self.txtReport.text = ""

                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }

    }
    
    func addToRecentSearch(post_id: String){
        
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if CheckReachability.isConnectedToNetwork() == true
        {
            let url = "add_recent_search.php?user_id=\(UserModel.sharedInstance().user_id!)&type=video&type_id=\(post_id)&user_type=\(UserModel.sharedInstance().user_type!)"
            
            Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON { response in
                    
                    // check for errors
                    guard response.result.error == nil else {
                        // got an error in getting the data, need to handle it
                        print("error calling GET addToRecentSearch")
                        print(response.result.error!)
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        return
                    }
                    
                    // make sure we got some JSON since that's what we expect
                    guard let json = response.result.value as? [String: Any] else {
                        print("didn't get todo object as JSON from API")
                        print("Error: \(String(describing: response.result.error))")
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        return
                    }
                    
                    if let strResult = json["status"] as? String
                    {
                        if strResult == "0"
                        {
                            print(strResult)
                            let strMessage = json["message"] as! String
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        }
                        else
                        {
                            let strMessage = json["message"] as! String
                            print(strMessage)
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        }
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    }
            }
        }
        else
        {
            print("no internet connection")
        }
    }
}

