//
//  AddVideoVC.swift
//  Kakehashi
//
//  Created by Keshav Infotech on 19/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import Alamofire

class AddVideoVC: UIViewController, UITextFieldDelegate {

    //MARK:- Outlets
    @IBOutlet var tfVideoTitle: UITextField!
    @IBOutlet var tfVideoCategory: UITextField!
    @IBOutlet weak var tfAlbumlist: UITextField!
    @IBOutlet var tvDescription: UITextView!
    @IBOutlet var btnSelectVideo: UIButton!

    @IBOutlet var btnCreateAlbum: UIButton!
    @IBOutlet var lblVideoStatus: UILabel!
    @IBOutlet var ivThumbnail: UIImageView!
    @IBOutlet var btnAdd: UIButton!

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnCancelPopup: UIButton!
    @IBOutlet var vwPopUp: UIView!
    @IBOutlet var tfEnterName: UITextField!
    @IBOutlet var ivPic: UIImageView!
    @IBOutlet var blurView: UIImageView!
    @IBOutlet var btnAddPop: UIButton!

    var arrCategory = Array<Dictionary<String, AnyObject>>()
    var arrAlbums = Array<Dictionary<String, AnyObject>>()

    var Allcategory = [String]()
    var AllAlbums = [String]()

    var strCategoryId = String()
    var strAlbumId = String()
    var isSelectedVideoURL = String()
    var picker:UIImagePickerController?=UIImagePickerController()

    //MARK:- View Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()

        GetCategoryList()
        GetalbumList()
        setLayout()
        
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.openImagePicker(_:)))
        ivPic.addGestureRecognizer(tapGesture)
        ivPic.isUserInteractionEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        self.view.endEditing(true)
    }

    //MARK:- Other Methods
    func setLayout(){

        tfAlbumlist.delegate = self
        tfEnterName.delegate = self
        tfVideoTitle.delegate = self
        tfVideoCategory.delegate = self

        lblVideoStatus.text = "No select video"

        blurView.isHidden = true
        vwPopUp.isHidden = true
        
        vwPopUp.layer.cornerRadius = 20
        vwPopUp.clipsToBounds = true
        
        DispatchQueue.main.async {
            self.ivPic.layer.borderWidth = 2
            self.ivPic.layer.borderColor = UIColor.lightGray.cgColor
        }
        
        tfVideoTitle.layer.cornerRadius = 8
        tfVideoTitle.clipsToBounds = true
        setBorder(tfVideoTitle, 1.0, UIColor.lightGray)
        setLeftPadding(tfVideoTitle)

        tfVideoCategory.layer.cornerRadius = 8
        tfVideoCategory.clipsToBounds = true
        setBorder(tfVideoCategory, 1.0, UIColor.lightGray)
        setLeftPadding(tfVideoCategory)
        setRightImage(tfVideoCategory, "DropDown")

        tfAlbumlist.layer.cornerRadius = 8
        tfAlbumlist.clipsToBounds = true
        setBorder(tfAlbumlist, 1.0, UIColor.lightGray)
        setLeftPadding(tfAlbumlist)
        setRightImage(tfAlbumlist, "DropDown")

        tvDescription.layer.cornerRadius = 8
        tvDescription.clipsToBounds = true
        setBorder(tvDescription, 1.0, UIColor.lightGray)

        btnSelectVideo.layer.cornerRadius = 8
        btnSelectVideo.clipsToBounds = true
        setBorder(btnSelectVideo, 1.0, UIColor.lightGray)
        
        btnCreateAlbum.layer.cornerRadius = 8
        btnCreateAlbum.clipsToBounds = true
        setBorder(btnCreateAlbum, 1.0, UIColor.lightGray)

        tfEnterName.layer.cornerRadius = 8
        tfEnterName.clipsToBounds = true
        setBorder(tfEnterName, 1.0, UIColor.lightGray)
        setLeftPadding(tfEnterName)
        
        ivThumbnail.layer.cornerRadius = 8
        ivThumbnail.clipsToBounds = true

        btnAdd.layer.cornerRadius = 5
        btnAdd.clipsToBounds = true
        
        btnCancel.layer.cornerRadius = 5
        btnCancel.clipsToBounds = true
        
        btnAddPop.layer.cornerRadius = 5
        btnAddPop.clipsToBounds = true

        btnCancelPopup.layer.cornerRadius = 5
        btnCancelPopup.clipsToBounds = true

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageBlurTapped(_:)))
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(tapGesture)
    }

    func setBorder(_ objName:AnyObject, _ borderWidth:CGFloat, _ borderColor:UIColor){
        objName.layer.borderColor = borderColor.cgColor
        objName.layer.borderWidth = borderWidth
    }

    func setLeftPadding(_ textField:UITextField){
        let ivLeft = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textField.frame.size.height))
        textField.leftView = ivLeft
        textField.leftViewMode = .always
    }

    func setRightImage(_ textField:UITextField, _ imageName:String){
        let ivRight = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: textField.frame.size.height))
        ivRight.image = UIImage(named: imageName)
        ivRight.contentMode = .center
        textField.rightView = ivRight
        textField.rightViewMode = .always
    }

    @objc func imageBlurTapped(_ sender: UITapGestureRecognizer){

        UIView.animate(withDuration: 0.4, animations:{
            self.blurView.isHidden = true
            self.vwPopUp.alpha = 0.0
            self.vwPopUp.transform = CGAffineTransform.identity
        }) { (finish) in

            self.vwPopUp.transform =  CGAffineTransform.init(scaleX : 1.0 ,y:1.0)
        }
    }

    //MARK:- Button Action
    
    @IBAction func btnCreateAlbumAction(_ sender: Any) {
        self.blurView.isHidden = false
        self.vwPopUp.isHidden = false
        self.blurView.addSubview(self.vwPopUp)
        self.vwPopUp.center = self.view.center
        self.vwPopUp.transform =  CGAffineTransform.init(scaleX : 1.0 ,y:1.0)
        
        UIView.animate(withDuration: 0.4, animations: {
            self.vwPopUp.alpha = 1
            self.vwPopUp.transform = CGAffineTransform.identity
        }) { (finish) in
            self.vwPopUp.transform =  CGAffineTransform.init(scaleX : 1.0 ,y:1.0)
        }
    }
    
    @IBAction func btnCancelTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnShow_Action(_ sender: Any)
    {
        
    }
    
    @IBAction func btnSelectPicture(_ sender: Any)
    {
        openPictureSelection()
    }

    @IBAction func btnCancelActionTap(_ sender: Any) {

        UIView.animate(withDuration: 0.4, animations:{
            self.blurView.isHidden = true
            self.vwPopUp.alpha = 0.0
            self.vwPopUp.transform = CGAffineTransform.identity
        }) { (finish) in
            self.vwPopUp.transform =  CGAffineTransform.init(scaleX : 1.0 ,y:1.0)
        }
    }

    @IBAction func btnAddPopUp_Action(_ sender: Any)
    {
        
        if (tfEnterName.text?.isEmpty)!
        {
            self.tfEnterName.becomeFirstResponder()
            SharedInstance.alertViewController(message: "Please enter album name", inViewController: self)
        }
        else
        {
            if ivPic.image == UIImage(named: "default_image")
            {
                SharedInstance.alertViewController(message: "Please select video", inViewController: self)
            }
            else
            {
                UIView.animate(withDuration: 0.4, animations:{
                    self.blurView.isHidden = true
                    self.vwPopUp.alpha = 0.0
                    self.vwPopUp.transform = CGAffineTransform.identity
                }) { (finish) in
                    self.vwPopUp.transform =  CGAffineTransform.init(scaleX : 1.0 ,y:1.0)
                    self.createAlbum()
                }
            }
        }
    }
    
    @IBAction func btnSelectVideo_Action(_ sender: Any) {

        PhotoServices.shared.getVideoFromCameraRoll(on: self) { url in

            self.isSelectedVideoURL = url.absoluteString
            let thumbnail = PhotoServices.shared.getThumbnailFrom(path: url)
            self.ivThumbnail.image = thumbnail

            self.lblVideoStatus.text = ""
        }
    }

    @IBAction func btnAdd_Action(_ sender: Any) {

        if (!CommonFunction().checkIsEmpty(s: tfVideoTitle.text!)) {

            if (!CommonFunction().checkIsEmpty(s: tfVideoCategory.text!)) {

                if (!CommonFunction().checkIsEmpty(s: tfAlbumlist.text!)) {

                    if (!CommonFunction().checkIsEmpty(s: tvDescription.text!)) {

                        if (!(isSelectedVideoURL.isEmpty)) {

                            uploadVideo()
                        }
                        else
                        {
                            self.showToastMessage("Please select upload video from gallery.")
                        }
                    }
                    else
                    {
                        self.showToastMessage("Please enter Description.")
                    }
                }
                else
                {
                    self.showToastMessage("Please select Albums category.")
                }
            }
            else
            {
                self.showToastMessage("Please select Video category.")
            }
        }
        else
        {
            self.showToastMessage("Please enter video title.")
        }
    }

    @IBAction func btnMenu_Action(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func openImagePicker(_ recognizer: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        ImagePicker.shared.showImagePicker(viewController1: self, imageView: self.ivPic)
    }
    
    func openPictureSelection()
    {
        self.view.endEditing(true)
        ImagePicker.shared.showImagePicker(viewController1: self, imageView: self.ivPic)
    }
    
    //MARK:- textfield delegate method
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        if textField == tfVideoCategory
        {
            tfVideoCategory.text = ""
            self.view.endEditing(true)
            SharedInstance.ShowStringPicker(id: tfVideoCategory, Title: tfVideoCategory.placeholder!, rows: Allcategory as NSArray, initialSelection: 0, onCompletion: {(indexNumber,indexValue)  in
                DispatchQueue.main.async{
                    print("value of index and value",indexNumber,indexValue)

                    self.tfVideoCategory.text = indexValue
                    self.strCategoryId = ((self.arrCategory[indexNumber])["category_id"] as! String)
                }
            })
            return false

        }
        else if textField == tfAlbumlist
        {
            tfAlbumlist.text = ""
            self.view.endEditing(true)
            
//            if AllAlbums.count == 0
//            {
//                self.AllAlbums.append("Create new album")
//            }
            
            if AllAlbums.count > 0
            {
                SharedInstance.ShowStringPicker(id: tfAlbumlist, Title: tfAlbumlist.placeholder!, rows: AllAlbums as NSArray, initialSelection: 0, onCompletion: {(indexNumber,indexValue)  in
                    DispatchQueue.main.async{
                        print("value of index and value",indexNumber,indexValue)
                        
                        self.tfAlbumlist.text = indexValue
                        self.strAlbumId = ((self.arrAlbums[indexNumber])["album_id"] as! String)
                        
                        //                    if indexValue == "Create new album"
                        //                    {
                        //                        self.blurView.isHidden = false
                        //                        self.vwPopUp.isHidden = false
                        //                        self.blurView.addSubview(self.vwPopUp)
                        //                        self.vwPopUp.center = self.view.center
                        //                        self.vwPopUp.transform =  CGAffineTransform.init(scaleX : 1.0 ,y:1.0)
                        //
                        //                        UIView.animate(withDuration: 0.4, animations: {
                        //                            self.vwPopUp.alpha = 1
                        //                            self.vwPopUp.transform = CGAffineTransform.identity
                        //                        }) { (finish) in
                        //                            self.vwPopUp.transform =  CGAffineTransform.init(scaleX : 1.0 ,y:1.0)
                        //                        }
                        //                    }
                        //                    else
                        //                    {
                        //                        self.tfAlbumlist.text = indexValue
                        //                        self.strAlbumId = ((self.arrAlbums[indexNumber])["album_id"] as! String)
                        //                    }
                        
                    }
                })
            }
            else{
                SharedInstance.alertViewController(message: "There are no albums in the list. Please create the album first", inViewController: self)
            }
            
            return false
        }
        else
        {
            return true
        }
    }

    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    //MARK:- webservice method
    func createAlbum()
    {
        if CheckReachability.isConnectedToNetwork() == true
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)

            var imgData = Data()

            if ivPic.image == UIImage(named: "default_image")
            {
                imgData = Data()
            }
            else{
                let profileImg = ivPic.image?.resizeWithWidth(width: 500)
                imgData = UIImageJPEGRepresentation(profileImg!, 0.5)!
            }

            let  parameters: [String:String]
            parameters = ["provider_id":"\(UserModel.sharedInstance().user_id!)","album_name":"\(tfEnterName.text!)"]

            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "album_image",fileName: "image_post.png", mimeType: "image/png")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            },
                             to:"\(Constant.web_url)create_album.php")
            { (result) in
                switch result {

                case .success(let upload, _, _):

                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })

                    upload.responseJSON { response in

                        guard let json = response.result.value as? [String: Any] else {
                            print("didn't get todo object as JSON from API")
                            print("Error: \(String(describing: response.result.error))")

                            return
                        }

                        if let strResult = json["status"] as? String
                        {
                            if strResult == "0"
                            {
                                let strMessage = json["message"] as! String
                                print(strMessage)
                            }
                            else
                            {
                                self.tfEnterName.text = ""
                                self.ivPic.image = nil

                                let alert = UIAlertController(title: "Kakehashi", message: "Album created successfully", preferredStyle: UIAlertControllerStyle.alert)

                                let okAction = UIAlertAction (title: "Ok", style: UIAlertActionStyle.cancel, handler: { action -> Void in

                                    self.GetalbumList()
                                })
                                alert.addAction(okAction)

                                self.present(alert, animated: true, completion: nil)
                            }

                            MBProgressHUD.hide(for:self.view, animated: true)
                        }
                    }
                case .failure(_):

                    MBProgressHUD.hide(for:self.view, animated: true)
                }
            }
        }
        else
        {
            MBProgressHUD.hide(for:self.view, animated: true)
            SharedInstance.alertViewController(message: "Please check your internet connection", inViewController: self)
        }

    }

    func GetCategoryList(){

        MBProgressHUD.showAdded(to: self.view, animated: true)

        WebserviceHandler.callWebService(viewcontroller: self, parameters: "get_category.php", onCompletion: {(serviceResponse) in
            DispatchQueue.main.async{
                print(serviceResponse)

                self.arrCategory.removeAll()
                self.arrCategory = serviceResponse
                self.Allcategory.removeAll()
                for i in 0..<self.arrCategory.count
                {
                    self.Allcategory.append("\((self.arrCategory[i] as! [String:AnyObject])["category_name"] as! String)")
                }

                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            }
        }, failure: {(Message) in
            DispatchQueue.main.async
                {
                    //self.showToastMessage(Message! as! String)

                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            }

        })
    }

    func GetalbumList(){

        MBProgressHUD.showAdded(to: self.view, animated: true)

        WebserviceHandler.callWebService(viewcontroller: self, parameters: "get_provider_video.php?provider_id=\(UserModel.sharedInstance().user_id!)&type=new", onCompletion: {(serviceResponse) in
            DispatchQueue.main.async{
                print(serviceResponse)

                self.arrAlbums.removeAll()
                self.arrAlbums = serviceResponse
                self.AllAlbums.removeAll()
                //self.AllAlbums.append("Create new album")
                for i in 0..<self.arrAlbums.count
                {
                    self.AllAlbums.append("\((self.arrAlbums[i] )["album_name"] as! String)")
                }

                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            }
        }, failure: {(Message) in
            DispatchQueue.main.async
                {
                    //self.showToastMessage(Message! as! String)

                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            }

        })
    }

    func uploadVideo(){

        MBProgressHUD.showAdded(to: self.view, animated: true)
        let serviceRootURL="http://keshavinfotechdemo.com/KESHAV/KG2/mladen/webservices/"

        let imgData = UIImageJPEGRepresentation(self.ivThumbnail.image!, 0.5)!

        let videoPathUrl = URL(string: isSelectedVideoURL)

        var movieData: Data!
        do {

            movieData = try Data(contentsOf: videoPathUrl!)

        } catch {
            print(error)
            movieData = nil

        }

        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let strDate = df.string(from: Date())

        let parameters = ["video_title": "\(tfVideoTitle.text!)","category":"\(strCategoryId)","description":"\(tvDescription.text!)","published_date":"\(strDate)","content_provider_id":"\(UserModel.sharedInstance().user_id!)","album_id":"\(strAlbumId)","album_name":"\(tfEnterName.text!)"]

        Alamofire.upload(multipartFormData: { multipartFormData in

            multipartFormData.append(imgData, withName: "video_image",fileName: "image_post.png", mimeType: "image/png")

            multipartFormData.append(movieData, withName: "video_file", fileName: "filename.mp4", mimeType: "mp4")

            // multipartFormData.append(videoPathUrl, withName: "video_file",fileName: "image_post.png", mimeType: "image/png")

            //  multipartFormData.append(movieData,withName: "video_file", fileName: "filename", mimeType: "mov")

            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                         to:"\(serviceRootURL)upload_video.php")
        { (result) in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })

                upload.responseJSON { response in
                    guard let json = response.result.value as? [String: Any] else {
                        print("didn't get todo object as JSON from API")
                        print("Error: \(String(describing: response.result.error))")
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        return
                    }

                    if let strResult = json["status"]
                    {
                        if (strResult as! String)=="0"
                        {
                            print(strResult)
                            //let strMessage = json["message"] as! String
                        }
                        else
                        {
                            //let strMessage = json["message"] as! String

                            let Alert = UIAlertController(title: Constant.PROJECT_NAME as String, message: "Video uploaded successfully", preferredStyle: UIAlertControllerStyle.alert)
                            
                            Alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                
                            
                                self.navigationController?.popViewController(animated: true)
                            }))
                            
                            self.present(Alert, animated: true, completion: nil)
                        }

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    }
                }

            case .failure(let encodingError):
                print(encodingError)
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            }
        }
    }
}
extension AddVideoVC : UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,UIPopoverPresentationControllerDelegate
{

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        defer {
            picker.dismiss(animated: true)
        }
        
        print(info)
        // get the image
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        // do something with it
        ivPic.image = image
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        defer {
            picker.dismiss(animated: true)
        }
        
        print("did cancel")
    }
}
