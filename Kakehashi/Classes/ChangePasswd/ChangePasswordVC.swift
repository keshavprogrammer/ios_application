//
//  ChangePasswordVC.swift
//  Kakehashi
//
//  Created by Keshav Infotech on 21/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import Alamofire

class ChangePasswordVC: UIViewController, UITextFieldDelegate {

    //MARK:- Outlets
    @IBOutlet var tfOldPassword: UITextField!
    @IBOutlet var tfNewPassword: UITextField!
    @IBOutlet var tfConfirmPassword: UITextField!
    @IBOutlet var btnChangePassword: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- textfield delegate method
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        self.view.endEditing(true)
    }

    //MARK:- Other Methods
    func setLayout(){

        setTextFieldProperty(tfOldPassword)
        setTextFieldProperty(tfNewPassword)
        setTextFieldProperty(tfConfirmPassword)

        tfOldPassword.delegate = self
        tfNewPassword.delegate = self
        tfConfirmPassword.delegate = self

        btnChangePassword.layer.cornerRadius = 5
        btnChangePassword.clipsToBounds = true
        
        btnCancel.layer.cornerRadius = 5
        btnCancel.clipsToBounds = true
    }
    
    func setTextFieldProperty(_ textField:UITextField){
        setCornerRadius(textField, 8)
        setBorder(textField, 1.0, UIColor(red: 231/255, green: 229/255, blue: 230/255, alpha: 1.0))
        setLeftPadding(textField)
    }
    
    func setBorder(_ objName:AnyObject, _ borderWidth:CGFloat, _ borderColor:UIColor){
        objName.layer.borderColor = borderColor.cgColor
        objName.layer.borderWidth = borderWidth
    }
    
    func setCornerRadius(_ textField:UITextField, _ cornerRadiusValue:CGFloat){
        textField.layer.cornerRadius = cornerRadiusValue
        textField.clipsToBounds = true
    }
    
    func setLeftPadding(_ textField:UITextField){
        let ivLeft = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textField.frame.size.height))
        textField.leftView = ivLeft
        textField.leftViewMode = .always
    }
    
    //MARK:- Button Action
    @IBAction func btnCancelTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnChangePassword_Action(_ sender: Any)
    {
        if tfOldPassword.text == "" {

            SharedInstance.alertViewController(message: "Please enter Old Password", inViewController: self)
        }else if tfNewPassword.text == "" {

            SharedInstance.alertViewController(message: "Please enter New Password", inViewController: self)
        }else if tfConfirmPassword.text == "" {

            SharedInstance.alertViewController(message: "Please enter Confirm Password", inViewController: self)
        }else if !(CommonFunction().isValidPassword(testStr: tfNewPassword.text!)) {

            SharedInstance.alertViewController(message: "Please enter minimum 6 characters in Password", inViewController: self)
        }
        else if tfNewPassword.text != tfConfirmPassword.text {

            SharedInstance.alertViewController(message: "Please enter correct Password", inViewController: self)
        }
        else
        {
            if tfOldPassword.text == UserModel.sharedInstance().password
            {
                changePassword()
            }
            else{
                SharedInstance.alertViewController(message: "Please check your old password", inViewController: self)
            }

        }
    }

    @IBAction func btnMenu_Action(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:- webservice method
    func changePassword()
    {
        if CheckReachability.isConnectedToNetwork() == true
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let URLString = "\(Constant.web_url)edit_password.php?user_id=\(UserModel.sharedInstance().user_id!)&password=\(tfNewPassword.text!)&user_type=\(UserModel.sharedInstance().user_type!)"
            Alamofire.request(URLString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    print("Error when calling service")
                    print(response.result.error!)

                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")

                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }
                    else
                    {
                        UserModel.sharedInstance().password = self.tfNewPassword.text
                        UserModel.sharedInstance().synchroniseData()
                        self.navigationController?.popViewController(animated: true)
                    }

                    MBProgressHUD.hide(for:self.view, animated: true)
                }
            }
        }
        else
        {
            MBProgressHUD.hide(for:self.view, animated: true)
            SharedInstance.alertViewController(message: "Please check your internet connection", inViewController: self)
        }
    }
}
