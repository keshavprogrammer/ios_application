//
//  VideoViewController.swift
//  Kakehashi
//
//  Created by keshav on 5/19/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class VideoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var isCheckBtn: UIButton!
    @IBOutlet weak var ImgThumb: UIImageView!
    @IBOutlet weak var btnPlayTap: UIButton!
}

class VideoViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    //MARK:- IBOutlet
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var PopupFilterVw: UIView!
    @IBOutlet weak var btnChangeType: UIButton!
    @IBOutlet var tblList: UITableView!
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    @IBOutlet weak var VideoCollectionView: UICollectionView!
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet var btnDeleteWidth: NSLayoutConstraint!
    var filter = String()

    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )

    let reuseIdentifier = "VideoCell"
    var arrVideoList = Array<Dictionary<String, AnyObject>>()
    var VideoTapData = [String]()
    var selcetVideoId = String()
    var AlbumId = String()
    var screenType = String()
    var screen_title = String()
    var isFromSearch = String()

    var startcounter = Int()
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        filter = "new"
        tblList.isHidden = true
        btnChangeType.setImage(#imageLiteral(resourceName: "listView"), for: .normal)
        tblList.register(UINib(nibName: "ListDetailCell", bundle: nil), forCellReuseIdentifier: "listDetail")
        //tblList.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "list")
        tblList.tableFooterView = UIView()
        
        setlayout()

        self.lblScreenTitle.text = screen_title
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true

        if screenType == "fromplaylist"
        {
            if isFromSearch == "yes"
            {
                addToRecentSearch(type: "playlist", post_id: AlbumId)
            }
            
            startcounter = 0
            GetVideos(playlist_id: AlbumId, start: startcounter, filterType: "\(filter)")
            
            let weakSelf:VideoViewController = self
            VideoCollectionView.addInfiniteScrolling(actionHandler: {() -> Void in
                weakSelf.insertRowAtBottomOfSearch()
            })
            tblList.addInfiniteScrolling(actionHandler: {() -> Void in
                weakSelf.insertRowAtBottomOfSearch()
            })
        }
        else
        {
            
            if isFromSearch == "yes"
            {
                addToRecentSearch(type: "album", post_id: AlbumId)
            }
            
            startcounter = 0
            GetVideoList(album_id: AlbumId, start: startcounter, filterType: "\(filter)")
            
            let weakSelf:VideoViewController = self
            VideoCollectionView.addInfiniteScrolling(actionHandler: {() -> Void in
                weakSelf.insertRowAtBottomOfSearch()
            })
            tblList.addInfiniteScrolling(actionHandler: {() -> Void in
                weakSelf.insertRowAtBottomOfSearch()
            })
        }
    }

    //MARK:- pagination methods
    func insertRowAtBottomOfSearch()
    {
        if screenType == "fromplaylist"
        {
            startcounter += 20
            
            GetVideos(playlist_id: AlbumId, start: startcounter, filterType: "\(filter)")
            self.VideoCollectionView.infiniteScrollingView.stopAnimating()
            self.tblList.infiniteScrollingView.stopAnimating()
        }
        else
        {
            startcounter += 20
            
            GetVideoList(album_id: AlbumId, start: startcounter, filterType: "\(filter)")
            self.VideoCollectionView.infiniteScrollingView.stopAnimating()
            self.tblList.infiniteScrollingView.stopAnimating()
        }
    }
    
    // MARK: - other methods
    func setlayout() {

        if UserModel.sharedInstance().user_type! == "provider"
        {
           // self.btnDeleteOutlet.isHidden = false
            self.btnDeleteWidth.constant = 30
        }
        else
        {
            if screenType == "fromplaylist"
            {
                //self.btnDeleteOutlet.isHidden = false
                self.btnDeleteWidth.constant = 30
            }
            else
            {
                self.btnDeleteOutlet.isHidden = true
                self.btnDeleteWidth.constant = 0
            }
        }

        VideoCollectionView.collectionViewLayout = columnLayout
        if #available(iOS 11.0, *) {
            VideoCollectionView.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
        }
        
        self.btnChangeType.layer.cornerRadius = self.btnChangeType.frame.size.height/2
        self.btnChangeType.clipsToBounds = true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        self.view.endEditing(true)
    }

    // MARK: - UICollectionViewDataSource & UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if (self.arrVideoList.count == 0) {
            self.VideoCollectionView.setEmptyMessage("")
        } else {
            self.VideoCollectionView.restore()
        }
        return self.arrVideoList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = VideoCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! VideoCollectionViewCell

        let thumImgUrl = arrVideoList[indexPath.row]["video_image"] as! String

        cell.ImgThumb.sd_setImage(with: NSURL(string: thumImgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)

        cell.lblTitle.text = arrVideoList[indexPath.row]["video_title"] as? String

        if UserModel.sharedInstance().user_type! == "provider"
        {
            cell.isCheckBtn.isHidden = false
            cell.lblSubTitle.text = "by \(UserModel.sharedInstance().fullname!)"
        }
        else
        {
            if screenType == "fromplaylist"
            {
                cell.isCheckBtn.isHidden = false
                cell.lblSubTitle.text = "by \(arrVideoList[indexPath.row]["provider_name"] as! String)"
            }
            else
            {
                cell.isCheckBtn.isHidden = true
                cell.lblSubTitle.text = "by \(arrVideoList[indexPath.row]["provider_name"] as! String)"
            }
        }


        let TapCheck = VideoTapData[indexPath.row]

        if TapCheck=="YES"
        {

            cell.isCheckBtn.isSelected = true
        }
        else
        {
            cell.isCheckBtn.isSelected = false
        }

        cell.isCheckBtn.tag = indexPath.row
        cell.isCheckBtn.addTarget(self, action: #selector(btnCheckTap(_:)), for: .touchUpInside)

        cell.btnPlayTap.tag = indexPath.row
        cell.btnPlayTap.addTarget(self, action: #selector(btnDetailView(_:)), for: .touchUpInside)

        return cell
    }

    @IBAction func btnDetailView(_ sender: UIButton)
    {
        let myIP = IndexPath(row: sender.tag, section: 0)
        self.performSegue(withIdentifier: "videoDetail", sender: myIP)
    }

    @IBAction func btnCheckTap(_ sender: Any)
    {
        let btn = sender as! UIButton
        let myIP = IndexPath(row: btn.tag, section: 0)
        let cell: VideoCollectionViewCell? = (VideoCollectionView.cellForItem(at: myIP) as? VideoCollectionViewCell)

        let TapCheck = VideoTapData[myIP.row]

        if TapCheck=="YES"
        {
            cell?.isCheckBtn.isSelected = false
            VideoTapData[myIP.row] = "NO"
        }
        else
        {
            cell?.isCheckBtn.isSelected = true
            VideoTapData[myIP.row]="YES"
        }
    }

    // MARK: - Uibutton Actions
    @IBAction func btnChangeViewTap(_ sender: Any) {
        
        if tblList.isHidden
        {
            tblList.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
            VideoCollectionView.isHidden = true
            tblList.isHidden = false
            btnChangeType.setImage(#imageLiteral(resourceName: "gridView"), for: .normal)
        }
        else
        {
            VideoCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
            VideoCollectionView.isHidden = false
            tblList.isHidden = true
            btnChangeType.setImage(#imageLiteral(resourceName: "listView"), for: .normal)
        }
    }
    
    @IBAction func btnMenuTap(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func BtnFilterTap(_ sender: Any) {
        
        if PopupFilterVw.isHidden
        {
            self.imgBlur.isHidden = false
            PopupFilterVw.isHidden = false
        }
        else{
            
            self.imgBlur.isHidden = true
            PopupFilterVw.isHidden = true
        }
    }
    
    @IBAction func btnDeleteVideos(_ sender: Any) {

        self.selcetVideoId = ""

        for i in 0..<VideoTapData.count {

            let TapCheck = VideoTapData[i]

            if TapCheck=="YES"
            {
                self.selcetVideoId = "\(self.selcetVideoId)"+"\(arrVideoList[i]["video_id"] as! String),"
            }
        }

        selcetVideoId = String(selcetVideoId.characters.dropLast())


        if selcetVideoId.isEmpty
        {
            SharedInstance.alertViewController(message: "Please select the video", inViewController: self)
        }
        else
        {
            if UserModel.sharedInstance().user_type! == "provider"
            {

                // delete provider video
                let logoutAlert = UIAlertController(title: Constant.PROJECT_NAME as String, message: "Are you sure that you want to delete the selected Video?", preferredStyle: UIAlertControllerStyle.alert)

                logoutAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in

                    self.DeleteMultipleVideos(Video_ids: self.selcetVideoId)
                }))

                logoutAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in

                }))

                present(logoutAlert, animated: true, completion: nil)
            }
            else
            {
                //deleteplaylist
                let logoutAlert = UIAlertController(title: Constant.PROJECT_NAME as String, message: "Are you sure that you want to delete the selected Video?", preferredStyle: UIAlertControllerStyle.alert)

                logoutAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in

                    self.DeletePlaylistVideos(Video_ids: self.selcetVideoId, playlist_id: self.AlbumId)
                }))

                logoutAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in

                }))

                present(logoutAlert, animated: true, completion: nil)
            }
        }
    }

    @IBAction func BtnOldToNewTap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        filter = "old"
        if screenType == "fromplaylist"
        {
            startcounter = 0
            GetVideos(playlist_id: AlbumId, start: startcounter, filterType: "\(filter)")
        }
        else
        {
            startcounter = 0
            GetVideoList(album_id: AlbumId, start: startcounter, filterType: "\(filter)")
        }
        //"old"
    }
    
    @IBAction func btnNewToOldTap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        filter = "new"
        if screenType == "fromplaylist"
        {
            startcounter = 0
            GetVideos(playlist_id: AlbumId, start: startcounter, filterType: "\(filter)")
        }
        else
        {
            startcounter = 0
            GetVideoList(album_id: AlbumId, start: startcounter, filterType: "\(filter)")
        }
       //"new"
    }
    
    @IBAction func btnAtoZTap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        filter = "atoz"
        if screenType == "fromplaylist"
        {
            startcounter = 0
            GetVideos(playlist_id: AlbumId, start: startcounter, filterType: "\(filter)")
        }
        else
        {
            startcounter = 0
            GetVideoList(album_id: AlbumId, start: startcounter, filterType: "\(filter)")
        }
       //"atoz"
    }
    
    @IBAction func btnZtoATap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        filter = "ztoa"
        if screenType == "fromplaylist"
        {
            startcounter = 0
            GetVideos(playlist_id: AlbumId, start: startcounter, filterType: "\(filter)")
        }
        else
        {
            startcounter = 0
            GetVideoList(album_id: AlbumId, start: startcounter, filterType: "\(filter)")
        }
        //"ztoa"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if (segue.identifier == "videoDetail") {

            let controller =  segue.destination as! VideoDetailViewController
            let row = (sender as! NSIndexPath).row
            controller.videodetail = arrVideoList[row]
        }
    }
    
    //MARK:- Web service called
    func GetVideoList(album_id:String, start: Int, filterType:String){
        
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if start == 0
        {
            self.arrVideoList.removeAll()
            self.VideoTapData.removeAll()
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        
        let url = "video_list.php?album_id=\(album_id)&start=\(start)&type=\(filterType)"
        
        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET video_list.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                        if start == 0
                        {
                            SharedInstance.alertViewController(message: strMessage, inViewController: self)
                            
                            self.arrVideoList.removeAll()
                            self.VideoTapData.removeAll()
                        }
                    }
                    else
                    {
                        
                        let responseData = json["responseData"] as! [[String:AnyObject]]
                        
                        if start == 0
                        {
                            self.arrVideoList.removeAll()
                            self.VideoTapData.removeAll()
                        }
                        
                        for i in 0..<responseData.count
                        {
                           self.arrVideoList.append(responseData[i])
                        }
                        
                        for _ in 0..<self.arrVideoList.count {
                            
                            let selectType = "NO"
                            self.VideoTapData.append(selectType)
                        }
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                    }
                    
                    self.VideoCollectionView.reloadData()
                    self.tblList.reloadData()
                    
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }
    
    func Getview_album(album_id:String){
        
        let url = "view_album.php?album_id=\(album_id)"
        
        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET view_album.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        //let strMessage = json["message"] as! String
                        
                    }
                    else
                    {
                        
                        print(strResult)
                        //let strMessage = json["message"] as! String
                        
                    }
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }
    
    func GetVideos(playlist_id:String,start: Int, filterType:String){
        
        if start == 0
        {
            self.arrVideoList.removeAll()
            self.VideoTapData.removeAll()
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        
        let url = "get_playlist.php?user_id=\(UserModel.sharedInstance().user_id!)&playlist_id=\(playlist_id)&start=\(start)&type=\(filterType)"
        
        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET get_playlist.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                        if start == 0
                        {
                            self.arrVideoList.removeAll()
                            self.VideoTapData.removeAll()
                            
                            SharedInstance.alertViewController(message: strMessage, inViewController: self)
                        }
                    }
                    else
                    {
                        
                        let responseData = json["responseData"] as! [[String:AnyObject]]
                        
                        if start == 0
                        {
                            self.arrVideoList.removeAll()
                            self.VideoTapData.removeAll()
                        }
                        
                        for i in 0..<responseData.count
                        {
                             self.arrVideoList.append(responseData[i])
                        }
                    
                        for _ in 0..<self.arrVideoList.count {
                            
                            let selectType = "NO"
                            self.VideoTapData.append(selectType)
                        }
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                    }
                    
                    self.VideoCollectionView.reloadData()
                    self.tblList.reloadData()
                    
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }
    
    func DeleteMultipleVideos(Video_ids:String){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let url = "delete_video.php?video_id=\(Video_ids)&user_id=\(UserModel.sharedInstance().user_id!)"
        
        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET delete_video.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }
                    else
                    {
                        let strMessage = json["message"] as! String
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                        self.startcounter = 0
                        self.GetVideoList(album_id: self.AlbumId, start: self.startcounter, filterType: "\(self.filter)")
                    }
                    
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
        
    }
    
    func DeletePlaylistVideos(Video_ids:String,playlist_id:String){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let url = "delete_playlist_video.php?user_id=\(UserModel.sharedInstance().user_id!)&playlist_id=\(AlbumId)&video_id=\(Video_ids)"
        
        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET delete_playlist_video.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }
                    else
                    {
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                        self.startcounter = 0
                        
                        self.GetVideos(playlist_id: self.AlbumId, start: self.startcounter, filterType: "\(self.filter)")
                    }
                    
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
        
    }
    
    func addToRecentSearch(type: String, post_id: String){
        
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if CheckReachability.isConnectedToNetwork() == true
        {
            let url = "add_recent_search.php?user_id=\(UserModel.sharedInstance().user_id!)&type=\(type)&type_id=\(post_id)&user_type=\(UserModel.sharedInstance().user_type!)"
            
            Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON { response in
                    
                    // check for errors
                    guard response.result.error == nil else {
                        // got an error in getting the data, need to handle it
                        print("error calling GET addToRecentSearch")
                        print(response.result.error!)
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        return
                    }
                    
                    // make sure we got some JSON since that's what we expect
                    guard let json = response.result.value as? [String: Any] else {
                        print("didn't get todo object as JSON from API")
                        print("Error: \(String(describing: response.result.error))")
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        return
                    }
                    
                    if let strResult = json["status"] as? String
                    {
                        if strResult == "0"
                        {
                            print(strResult)
                            let strMessage = json["message"] as! String
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        }
                        else
                        {
                            let strMessage = json["message"] as! String
                            print(strMessage)
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        }
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    }
            }
        }
        else
        {
            print("no internet connection")
        }
    }
}

class ColumnFlowLayout: UICollectionViewFlowLayout {

    let cellsPerRow: Int

    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()

        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepare() {
        super.prepare()

        guard let collectionView = collectionView else { return }

        if #available(iOS 11.0, *) {
            let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)

            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)

            itemSize = CGSize(width: itemWidth, height: itemWidth*0.6+100)
        } else {
            // Fallback on earlier versions
        }
    }

    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }

}

extension VideoViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrVideoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblList.dequeueReusableCell(withIdentifier: "listDetail", for: indexPath) as! ListDetailCell
        
        //let cell = tblList.dequeueReusableCell(withIdentifier: "list", for: indexPath) as! ListTableViewCell
        
        let thumImgUrl = arrVideoList[indexPath.row]["video_image"] as! String
        cell.imgThumb.sd_setImage(with: NSURL(string: thumImgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)
        
        cell.lblTitle.text = arrVideoList[indexPath.row]["video_title"] as? String
        
//        if screenType == "fromplaylist"
//        {
//            cell.lblNoSong.isHidden = true
//        }
        
        if UserModel.sharedInstance().user_type! == "provider"
        {
            cell.isCheckBtn.isHidden = false
            cell.lblNoSong.isHidden = true
            cell.lblSubTitle.text = "by \(UserModel.sharedInstance().fullname!)"
        }
        else
        {
            if screenType == "fromplaylist"
            {
                cell.isCheckBtn.isHidden = false
                cell.lblNoSong.isHidden = true
                cell.lblSubTitle.text = "by \(arrVideoList[indexPath.row]["provider_name"] as! String)"
            }
            else
            {
                cell.isCheckBtn.isHidden = true
                cell.lblNoSong.isHidden = true
                cell.lblSubTitle.text = "by \(arrVideoList[indexPath.row]["provider_name"] as! String)"
            }
        }
        
        let TapCheck = VideoTapData[indexPath.row]
        
        if TapCheck=="YES"
        {
            
            cell.isCheckBtn.isSelected = true
        }
        else
        {
            cell.isCheckBtn.isSelected = false
        }
        
        cell.isCheckBtn.tag = indexPath.row
        cell.isCheckBtn.addTarget(self, action: #selector(btnCheckTap(_:)), for: .touchUpInside)
        
        cell.btnPlayTap.tag = indexPath.row
        cell.btnPlayTap.addTarget(self, action: #selector(btnDetailView(_:)), for: .touchUpInside)
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "videoDetail", sender: indexPath)
    }
}

