//
//  LoginVC.swift
//  Kakehashi
//
//  Created by Keshav Infotech on 19/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit
import Alamofire

struct INSTAGRAM_IDS {
    
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    
    static let INSTAGRAM_CLIENT_ID  = "f9561f120744407b83f06043cb622c8c"
    
    static let INSTAGRAM_CLIENTSERCRET = "1a06bfd00d35408da52cf0259564413b"
    
    static let INSTAGRAM_REDIRECT_URI = "http://keshavinfotech.com"
    
    static var INSTAGRAM_ACCESS_TOKEN =  "access_token"
    
    static let INSTAGRAM_SCOPE = "likes+comments+relationships"
    
}

class LoginVC: UIViewController,UITextFieldDelegate {

    //MARK:- Outlets
    var dict = [String : String]()
    var userType = String()
    @IBOutlet var tfEmailAddress: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet weak var loginIndicator: UIActivityIndicatorView!
   
    @IBOutlet weak var webViewInsta: UIWebView!
    @IBOutlet weak var tvLinks: UITextView!
    @IBOutlet weak var lblLinks: UILabel!
    @IBOutlet weak var lblForgot: UILabel!
    
    @IBOutlet var vwPopUp: UIView!
    @IBOutlet var blurView: UIImageView!
    @IBOutlet var btnUser: UIButton!
    @IBOutlet var btnContentProvider: UIButton!
    
    @IBOutlet weak var btnJapanese: UIButton!
    @IBOutlet weak var btnEnglish: UIButton!
    
    let typeOfAuthentication = String()

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        webViewInsta.isHidden = true
        loginIndicator.isHidden = true
        setLayout()
        
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleForgotPassword(_:)))
        lblForgot.addGestureRecognizer(tapGesture)
        lblForgot.isUserInteractionEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    //MARK:- Other Methods
    func setLayout(){

        tfEmailAddress.delegate = self
        tfPassword.delegate = self

        let mutAttrString : NSMutableAttributedString = NSMutableAttributedString()
        
        let attrString_2 : NSAttributedString = NSAttributedString(string: "Terms and Condition", attributes: [
            NSAttributedStringKey(rawValue: "Terms"):"clickable",
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 12),
            NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        
        let attrString_3 : NSAttributedString = NSAttributedString(string: " and ", attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 12)])
        
        let attrString_4 : NSAttributedString = NSAttributedString(string: "Privacy Policy", attributes: [
            NSAttributedStringKey(rawValue: "privacy"):"clickable",
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 12),
            NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        
        mutAttrString.append(attrString_2)
        mutAttrString.append(attrString_3)
        mutAttrString.append(attrString_4)
        
        mutAttrString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: NSRange(location:0,length:38) )
        
        tvLinks.attributedText = mutAttrString
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tvLinks.addGestureRecognizer(tapGesture)
        
        tfEmailAddress.layer.cornerRadius = 5
        tfEmailAddress.clipsToBounds = true
        setBorder(tfEmailAddress, 1.0, UIColor.white)
        
        tfPassword.layer.cornerRadius = 5
        tfPassword.clipsToBounds = true
        setBorder(tfPassword, 1.0, UIColor.white)
        
        btnLogin.layer.cornerRadius = 5
        btnLogin.clipsToBounds = true
        
        btnSignUp.layer.cornerRadius = 5
        btnSignUp.clipsToBounds = true
        
        tfEmailAddress.attributedPlaceholder = NSAttributedString(string:"Email Address", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        tfPassword.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        let paddingView1 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: self.tfEmailAddress.frame.size.height))
        tfEmailAddress.leftView = paddingView1
        tfEmailAddress.leftViewMode = .always
        
        let paddingView2 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: self.tfPassword.frame.size.height))
        tfPassword.leftView = paddingView2
        tfPassword.leftViewMode = .always
        
        blurView.isHidden = true
        vwPopUp.isHidden = true
        
        vwPopUp.layer.cornerRadius = 20
        vwPopUp.clipsToBounds = true
    }
    
    func setBorder(_ objName:AnyObject, _ borderWidth:CGFloat, _ borderColor:UIColor){
        objName.layer.borderColor = borderColor.cgColor
        objName.layer.borderWidth = borderWidth
    }
    
    //MARK:- Button Action
    @IBAction func btnLogin_Action(_ sender: Any) {
        
        if tfEmailAddress.text != ""{
            if (tfEmailAddress.text?.isEmail)!{
                if tfPassword.text != ""{

//                    if (CommonFunction().isValidPassword(testStr: tfPassword.text!))
//                    {
                        login()
                    //}
//                    else
//                    {
//                        SharedInstance.alertViewController(message: "Please enter minimum 6 characters in password", inViewController: self)
//                    }
                }
                else
                {
                   self.tfPassword.becomeFirstResponder()
                   SharedInstance.alertViewController(message: "Please enter password", inViewController: self)
                }
            }
            else
            {
                self.tfEmailAddress.becomeFirstResponder()
                SharedInstance.alertViewController(message: "Please enter valid email address", inViewController: self)
            }
        }
        else
        {
            self.tfEmailAddress.becomeFirstResponder()
            SharedInstance.alertViewController(message: "Please enter email address", inViewController: self)
        }
    }

    @IBAction func btnFBLogin_Action(_ sender: Any)
    {
        //self.LoginWithFB()
    }
    
    @IBAction func btnTwitterLogin_Action(_ sender: Any) {

//        dict.removeAll()
//        TWTRTwitter.sharedInstance().logIn {
//            (session, error) -> Void in
//            if (session != nil) {
//                if let unwrappedsession = session{
//
//                    let client = TWTRAPIClient(userID: unwrappedsession.userID)
//
//                    client.loadUser(withID: (unwrappedsession.userID), completion: { (user, error) in
//                        self.dict["first_name"] = (user?.name)!
//                        self.dict["username"] = unwrappedsession.userName
//                        self.dict["twitterid"] = unwrappedsession.userID
//                        self.dict["twitter_token"] = unwrappedsession.authToken
//                        self.dict["profilepicture"] = (user?.profileImageLargeURL)!
//                        self.dict["user_type"] = self.userType
//                        self.performSegue(withIdentifier: "Signup2", sender: "twitter")
//                    })
//                }
//                else{
//                    print("login error")
//                }
//            }
//
//        }
    }
    
    @IBAction func btnSignUp_Action(_ sender: Any) {
        
        self.performSegue(withIdentifier: "signUp", sender: nil)
        
    }
    
    @IBAction func btnEnglish_Action(_ sender: Any) {
        btnEnglish.setImage(UIImage(named:"select_eng"), for: .normal)
        btnJapanese.setImage(UIImage(named:"deselect_jap"), for: .normal)
    }
    
    @IBAction func btnJapanese_Action(_ sender: Any) {
        
        btnJapanese.setImage(UIImage(named:"select_jap"), for: .normal)
        btnEnglish.setImage(UIImage(named:"deselect_eng"), for: .normal)
        
        SharedInstance.alertViewController(message: "Japanese version is coming soon", inViewController: self)
    }

    //MARK:- textfield delegate method
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        self.view.endEditing(true)
    }

    //MARK:- NavigationBar Delegate Method
    override func prepare(for segue: UIStoryboardSegue, sender: (Any)?) {

        if segue.identifier == "Signup2"{
            let vc = segue.destination as! SocialSignViewController
            vc.dictTwitter = self.dict
            vc.socialMediaType = sender as! String
        }
    }

    //MARK:- webservice method
    func login()
    {
        view.endEditing(true)

        if CheckReachability.isConnectedToNetwork() == true
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let URLString = "\(Constant.web_url)login_with_email.php?email=\(tfEmailAddress.text!)&password=\(tfPassword.text!)"
            Alamofire.request(URLString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    print("Error when calling service")
                    print(response.result.error!)

                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")

                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }
                    else
                    {
                        let responseData = json["responseData"] as! [String:AnyObject]
                        print("Response Data : \(responseData)")

                        UserModel.sharedInstance().password = self.tfPassword.text!
                        UserModel.sharedInstance().user_id = "\(responseData["user_id"]!)"
                        UserModel.sharedInstance().fullname = responseData["full_name"] as? String
                        UserModel.sharedInstance().profile_image = responseData["profile_image"] as? String
                        UserModel.sharedInstance().email = responseData["email_address"] as? String
                        UserModel.sharedInstance().user_type = responseData["user_type"] as? String
                        UserModel.sharedInstance().gender = responseData["gender"] as? String
                        UserModel.sharedInstance().country = responseData["country"] as? String
                        UserModel.sharedInstance().city = responseData["city"] as? String
                        UserModel.sharedInstance().zipcode = responseData["zipcode"] as? String
                        UserModel.sharedInstance().birthdate = responseData["birth_date"] as? String
                        UserModel.sharedInstance().synchroniseData()
                        //user_type = responseData["user_type"] as! String

                        SharedInstance.appDelegate().setSlideRootNavigation()

                    }

                    MBProgressHUD.hide(for:self.view, animated: true)
                }
            }
        }
        else
        {
            MBProgressHUD.hide(for:self.view, animated: true)
            SharedInstance.alertViewController(message: "Please check your internet connection", inViewController: self)
        }
    }


    //MARK:- login With FB
    func LoginWithFB()  {

        let login: FBSDKLoginManager = FBSDKLoginManager()
        // Make login and request permissions
        login.logIn(withReadPermissions: ["email", "public_profile"], from: self, handler: {(result, error) -> Void in

            if error != nil {
                // Handle Error
                NSLog("Process error")
            } else if (result?.isCancelled)!
            {
                // If process is cancel
                NSLog("Cancelled")
            }
            else {
                let parameters = ["fields": "id, name, first_name, last_name,  email, gender, picture.type(large)"]

                FBSDKGraphRequest(graphPath: "me", parameters: parameters).start {(connection, result, error) -> Void in
                    if error != nil
                    {
                        NSLog(error.debugDescription)
                        return
                    }
                    // Result
                    print(result!)
                    //print("accessToken: " , FBSDKAccessToken.current())

                    if let result = result as? [String:AnyObject]{
                        self.dict["email"] = result["email"]! as? String
                        self.dict["accesstoken"] = FBSDKAccessToken.current().tokenString!
                        self.dict["fb_id"] = result["id"]! as? String
                        self.dict["fullname"] = result["name"]! as? String
                        self.dict["profilepicture"] = ((result["picture"] as! [String:AnyObject])["data"] as! [String:AnyObject])["url"] as? String
                        //self.loginWithFBService()

                        self.performSegue(withIdentifier: "Signup2", sender: "fb")
                    }
                }
            }
        })
    }

    @objc func handleTap(_ recognizer: UITapGestureRecognizer) {
        
        let textView: UITextView = recognizer.view as! UITextView
        
        let layoutManager: NSLayoutManager = textView.layoutManager
        var location: CGPoint = recognizer.location(in: textView)
        location.x -= textView.textContainerInset.left
        location.y -= textView.textContainerInset.top
        
        var characterIndex: Int
        characterIndex = layoutManager.characterIndex(for: location, in: textView.textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        if characterIndex < textView.textStorage.length {
            var range : NSRange? = NSMakeRange(0, 1)
            
            let value: AnyObject? = textView.attributedText.attribute(NSAttributedStringKey(rawValue: "Terms"), at: characterIndex, effectiveRange: &range!) as AnyObject?
            if value != nil
            {
                print("Terms of Service ")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "StaticPageVC") as! StaticPageVC
                vc.text = "Terms & Conditions"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            let value1: AnyObject? = textView.attributedText.attribute(NSAttributedStringKey(rawValue: "privacy"), at: characterIndex, effectiveRange: &range!) as AnyObject?
            if value1 != nil
            {
                print("Privacy Policy ")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "StaticPageVC") as! StaticPageVC
                vc.text = "Privacy Policy"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
     @objc func handleForgotPassword(_ recognizer: UITapGestureRecognizer) {
        
        self.performSegue(withIdentifier: "forgot", sender: nil)
    }
    
    @IBAction func btnCancelPopUp(_ sender: Any) {
//        UIView.animate(withDuration: 0.4, animations:{
//            self.blurView.isHidden = true
//            self.vwPopUp.alpha = 0.0
//            self.vwPopUp.transform = CGAffineTransform.identity
//        }) { (finish) in
//            self.vwPopUp.transform =  CGAffineTransform.init(scaleX : 1.0 ,y:1.0)
//        }
    }
    
    @IBAction func btnDonePopUp(_ sender: Any) {

//        UIView.animate(withDuration: 0.4, animations:{
//            self.blurView.isHidden = true
//            self.vwPopUp.alpha = 0.0
//            self.vwPopUp.transform = CGAffineTransform.identity
//        }) { (finish) in
//            self.vwPopUp.transform =  CGAffineTransform.init(scaleX : 1.0 ,y:1.0)
//        }


    }
    
    @IBAction func btnUserSelectPopUP(_ sender: Any) {
        
        btnUser.setImage(UIImage(named: "radio-on"), for: .normal)
        btnContentProvider.setImage(UIImage(named: "radio-off"), for: .normal)

        userType = "user"

    }
    @IBAction func btnContentProviderSelectPopUP(_ sender: Any) {
        
        btnUser.setImage(UIImage(named: "radio-off"), for: .normal)
        btnContentProvider.setImage(UIImage(named: "radio-on"), for: .normal)

        userType = "provider"
    }
    
    @IBAction func btnInstagramLogin_Action(_ sender: Any) {
        webViewInsta.delegate = self
        webViewInsta.isHidden = false
        unSignedRequest()
    }
    
    //MARK: - unSignedRequest
    func unSignedRequest () {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        webViewInsta.loadRequest(urlRequest)
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        print(requestURLString)
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        } else if requestURLString == "about:blank" && INSTAGRAM_IDS.INSTAGRAM_ACCESS_TOKEN != "access_token"{
            self.unSignedRequest()
        }
        return true
    }
    
    func handleAuth(authToken: String)  {
        print("Instagram authentication token ==", authToken)
        INSTAGRAM_IDS.INSTAGRAM_ACCESS_TOKEN = authToken
        getUser()
    }
    
    func getUser() {
        //https://api.instagram.com/v1/users/self/?access_token=ACCESS-TOKEN
        let url = "https://api.instagram.com/v1/users/self/?access_token=\(INSTAGRAM_IDS.INSTAGRAM_ACCESS_TOKEN)"
        var request: URLRequest? = nil
        if let anUrl = URL(string: url) {
            request = URLRequest(url: anUrl)
            request?.httpMethod = "GET"
            let session = URLSession.shared
            let task = session.dataTask(with: request!, completionHandler: {data, response, error -> Void in
                print(response as Any)
                if response != nil && data != nil{
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        print(jsonResult as Any)
                        
                        DispatchQueue.main.async {
                            self.webViewInsta.isHidden = true
                            self.loginIndicator.isHidden = true
                            self.loginIndicator.stopAnimating()
                            let story = UIStoryboard(name: "Main", bundle: nil)
                            let vc = story.instantiateViewController(withIdentifier: "SocialSignViewController") as! SocialSignViewController
                            vc.dictInsta = (jsonResult as! [String: AnyObject])["data"] as! [String: AnyObject]
                            vc.dictInsta["insta_token"] = INSTAGRAM_IDS.INSTAGRAM_ACCESS_TOKEN as AnyObject
                            vc.socialMediaType = "insta"
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }
            })
            task.resume()
        }
    }

}

extension LoginVC: UIWebViewDelegate{
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        loginIndicator.isHidden = false
        loginIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loginIndicator.isHidden = true
        loginIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }
}
