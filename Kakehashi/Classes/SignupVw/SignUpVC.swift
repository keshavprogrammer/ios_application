//
//  SignUpVC.swift
//  Kakehashi
//
//  Created by Keshav on 23/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import Alamofire

class SignUpVC: UIViewController, DropDownViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAccountName: UITextField!
    @IBOutlet weak var txtAccountType: UITextField!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblLink: UILabel!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var vwDropdown: DropDownView!
    let userTypes = ["user","content provider"]
    
    //MARK:- webservice method
    func signUp()
    {
         view.endEditing(true)

        if CheckReachability.isConnectedToNetwork() == true
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            var imgData = Data()
            
            if imgProfile.image == UIImage(named: "placeholder")
            {
                imgData = Data()
            }
            else{
                imgData = UIImageJPEGRepresentation(imgProfile.image!, 0.5)!
            }
            
            var typeUser = String()
            if txtAccountType.text == "content provider"
            {
                typeUser = "provider"
            }
            else
            {
                typeUser = "user"
            }
            
            let  parameters: [String:String]
            parameters = ["user_type":"\(typeUser)","full_name":"\(txtAccountName.text!)","email_address":"\(txtEmail.text!)","password":"\(txtPassword.text!)"]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "profile_image",fileName: "image_post.png", mimeType: "image/png")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            },
                             to:"\(Constant.web_url)signup.php")
            { (result) in
                switch result {
                    
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        
                        guard let json = response.result.value as? [String: Any] else {
                            print("didn't get todo object as JSON from API")
                            print("Error: \(String(describing: response.result.error))")
                            
                            return
                        }
                        
                        if let strResult = json["status"] as? String
                        {
                            if strResult == "0"
                            {
                                let strMessage = json["message"] as! String
                                print(strMessage)
                                SharedInstance.alertViewController(message: strMessage, inViewController: self)

                            }
                            else
                            {
    
                                let responseData = json["responseData"] as! [String:AnyObject]
                                print("Response Data : \(responseData)")

                                self.txtAccountType.text = ""
                                self.txtAccountName.text = ""
                                self.txtEmail.text = ""
                                self.txtPassword.text = ""

                                let strMessage = json["message"] as! String
                                print(strMessage)

                                let alertController = UIAlertController(title: Constant.PROJECT_NAME as String, message: "Your account has been created successfully you can now log in", preferredStyle: .alert)

                                let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in

                                    self.navigationController?.popViewController(animated: true)
                                }

                                alertController.addAction(action1)

                                self.present(alertController, animated: true, completion: nil)

//                                UserModel.sharedInstance().profile_image = "\(responseData["profile_image"]!)"
//                                UserModel.sharedInstance().user_id = "\(responseData["user_id"]!)"
//                                UserModel.sharedInstance().fullname = responseData["full_name"] as? String
//                                UserModel.sharedInstance().profile_image = responseData["profile_image"] as? String
//                                UserModel.sharedInstance().email = responseData["email_address"] as? String
//                                UserModel.sharedInstance().user_type = responseData["user_type"] as? String
//                                UserModel.sharedInstance().password = self.txtPassword.text!
//
//                                UserModel.sharedInstance().synchroniseData()
//
//                                SharedInstance.appDelegate().setSlideRootNavigation()
                            }
                            
                            MBProgressHUD.hide(for:self.view, animated: true)
                        }
                    }
                case .failure(_):
                    
                    MBProgressHUD.hide(for:self.view, animated: true)
                }
            }
        }
        else
        {
            MBProgressHUD.hide(for:self.view, animated: true)
            SharedInstance.alertViewController(message: "Please check your internet connection", inViewController: self)
        }
        
    }

    //MARK:- view lifecycle method
    override func viewDidLoad() {
        super.viewDidLoad()

        setLayout()
        
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        lblLink.addGestureRecognizer(tapGesture)
        
        let tapGesture2 : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.openImagePicker(_:)))
        imgProfile.addGestureRecognizer(tapGesture2)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- UIAction method
    func setLayout(){

        txtAccountType.setPaddingImage(#imageLiteral(resourceName: "White-dropdown"), SetPaddingType: .rightSide)

        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.height/2
            self.imgProfile.clipsToBounds = true
            self.imgProfile.contentMode = .scaleAspectFill
        }

        txtAccountName.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtAccountName.delegate = self

        txtAccountType.layer.cornerRadius = 5
        txtAccountType.clipsToBounds = true
        setBorder(txtAccountType, 1.0, UIColor.white)
        
        txtAccountName.layer.cornerRadius = 5
        txtAccountName.clipsToBounds = true
        setBorder(txtAccountName, 1.0, UIColor.white)
        
        txtEmail.layer.cornerRadius = 5
        txtEmail.clipsToBounds = true
        setBorder(txtEmail, 1.0, UIColor.white)
        
        txtPassword.layer.cornerRadius = 5
        txtPassword.clipsToBounds = true
        setBorder(txtPassword, 1.0, UIColor.white)
        
        btnCancel.layer.cornerRadius = 5
        btnCancel.clipsToBounds = true
        
        btnSignUp.layer.cornerRadius = 5
        btnSignUp.clipsToBounds = true
        
        txtAccountType.attributedPlaceholder = NSAttributedString(string:"Account Type", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        txtAccountName.attributedPlaceholder = NSAttributedString(string:"Account Name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Email Address", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        txtPassword.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        let paddingView1 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: self.txtAccountType.frame.size.height))
        txtAccountType.leftView = paddingView1
        txtAccountType.leftViewMode = .always
        
        let paddingView2 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: self.txtAccountName.frame.size.height))
        txtAccountName.leftView = paddingView2
        txtAccountName.leftViewMode = .always
        
        let paddingView3 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: self.txtEmail.frame.size.height))
        txtEmail.leftView = paddingView3
        txtEmail.leftViewMode = .always
        
        let paddingView4 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: self.txtPassword.frame.size.height))
        txtPassword.leftView = paddingView4
        txtPassword.leftViewMode = .always
    }
    
    func setBorder(_ objName:AnyObject, _ borderWidth:CGFloat, _ borderColor:UIColor){
        objName.layer.borderColor = borderColor.cgColor
        objName.layer.borderWidth = borderWidth
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {

        if self.vwDropdown == nil
        {

        }
        else
        {
            vwDropdown.closeAnimation()
        }

        txtAccountType.text = ""
        txtAccountName.text = ""
        txtEmail.text = ""
        txtPassword.text = ""

        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        
        if txtAccountType.text != ""
        {
            if txtAccountName.text != ""
            {
                if txtEmail.text != ""
                {
                    if (txtEmail.text?.isEmail)!
                    {
                        if txtPassword.text != ""
                        {
                            if (CommonFunction().isValidPassword(testStr: txtPassword.text!))
                            {
                                signUp()
                            }
                            else
                            {
                                self.txtPassword.becomeFirstResponder()
                                SharedInstance.alertViewController(message: "Please enter minimum 6 characters in password", inViewController: self)
                            }
                        }
                        else
                        {
                            self.txtPassword.becomeFirstResponder()
                            SharedInstance.alertViewController(message: "Please enter password", inViewController: self)
                        }
                    }
                    else
                    {
                        self.txtPassword.becomeFirstResponder()
                        SharedInstance.alertViewController(message: "Please enter valid email address", inViewController: self)
                    }
                }
                else
                {
                    self.txtEmail.becomeFirstResponder()
                   SharedInstance.alertViewController(message: "Please enter email address", inViewController: self)
                }
            }
            else
            {
                self.txtAccountName.becomeFirstResponder()
                SharedInstance.alertViewController(message: "Please enter account name", inViewController: self)
            }
        }
        else
        {
            self.txtAccountType.becomeFirstResponder()
            SharedInstance.alertViewController(message: "Please select account type", inViewController: self)
        }
    }
    
    //MARK:- tap gesture method
    @objc func handleTap(_ recognizer: UITapGestureRecognizer) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func openImagePicker(_ recognizer: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        ImagePicker.shared.showImagePicker(viewController1: self, imageView: self.imgProfile)
    }
    
    //MARK:- textfield delegate method
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtAccountType
        {
            if self.vwDropdown == nil
            {
                
                self.vwDropdown = DropDownView.init(arrayData: userTypes, cellHeight: 30, heightTableView: 60, paddingTop: 3, paddingLeft: 3, paddingRight: 3, refView: textField as UIView, animation: BLENDIN, openAnimationDuration: 0.1, closeAnimationDuration: 1)
                
                self.vwDropdown.delegate = self
                self.view.addSubview(self.vwDropdown.view)
                
            }
            else
            {
                self.vwDropdown.view.removeFromSuperview()
                self.vwDropdown = nil
                
                self.vwDropdown = DropDownView.init(arrayData: userTypes, cellHeight: 30, heightTableView: 60, paddingTop: 3, paddingLeft: 3, paddingRight: 3, refView: textField as UIView, animation: BLENDIN, openAnimationDuration: 0.1, closeAnimationDuration: 1)
                
                self.vwDropdown.delegate = self
                self.view.addSubview(self.vwDropdown.view)
                self.vwDropdown.openAnimation()
            }
            return false
        }
        else
        {

            if vwDropdown != nil{
                self.vwDropdown.closeAnimation()
            }
            return true
        }
        
        //return true
    }

    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        self.view.endEditing(true)

        if vwDropdown != nil{
            self.vwDropdown.closeAnimation()
        }
    }

    // MARK: - DropDown Delegate
    func dropDownCellSelected(_ returnIndex: Int, in viewDropDown: UIView!) {
        
        if returnIndex==0 {
            
            txtAccountType.text = "user"
        }
        else
        {
            txtAccountType.text = "content provider"
        }
        
        viewDropDown.removeFromSuperview()
    }
}
