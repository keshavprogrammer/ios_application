//
//  DeletePopupVC.swift
//  Kakehashi
//
//  Created by Keshav Infotech on 21/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit

class DeletePopupVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet var vwMain: UIView!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnYes: UIButton!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Other Method
    func setLayout(){
        vwMain.layer.cornerRadius = 12
        vwMain.clipsToBounds = true
        
        btnYes.layer.cornerRadius = 8
        btnYes.clipsToBounds = true
        
        btnCancel.layer.cornerRadius = 8
        btnCancel.clipsToBounds = true
    }
}
