//
//  SettingViewController.swift
//  Kakehashi
//
//  Created by keshav on 5/19/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit

class SettingViewCell: UITableViewCell {
    @IBOutlet var ivIcon: UIImageView!
    @IBOutlet var lblText: UILabel!
    @IBOutlet var btnDetail: UIButton!
}

class SettingViewCell1: UITableViewCell {

    @IBOutlet var ivIcon: UIImageView!
    @IBOutlet var lblText: UILabel!
    @IBOutlet weak var btnJapanese: UIButton!
    @IBOutlet weak var btnEnglish: UIButton!
    
    @IBAction func japaneseAction(_ sender: Any) {
        
        btnEnglish.setImage(UIImage(named:"setting_desel_eng"), for: .normal)
        btnJapanese.setImage(UIImage(named:"setting_sel_jap"), for: .normal)
    }
    
    @IBAction func englishAction(_ sender: Any) {
        
        btnEnglish.setImage(UIImage(named:"setting_sel_eng"), for: .normal)
        btnJapanese.setImage(UIImage(named:"setting_desel_jap"), for: .normal)
    }
    
}

class SettingViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tblSetting: UITableView!

    //MARK:- Global Variables
    let arrImages = ["Info", "Lock_Screen", "Language", "Terms", "Privacy", "Info"]
    let arrText = ["About Us", "Change Password", "Language", "Terms and condition", "Privacy policy", "Contact Us"]
    
    //MARK:- View Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblSetting.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func btnSideMenu_Action(_ sender: Any)
    {

        self.navigationController?.sideMenu.toggleLeftSideMenu()
    }
}

extension SettingViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row==2
        {
            let cell:SettingViewCell1 = tableView.dequeueReusableCell(withIdentifier: "SettingCell1") as! SettingViewCell1
            
            cell.ivIcon.image = UIImage(named:arrImages[indexPath.row])
            cell.lblText.text = arrText[indexPath.row]
            cell.selectionStyle = .none
            
            return cell
        }
        else
        {
            let cell:SettingViewCell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingViewCell
            
            cell.ivIcon.image = UIImage(named:arrImages[indexPath.row])
            cell.lblText.text = arrText[indexPath.row]
            cell.selectionStyle = .none
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")

        if indexPath.row == 0{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "StaticPageVC") as! StaticPageVC
            vc.text = "About Us"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2{

        }else if indexPath.row == 3{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "StaticPageVC") as! StaticPageVC
            vc.text = "Terms & Conditions"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 4{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "StaticPageVC") as! StaticPageVC
            vc.text = "Privacy Policy"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 5{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
}
