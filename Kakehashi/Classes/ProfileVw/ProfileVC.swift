//
//  ProfileVC.swift
//  Kakehashi
//
//  Created by Keshav on 24/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import Alamofire

struct Country {
    
    let country_name : String
}

protocol SideMenuViewDelegate: class {
    
    func willUpdateUserData(data:ProfileVC)
}

class ProfileVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtBirthdate: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtZipcode: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    
    var gender = String()
    var countries = [[String: String]]()
    var marrCountry = NSMutableArray()

    weak var delegate: SideMenuViewDelegate?
    
    //MARK:- view lifecycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        setLayout()
        getProfile()
        jsonSerial()
        
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.openImagePicker(_:)))
        imgProfile.addGestureRecognizer(tapGesture)

//        let thumImgUrl = UserModel.sharedInstance().profile_image
//
//        imgProfile.sd_setImage(with: NSURL(string: (thumImgUrl!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!)) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)

        //imgProfile.isUserInteractionEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- textfield delegate method
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {

        self.view.endEditing(true)
        return false
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        self.view.endEditing(true)
    }

    //MARK:- UIAction method
    func setData(){
        
        if UserModel.sharedInstance().birthdate ==  "00-00-0000"
        {
            txtBirthdate.text = ""
        }
        else
        {
            txtBirthdate.text = UserModel.sharedInstance().birthdate
        }
        
        txtName.text = UserModel.sharedInstance().fullname
        txtEmail.text = UserModel.sharedInstance().email
        txtCountry.text = UserModel.sharedInstance().country
        txtCity.text = UserModel.sharedInstance().city
        txtZipcode.text = UserModel.sharedInstance().zipcode
        
//        if UserModel.sharedInstance().profile_image != ""
//        {
//            self.imgProfile.sd_setImage(with: URL(string: (UserModel.sharedInstance().profile_image!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!)), placeholderImage: UIImage(named:"placeholder"), options: .progressiveDownload)
//        }
        
        if UserModel.sharedInstance().gender == "male"
        {
            btnMale.setImage(UIImage(named: "radio-on"), for: .normal)
            btnFemale.setImage(UIImage(named: "radio-off"), for: .normal)
            btnOther.setImage(UIImage(named: "radio-off"), for: .normal)
        }
        else if UserModel.sharedInstance().gender == "female"
        {
            btnMale.setImage(UIImage(named: "radio-off"), for: .normal)
            btnFemale.setImage(UIImage(named: "radio-on"), for: .normal)
            btnOther.setImage(UIImage(named: "radio-off"), for: .normal)
        }
        else if UserModel.sharedInstance().gender == "other"
        {
            btnMale.setImage(UIImage(named: "radio-off"), for: .normal)
            btnFemale.setImage(UIImage(named: "radio-off"), for: .normal)
            btnOther.setImage(UIImage(named: "radio-on"), for: .normal)
        }
        else
        {
            btnMale.setImage(UIImage(named: "radio-off"), for: .normal)
            btnFemale.setImage(UIImage(named: "radio-off"), for: .normal)
            btnOther.setImage(UIImage(named: "radio-off"), for: .normal)
        }
    }
    
    func setLayout(){

        txtCity.delegate = self
        txtName.delegate = self
        txtEmail.delegate = self
        txtCountry.delegate = self
        txtZipcode.delegate = self
        txtBirthdate.delegate = self

        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.height/2
            self.imgProfile.clipsToBounds = true
            self.imgProfile.layer.borderWidth = 2.0
            self.imgProfile.layer.borderColor = UIColor(hex:"F0F0F2").cgColor
            self.imgProfile.contentMode = .scaleAspectFill
        }
        
        setInteraction(value: true)
        
        //set button corner
        btnCancel.layer.cornerRadius = 5
        btnCancel.clipsToBounds = true
        
        btnSave.layer.cornerRadius = 5
        btnSave.clipsToBounds = true
        
        //set corner radius and border
        setCornerRadiusAndBorder(txtName)
        setCornerRadiusAndBorder(txtEmail)
        setCornerRadiusAndBorder(txtBirthdate)
        setCornerRadiusAndBorder(txtCountry)
        setCornerRadiusAndBorder(txtCity)
        setCornerRadiusAndBorder(txtZipcode)
        
        //set placeholder
        setAttributePlaceholder(txtName, text: "Name")
        setAttributePlaceholder(txtEmail, text: "Email Address")
        setAttributePlaceholder(txtBirthdate, text: "Birthdate")
        setAttributePlaceholder(txtCountry, text: "Country")
        setAttributePlaceholder(txtCity, text: "City")
        setAttributePlaceholder(txtZipcode, text: "Zipcode")
        
        //Set padding
        setLeftPadding(txtName)
        setLeftPadding(txtEmail)
        setLeftPadding(txtBirthdate)
        setLeftPadding(txtCountry)
        setLeftPadding(txtCity)
        setLeftPadding(txtZipcode)
        
    }
    
    func setCornerRadiusAndBorder(_ textField:UITextField){
        textField.layer.cornerRadius = 5
        setBorder(textField, 1.0, UIColor(red: 231/255, green: 229/255, blue: 230/255, alpha: 1.0))
    }
    
    func setAttributePlaceholder(_ textField:UITextField, text: String){
        
        textField.attributedPlaceholder = NSAttributedString(string:text, attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 147/255, green: 147/255, blue: 147/255, alpha: 1.0)])
    }
    
    func setBorder(_ objName:AnyObject, _ borderWidth:CGFloat, _ borderColor:UIColor){
        objName.layer.borderColor = borderColor.cgColor
        objName.layer.borderWidth = borderWidth
    }
    
    func setLeftPadding(_ textField:UITextField){
        
        let ivLeft = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: textField.frame.size.height))
        textField.leftView = ivLeft
        textField.leftViewMode = .always
    }
    
    func setInteraction(value: Bool){

        imgProfile.isUserInteractionEnabled = value
        txtName.isUserInteractionEnabled = value
        txtEmail.isUserInteractionEnabled = value
        txtCountry.isUserInteractionEnabled = value
        txtBirthdate.isUserInteractionEnabled = value
        txtCity.isUserInteractionEnabled = value
        txtZipcode.isUserInteractionEnabled = value
        btnMale.isUserInteractionEnabled = value
        btnFemale.isUserInteractionEnabled = value
        btnOther.isUserInteractionEnabled = value
    }
    
    @objc func openImagePicker(_ recognizer: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        ImagePicker.shared.showImagePicker(viewController1: self, imageView: self.imgProfile)
    }
    
    //MARK:- Button methods
    @IBAction func btnSaveAction(_ sender: Any) {
        
        if txtName.text != ""
        {
            if txtEmail.text != ""
            {
                if (txtEmail.text?.isEmail)!
                {
                    editProfile()
                }
                else
                {
                    SharedInstance.alertViewController(message: "Please enter valid email address", inViewController: self)
                }
            }
            else
            {
                SharedInstance.alertViewController(message: "Please enter email address", inViewController: self)
            }
        }
        else
        {
            SharedInstance.alertViewController(message: "Please enter your name", inViewController: self)
        }
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.navigationController!.sideMenu.toggleLeftSideMenu()
    }
    
    @IBAction func btnEditAction(_ sender: Any) {
        
        setInteraction(value: true)
    }
    
    @IBAction func btnMenuAction(_ sender: Any) {

        self.navigationController?.sideMenu.toggleLeftSideMenu()
    }
    
    @IBAction func btnMaleAction(_ sender: Any) {
        
        btnMale.setImage(UIImage(named: "radio-on"), for: .normal)
        btnFemale.setImage(UIImage(named: "radio-off"), for: .normal)
        btnOther.setImage(UIImage(named: "radio-off"), for: .normal)

        gender = "male"
    }
    
    @IBAction func btnFemaleAction(_ sender: Any) {
        
        btnMale.setImage(UIImage(named: "radio-off"), for: .normal)
        btnFemale.setImage(UIImage(named: "radio-on"), for: .normal)
        btnOther.setImage(UIImage(named: "radio-off"), for: .normal)

        gender = "female"
    }
    
    @IBAction func btnOtherAction(_ sender: Any) {
        
        btnMale.setImage(UIImage(named: "radio-off"), for: .normal)
        btnFemale.setImage(UIImage(named: "radio-off"), for: .normal)
        btnOther.setImage(UIImage(named: "radio-on"), for: .normal)

        gender = "other"
    }
    
    //MARK:- TextField Delegate method
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCountry || textField == txtBirthdate
        {
            self.view.endEditing(true)
            if textField == txtBirthdate
            {
                SharedInstance.showDatePicker(format: dateFormat.type10, target: textField, title: "Select Birthdate", defaultDate: Date().yesterday, maximumDate: Date().yesterday, onCompletion: { (indexValue) in
                    
                    self.txtBirthdate.text = indexValue
                })
            }
            else if textField == txtCountry
            {
                SharedInstance.ShowStringPicker(id: textField, Title: "Select Country", rows: marrCountry, initialSelection: 0, onCompletion: {(indexNumber,indexValue)  in
                    DispatchQueue.main.async{
                        print("value of index and value",indexNumber,indexValue)
                        
                        self.txtCountry.text = indexValue
                    }
                    
                })
            }
            return false
        }
        
        return true
    }
    
    //MARK:- country code
    func jsonSerial() {
        let data = try? Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "countries", ofType: "json")!))
        do {
            let parsedObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
            countries = parsedObject as! [[String : String]]
            
            collectCountries()
            
        }catch{
            print("not able to parse")
        }
    }
    
    func collectCountries() {
        for country in countries  {
            
            let name = country["name"]!
            marrCountry.add(name)
        }
    }

    //MARK:- webservice method
    func getProfile()
    {
        if CheckReachability.isConnectedToNetwork() == true
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let URLString = "\(Constant.web_url)get_userprofile.php?user_type=\(UserModel.sharedInstance().user_type!)&user_id=\(UserModel.sharedInstance().user_id!)"
            Alamofire.request(URLString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    print("Error when calling service")
                    print(response.result.error!)
                    
                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    
                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }
                    else
                    {
                        let responseData = json["responseData"] as! [String:AnyObject]
                        print("Response Data : \(responseData)")
                        
                        UserModel.sharedInstance().fullname = responseData["full_name"] as? String
                        UserModel.sharedInstance().profile_image = responseData["profile_image"] as? String
                        UserModel.sharedInstance().email = responseData["email_address"] as? String
                        UserModel.sharedInstance().gender = responseData["gender"] as? String
                        UserModel.sharedInstance().country = responseData["country"] as? String
                        UserModel.sharedInstance().city = responseData["city"] as? String
                        UserModel.sharedInstance().zipcode = responseData["zipcode"] as? String
                        UserModel.sharedInstance().birthdate = responseData["birthdate"] as? String
                        self.gender = responseData["gender"] as! String
                        UserModel.sharedInstance().synchroniseData()
                       
                        self.txtName.text = UserModel.sharedInstance().fullname
                        self.txtEmail.text = UserModel.sharedInstance().email
                        self.txtCountry.text = UserModel.sharedInstance().country
                        self.txtCity.text = UserModel.sharedInstance().city
                        self.txtZipcode.text = UserModel.sharedInstance().zipcode
                        
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "dd/MM/yyyy"
//                        let strBirth = responseData["birthdate"] as! String
//                        let datebirth = dateFormatter.date(from: "\(strBirth)")
//                        //dateFormatter.dateFormat = "dd/MM/yyyy"
//                        UserModel.sharedInstance().birthdate = dateFormatter.string(from: datebirth!)
//                        print("\(UserModel.sharedInstance().birthdate ?? "")")
//                        UserModel.sharedInstance().synchroniseData()
                        
                        if UserModel.sharedInstance().gender == "male"
                        {
                            self.btnMale.setImage(UIImage(named: "radio-on"), for: .normal)
                            self.btnFemale.setImage(UIImage(named: "radio-off"), for: .normal)
                            self.btnOther.setImage(UIImage(named: "radio-off"), for: .normal)
                        }
                        else if UserModel.sharedInstance().gender == "female"
                        {
                            self.btnMale.setImage(UIImage(named: "radio-off"), for: .normal)
                            self.btnFemale.setImage(UIImage(named: "radio-on"), for: .normal)
                            self.btnOther.setImage(UIImage(named: "radio-off"), for: .normal)
                        }
                        else if UserModel.sharedInstance().gender == "other"
                        {
                            self.btnMale.setImage(UIImage(named: "radio-off"), for: .normal)
                            self.btnFemale.setImage(UIImage(named: "radio-off"), for: .normal)
                            self.btnOther.setImage(UIImage(named: "radio-on"), for: .normal)
                        }
                        else
                        {
                            self.btnMale.setImage(UIImage(named: "radio-off"), for: .normal)
                            self.btnFemale.setImage(UIImage(named: "radio-off"), for: .normal)
                            self.btnOther.setImage(UIImage(named: "radio-off"), for: .normal)
                        }
                        
                        if UserModel.sharedInstance().birthdate ==  "00/00/0000"
                        {
                            self.txtBirthdate.text = ""
                        }
                        else
                        {
                            self.txtBirthdate.text = UserModel.sharedInstance().birthdate
                        }
                        
                        let thumImgUrl = UserModel.sharedInstance().profile_image
                        
                        self.imgProfile.sd_setImage(with: NSURL(string: (thumImgUrl!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!)) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)
                    }
                    
                    MBProgressHUD.hide(for:self.view, animated: true)
                }
            }
        }
        else
        {
            MBProgressHUD.hide(for:self.view, animated: true)
            SharedInstance.alertViewController(message: "Please check your internet connection", inViewController: self)
        }

    }
    
    func editProfile()
    {
        if CheckReachability.isConnectedToNetwork() == true
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)

            var imgData = Data()

            if imgProfile.image == UIImage(named: "placeholder")
            {
                imgData = Data()
            }
            else{

                let profileImg = imgProfile.image?.resizeWithWidth(width: 500)
                imgData = UIImageJPEGRepresentation(profileImg!, 0.5)!
            }

            //let  parameters: [String:String]
            let parameters = ["user_type":"\(UserModel.sharedInstance().user_type!)","user_id":"\(UserModel.sharedInstance().user_id!)","full_name":"\(txtName.text!)","email_address":"\(txtEmail.text!)","birth_date":"\(txtBirthdate.text!)","gender":"\(gender)","country":"\(txtCountry.text!)","city":"\(txtCity.text!)","zipcode":"\(txtZipcode.text!)","about":""]

            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "profile_image",fileName: "image_post.png", mimeType: "image/png")
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            },
                             to:"\(Constant.web_url)edit_profile.php")
            { (result) in
                switch result {

                case .success(let upload, _, _):

                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })

                    upload.responseJSON { response in

                        guard let json = response.result.value as? [String: Any] else {
                            print("didn't get todo object as JSON from API")
                            print("Error: \(String(describing: response.result.error))")

                            return
                        }

                        if let strResult = json["status"] as? String
                        {
                            if strResult == "0"
                            {
                                let strMessage = json["message"] as! String
                                print(strMessage)
                            }
                            else
                            {
                                
                                //let responseData = json["responseData"] as! [String:AnyObject]
                                
//                                UserModel.sharedInstance().profile_image = "\(responseData["profile_image"]!)"
//                                UserModel.sharedInstance().fullname = "\(self.txtName.text!)"
//                                UserModel.sharedInstance().email = "\(self.txtEmail.text!)"
//                                UserModel.sharedInstance().country = "\(self.txtCountry.text!)"
//                                UserModel.sharedInstance().city = "\(self.txtCity.text!)"
//                                UserModel.sharedInstance().zipcode = "\(self.txtZipcode.text!)"
//                                UserModel.sharedInstance().birthdate = "\(self.txtBirthdate.text!)"
//                                UserModel.sharedInstance().gender = "\(self.gender)"
//                                UserModel.sharedInstance().synchroniseData()
                                SharedInstance.alertViewController(message: "Profile edited successfully", inViewController: self)
                                
                                self.delegate?.willUpdateUserData(data: self)
                                
                                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.setSlideRootNavigation()
                            }

                            MBProgressHUD.hide(for:self.view, animated: true)
                        }
                    }
                case .failure(_):

                    MBProgressHUD.hide(for:self.view, animated: true)
                }
            }
        }
        else
        {
            MBProgressHUD.hide(for:self.view, animated: true)
            SharedInstance.alertViewController(message: "Please check your internet connection", inViewController: self)
        }

    }
}

