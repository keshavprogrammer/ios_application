//
//  ProviderViewController.swift
//  Kakehashi
//
//  Created by keshav on 5/21/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire

class ProviderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var ImgThumb: UIImageView!
    @IBOutlet weak var lblArtistnm: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
}

class ProviderViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, IndicatorInfoProvider {

    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {

        return IndicatorInfo(title: "Provider")
    }

    @IBOutlet weak var ProviderCollectionView: UICollectionView!

    let columnLayout = HomeScreenColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )

    let reuseIdentifier = "ProviderCell"
    var arrProviderList = Array<Dictionary<String, AnyObject>>()
    var startcounter = Int()
    
    // MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()


        if #available(iOS 11.0, *) {
            ProviderCollectionView.collectionViewLayout = columnLayout
            ProviderCollectionView.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {

        self.navigationController?.isNavigationBarHidden = true

        startcounter = 0
        GetContentProvider(start: startcounter)
        
        let weakSelf:ProviderViewController = self
        ProviderCollectionView.addInfiniteScrolling(actionHandler: {() -> Void in
            weakSelf.insertRowAtBottomOfSearch()
        })
    }

    //MARK:- pagination methods
    func insertRowAtBottomOfSearch()
    {
            startcounter += 20

            GetContentProvider(start: startcounter)
            self.ProviderCollectionView.infiniteScrollingView.stopAnimating()
    }
    
    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if (self.arrProviderList.count == 0) {
            self.ProviderCollectionView.setEmptyMessage("")
        } else {
            self.ProviderCollectionView.restore()
        }
        return self.arrProviderList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = ProviderCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ProviderCollectionViewCell

        cell.layoutIfNeeded()
        cell.ImgThumb.layer.cornerRadius = cell.ImgThumb.frame.width/2
        cell.ImgThumb.layer.masksToBounds = false
        cell.ImgThumb.clipsToBounds = true
        cell.ImgThumb.layer.borderColor = UIColor.lightGray.cgColor
        cell.ImgThumb.layer.borderWidth = 1

        let thumImgUrl = arrProviderList[indexPath.row]["provider_image"] as! String

        cell.ImgThumb.sd_setImage(with: NSURL(string: thumImgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!) as URL?, placeholderImage: UIImage(named: "Dummy_User1"), options: .progressiveDownload)

        cell.lblCountry.text = arrProviderList[indexPath.row]["country_name"] as? String

        cell.lblArtistnm.text = arrProviderList[indexPath.row]["provider_name"] as? String

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        SharedInstance.alertViewController(message: "This content provider section is coming soon", inViewController: self)
    }

    //MARK:- Web service called
    func GetContentProvider(start: Int){

        if start == 0
        {
            self.arrProviderList.removeAll()
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        
        let url = "get_content_provider.php?start=\(start)"

        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET login.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        if start == 0
                        {
                            SharedInstance.alertViewController(message: strMessage, inViewController: self)
                            
                            self.arrProviderList.removeAll()
                        }
                    }
                    else
                    {

                        let responseData = json["responseData"] as! [[String:AnyObject]]

                        for i in 0..<responseData.count
                        {
                            self.arrProviderList.append(responseData[i])
                        }
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                    }

                    self.ProviderCollectionView.reloadData()
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }
}


class HomeScreenColumnFlowLayout: UICollectionViewFlowLayout {

    let cellsPerRow: Int

    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()

        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepare() {
        super.prepare()

        guard let collectionView = collectionView else { return }

        if #available(iOS 11.0, *) {
            let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)

            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)

            let _: CGFloat = collectionView.superview!.frame.size.height

            itemSize = CGSize(width: itemWidth, height: itemWidth*0.8+78)
        } else {
            // Fallback on earlier versions
        }
    }
    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }

}
