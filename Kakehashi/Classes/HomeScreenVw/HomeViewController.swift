//
//  HomeViewController.swift
//  Kakehashi
//
//  Created by keshav on 5/19/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var btnOption: UIButton!
    @IBOutlet weak var ImgThumb: UIImageView!
    @IBOutlet weak var btnPlayTap: UIButton!
    @IBOutlet weak var lblPlaylistTitle: UILabel!
    @IBOutlet weak var lblByusernm: UILabel!
    @IBOutlet weak var lblNoSong: UILabel!
}

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, IndicatorInfoProvider {

    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Playlists")
    }

    @IBOutlet var tblPlaylist: UITableView!
    @IBOutlet weak var btnChangeType: UIButton!
    @IBOutlet weak var HomeCollectionView: UICollectionView!

    let columnLayout = MainHomeScreenColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )

    let reuseIdentifier = "HomeCell"
    var arrPlayList = Array<Dictionary<String, AnyObject>>()
    var playlistNm = String()

    var addPop = UIView()
    var isShown = Bool()
    var startcounter = Int()
    var backView: UIView!
    var strEditPlaylistNM = String()
    var filter = String()
    
    //MARK:- view lifecycle method
    override func viewDidLoad() {
        super.viewDidLoad()

        filter = "new"
        tblPlaylist.isHidden = true
        btnChangeType.setImage(#imageLiteral(resourceName: "listView"), for: .normal)
        tblPlaylist.register(UINib(nibName: "ListDetailCell", bundle: nil), forCellReuseIdentifier: "listDetail")
        tblPlaylist.tableFooterView = UIView()
        
        if UserModel.sharedInstance().user_type! == "provider"
        {
            HomeCollectionView.register(UINib(nibName: "AlbumCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AlbumCollectionCell")
        }
        
        backView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        backView.backgroundColor = UIColor.clear
        self.view.addSubview(backView)
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hidePopUp(_:)))
        backView.addGestureRecognizer(tapGesture)
        backView.isUserInteractionEnabled = true
        backView.isHidden = true
        
        self.btnChangeType.layer.cornerRadius = self.btnChangeType.frame.size.height/2
        self.btnChangeType.clipsToBounds = true

        if #available(iOS 11.0, *) {
            HomeCollectionView?.collectionViewLayout = columnLayout
            HomeCollectionView?.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
        }
        
        startcounter = 0
        self.GetPlaylist(start: startcounter, filterType: "\(filter)")

        let weakSelf:HomeViewController = self
        HomeCollectionView.addInfiniteScrolling(actionHandler: {() -> Void in
            weakSelf.insertRowAtBottomOfSearch()
        })
        tblPlaylist.addInfiniteScrolling(actionHandler: {() -> Void in
            weakSelf.insertRowAtBottomOfSearch()
        })
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.filterList(notification:)), name: NSNotification.Name(rawValue: "filterNotification"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {

        self.navigationController?.isNavigationBarHidden = true

        startcounter = 0
        filter = "new"
        self.GetPlaylist(start: startcounter, filterType: "\(filter)")
        
        let weakSelf:HomeViewController = self
        HomeCollectionView.addInfiniteScrolling(actionHandler: {() -> Void in
            weakSelf.insertRowAtBottomOfSearch()
        })
        tblPlaylist.addInfiniteScrolling(actionHandler: {() -> Void in
            weakSelf.insertRowAtBottomOfSearch()
        })
    }

    
    //MARK:- pagination methods
    func insertRowAtBottomOfSearch()
    {
        startcounter += 20
        
        self.GetPlaylist(start: startcounter, filterType: "\(filter)")
        self.HomeCollectionView.infiniteScrollingView.stopAnimating()
        self.tblPlaylist.infiniteScrollingView.stopAnimating()
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPlayList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = HomeCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! HomeCollectionViewCell

        cell.lblPlaylistTitle.text = arrPlayList[indexPath.row]["title"] as? String
        cell.lblByusernm.text = "by \(UserModel.sharedInstance().fullname!)"
        
        let no_of_song = arrPlayList[indexPath.row]["no_of_songs"] as! String
        
        if no_of_song == "0"
        {
            cell.lblNoSong.text = "0 Video"
        }
        else if no_of_song == "1"
        {
            cell.lblNoSong.text = "\(no_of_song) Video"
        }
        else
        {
            cell.lblNoSong.text = "\(no_of_song) Videos"
        }
        
        cell.btnOption.addTarget(self, action: #selector(self.showSubAction), for: .touchUpInside)

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        print("You selected cell #\(indexPath.item)!")

        let Id = arrPlayList[indexPath.row]["playlist_id"] as! String
        playlistNm = (arrPlayList[indexPath.row]["title"] as? String)!

        self.performSegue(withIdentifier: "PlaylistToAlbum", sender: Id)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if (segue.identifier == "PlaylistToAlbum") {

            let controller =  segue.destination as! VideoViewController
            controller.AlbumId = sender as! String
            controller.screenType = "fromplaylist"
            controller.screen_title = playlistNm
        }
    }

    // MARK: - Uibutton Actions
    @objc private func filterList(notification: Notification) {
        //let sender = notification.object
        if let userInfo = notification.userInfo {
            // Safely unwrap the name sent out by the notification sender
            if let filterType = userInfo["type"] as? String {
                print(filterType)
                filter = filterType
                startcounter = 0
                self.GetPlaylist(start: startcounter, filterType: "\(filter)")
            }
        }
    }
    
    @IBAction func btnChangeViewTap(_ sender: Any) {
        
        if tblPlaylist.isHidden
        {
            tblPlaylist.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
            HomeCollectionView.isHidden = true
            tblPlaylist.isHidden = false
            btnChangeType.setImage(#imageLiteral(resourceName: "gridView"), for: .normal)
        }
        else
        {
            HomeCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
            HomeCollectionView.isHidden = false
            tblPlaylist.isHidden = true
            btnChangeType.setImage(#imageLiteral(resourceName: "listView"), for: .normal)
        }
    }
    
    @IBAction func showSubAction(_ sender: UIButton) {
        
        if isShown
        {
            backView.isHidden = true
            addPop.removeFromSuperview()
            isShown = false
        }
        else
        {
            backView.isHidden = false
            addPop.removeFromSuperview()
            let cell = sender.superview?.superview as! HomeCollectionViewCell
            let indexAdd = self.HomeCollectionView.indexPath(for: cell)
            let attributes = self.HomeCollectionView.layoutAttributesForItem(at: indexAdd!)
            let cellPoint = attributes?.frame.origin
            
            addPop = UIView(frame: CGRect(x: (cellPoint?.x)! + 5, y: (cellPoint?.y)! + ((attributes?.frame.size.height)!/6), width: (attributes?.frame.size.width)! - 5, height: 100))
            addPop.backgroundColor = UIColor(hex: "104353")
            addPop.layer.cornerRadius = 3.0
            //addPop.layer.borderWidth = 2.0
            //addPop.layer.borderColor = UIColor.red.cgColor
            self.view.addSubview(addPop)
            isShown = true
            self.view.bringSubview(toFront: addPop)
            
            let btnRename = UIButton(frame: CGRect(x: 5, y: 10, width: addPop.frame.size.width - 10, height: 35))
            btnRename.setTitle("Rename playlist", for: .normal)
            btnRename.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            btnRename.setTitleColor(UIColor.white, for: .normal)
            btnRename.backgroundColor = UIColor.clear
            btnRename.layer.cornerRadius = 5.0
            btnRename.addTarget(self, action: #selector(self.renameAction), for: .touchUpInside)
            addPop.addSubview(btnRename)
            btnRename.tag = (indexAdd?.row)!
            
            let btnDelete = UIButton(frame: CGRect(x: 5, y: btnRename.frame.size.height + btnRename.frame.origin.y + 10, width: addPop.frame.size.width - 10, height: 35))
            btnDelete.setTitle("Delete playlist", for: .normal)
            btnDelete.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            btnDelete.setTitleColor(UIColor.white, for: .normal)
            btnDelete.backgroundColor = UIColor.clear
            btnDelete.layer.cornerRadius = 5.0
            btnDelete.tag = (indexAdd?.row)!
            btnDelete.addTarget(self, action: #selector(self.deleteAction), for: .touchUpInside)
            addPop.addSubview(btnDelete)
        }
        
    }

    @IBAction func renameAction(_ sender: UIButton) {

        addPop.removeFromSuperview()
        isShown = false
        backView.isHidden = true

        let Id = arrPlayList[sender.tag]["playlist_id"] as! String

        let playlist = arrPlayList[sender.tag]["title"] as! String

        strEditPlaylistNM = ""
        
        strEditPlaylistNM = playlist
        
        let alert = UIAlertController(title: "Edit playlist name", message: "", preferredStyle: UIAlertControllerStyle.alert)

        alert.addTextField(configurationHandler: self.configurationTextField)
        
        alert.addAction(UIAlertAction(title: "Update", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in

            let firstTextField = alert.textFields![0] as UITextField

            self.edit_playlist(edit_title: firstTextField.text!, playlist_id: Id)

        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")

        })
    }

    func configurationTextField(textField: UITextField!){
    
        textField.text = strEditPlaylistNM
        
        textField.placeholder = "Enter playlist name"
    }

    @IBAction func deleteAction(_ sender: UIButton) {

        backView.isHidden = true
        let logoutAlert = UIAlertController(title: Constant.PROJECT_NAME as String, message: "Are you sure that you want to delete this playlist?", preferredStyle: UIAlertControllerStyle.alert)

        logoutAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in

            self.addPop.removeFromSuperview()
            self.isShown = false

            let Id = self.arrPlayList[sender.tag]["playlist_id"] as! String

            self.DeletePlaylist(delete_id: Id)

        }))

        logoutAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in

            self.addPop.removeFromSuperview()
            self.isShown = false

        }))

        present(logoutAlert, animated: true, completion: nil)

    }
    
    @objc func hidePopUp(_ recognizer: UITapGestureRecognizer) {
        
        if isShown
        {
            backView.isHidden = true
            addPop.removeFromSuperview()
            isShown = false
        }
    }

    //MARK:- Web service called
    func edit_playlist(edit_title:String,playlist_id:String){

        //edit_playlist.php?title=&user_id=8&playlist_id=22&title=mydglist
        MBProgressHUD.showAdded(to: self.view, animated: true)

        let url = "edit_playlist.php?title=\(edit_title)&user_id=\(UserModel.sharedInstance().user_id!)&playlist_id=\(playlist_id)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!

        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET edit_playlist.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        SharedInstance.alertViewController(message: strMessage, inViewController: self)

                    }
                    else
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        SharedInstance.alertViewController(message: strMessage, inViewController: self)

                        self.startcounter = 0
                        
                        self.GetPlaylist(start: self.startcounter, filterType: "\(self.filter)")
                    }

                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }

    func DeletePlaylist(delete_id:String){
        //http://keshavinfotechdemo.com/KESHAV/KG2/mladen/webservices/delete_playlist.php?playlist_id=1

        MBProgressHUD.showAdded(to: self.view, animated: true)

        let url = "delete_playlist.php?playlist_id=\(delete_id)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!

        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET Delete_playlist.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        SharedInstance.alertViewController(message: strMessage, inViewController: self)

                    }
                    else
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        SharedInstance.alertViewController(message: strMessage, inViewController: self)

                        self.GetPlaylist(start: self.startcounter, filterType: "\(self.filter)")
                    }

                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }

    func GetPlaylist(start: Int, filterType:String){

        if start == 0
        {
            self.arrPlayList.removeAll()
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        
        let url = "get_user_playlist_page.php?user_id=\(UserModel.sharedInstance().user_id!)&start=\(start)&type=\(filterType)"

        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET login.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        if start == 0
                        {
                            SharedInstance.alertViewController(message: strMessage, inViewController: self)
                            
                            self.arrPlayList.removeAll()
                        }
                    }
                    else
                    {

                        let responseData = json["responseData"] as! [[String:AnyObject]]

                        if start == 0
                        {
                            self.arrPlayList.removeAll()
                        }
                        
                        for i in 0..<responseData.count
                        {
                             self.arrPlayList.append(responseData[i])
                        }
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                    }

                    self.HomeCollectionView.reloadData()
                    self.tblPlaylist.reloadData()
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }
}

class MainHomeScreenColumnFlowLayout: UICollectionViewFlowLayout {

    let cellsPerRow: Int

    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()

        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepare() {
        super.prepare()

        guard let collectionView = collectionView else { return }

        if #available(iOS 11.0, *) {
            let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)

            let _: CGFloat = collectionView.superview!.frame.size.height

            itemSize = CGSize(width: itemWidth, height: itemWidth*0.9+100)
        } else {
            // Fallback on earlier versions
        }
    }

    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }

}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrPlayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblPlaylist.dequeueReusableCell(withIdentifier: "listDetail", for: indexPath) as! ListDetailCell
        
        cell.imgThumb.backgroundColor = UIColor.lightGray
        cell.imgThumb.image = #imageLiteral(resourceName: "Playlist_table")
        
        cell.lblTitle.text = arrPlayList[indexPath.row]["title"] as? String
        cell.lblSubTitle.text = "by \(UserModel.sharedInstance().fullname!)"
        let no_of_song = arrPlayList[indexPath.row]["no_of_songs"] as! String
        
        if no_of_song == "0"
        {
            cell.lblNoSong.text = "0 Video"
        }
        else if no_of_song == "1"
        {
            cell.lblNoSong.text = "\(no_of_song) Video"
        }
        else
        {
            cell.lblNoSong.text = "\(no_of_song) Videos"
        }
        
        cell.isCheckBtn.isHidden = true
        cell.btnPlayTap.isHidden = true
        
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.item)!")
        
        let Id = arrPlayList[indexPath.row]["playlist_id"] as! String
        playlistNm = (arrPlayList[indexPath.row]["title"] as? String)!
        
        self.performSegue(withIdentifier: "PlaylistToAlbum", sender: Id)
    }
}


