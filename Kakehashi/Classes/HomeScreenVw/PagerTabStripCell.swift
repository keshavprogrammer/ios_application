
import Foundation
import UIKit

class YoutubeIconWithLabelCell: UICollectionViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var iconLabel: UILabel!

}
