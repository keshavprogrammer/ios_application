//
//  AlbumVC.swift
//  Kakehashi
//
//  Created by Keshav Infotech on 21/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire

class AlbumTableCell : UITableViewCell{

    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var cvAlbum: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()

        cvAlbum.register(UINib(nibName: "AlbumCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AlbumCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {

    }
}
class AlbumCollectionCell :  UICollectionViewCell{

    @IBOutlet weak var btnOption: UIButton!
    @IBOutlet var ivAlbum: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblUnm: UILabel!
    @IBOutlet var lblSongCounter: UILabel!

    override func layoutSubviews()
    {
        super.layoutSubviews()
    }
}

extension AlbumTableCell {

    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

        let columnLayout = AlbumAllListScreenColumnFlowLayout(

            cellsPerRow: 3,
            minimumInteritemSpacing: 10,
            minimumLineSpacing: 10,
            sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        )

        cvAlbum.delegate = dataSourceDelegate
        cvAlbum.dataSource = dataSourceDelegate
        if #available(iOS 11.0, *) {
            cvAlbum.collectionViewLayout = columnLayout
            cvAlbum.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
        }
        cvAlbum.tag = row
        cvAlbum.setContentOffset(cvAlbum.contentOffset, animated:false) // Stops collection view if it was scrolling.
        cvAlbum.reloadData()
    }

    var collectionViewOffset: CGFloat {
        set {
            cvAlbum.contentOffset.x = newValue
        }

        get {
            return cvAlbum.contentOffset.x
        }
    }
}

class AlbumVC: UIViewController, IndicatorInfoProvider {

    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {

        return IndicatorInfo(title: "Albums")
    }

    //MARK:- Outlets
    @IBOutlet var tblAlbum: UITableView!

    var arrNewAlbumList = Array<Dictionary<String, AnyObject>>()
    var arrPopularAlbumList = Array<Dictionary<String, AnyObject>>()

    //MARK:- Global Variables
    var storedOffsets = [Int: CGFloat]()
    var selecetAlbum_name = String()
    var startcounter = Int()

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tblAlbum.register(UINib(nibName: "AlbumTableCell", bundle: nil), forCellReuseIdentifier: "AlbumTableCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {

        self.navigationController?.isNavigationBarHidden = true

        GetAlbumlist(type: "new", start: 0)
        GetAlbumlist(type: "popular", start: 0)
    }

    func GetAlbumlist(type:String,start:Int){

        if start == 0
        {
            
            if type=="new"
            {
                self.arrNewAlbumList.removeAll()
            }
            else
            {
                self.arrPopularAlbumList.removeAll()
            }
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)

        let url = "albums_list.php?type=\(type)&start=\(start)"

        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET login.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        if start == 0
                        {
                            
                            if type=="new"
                            {
                                self.arrNewAlbumList.removeAll()
                            }
                            else
                            {
                                self.arrPopularAlbumList.removeAll()
                            }
                            
                            SharedInstance.alertViewController(message: strMessage, inViewController: self)
                        }
                        
                    }
                    else
                    {

                        let responseData = json["responseData"] as! [[String:AnyObject]]

                        if type=="new"
                        {
                            for i in 0..<responseData.count
                            {
                                self.arrNewAlbumList.append(responseData[i])
                            }
                        }
                        else
                        {
                            for i in 0..<responseData.count
                            {
                                self.arrPopularAlbumList.append(responseData[i])
                            }
                            self.arrPopularAlbumList = responseData
                        }

                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                    }

                    if (self.arrPopularAlbumList.count > 0) && (self.arrPopularAlbumList.count > 0)
                    {
                        self.tblAlbum.reloadData()
                    }
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }
}

extension AlbumVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumTableCell", for: indexPath) as! AlbumTableCell

        cell.selectionStyle = .none

        if indexPath.row == 0
        {
            cell.lblTitle.text = "New Releases"
        }
        else
        {
            cell.lblTitle.text = "Most Popular"
        }

        cell.btnViewAll.tag = indexPath.row
        cell.btnViewAll.addTarget(self, action: #selector(Viewall(button:)), for: .touchUpInside)
        cell.cvAlbum.reloadData()

        return cell
    }

    @objc func Viewall(button: UIButton) {

        if button.tag == 0
        {
            self.performSegue(withIdentifier: "ViewallAlbum", sender: "new")
        }
        else
        {
            self.performSegue(withIdentifier: "ViewallAlbum", sender: "popular")
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        guard let tableViewCell = cell as? AlbumTableCell else { return }

        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
}

extension AlbumVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0
        {
            if (self.arrNewAlbumList.count == 0) {
                collectionView.setEmptyMessage("")
            } else {
                collectionView.restore()
            }
            
            if self.arrNewAlbumList.count > 5
            {
                return 5
            }
            else
            {
                return self.arrNewAlbumList.count
            }
            
            
        }
        else
        {
            if (self.arrPopularAlbumList.count == 0) {
                collectionView.setEmptyMessage("")
            } else {
                collectionView.restore()
            }
            
            if self.arrNewAlbumList.count > 5
            {
                return 5
            }
            else
            {
                return self.arrPopularAlbumList.count
            }
            
            
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCollectionCell", for: indexPath) as! AlbumCollectionCell

        if collectionView.tag == 0
        {

            let thumImgUrl = arrNewAlbumList[indexPath.row]["album_image"] as! String

            cell.ivAlbum.sd_setImage(with: NSURL(string: thumImgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)

            let no_of_song = arrNewAlbumList[indexPath.row]["no_of_songs"] as! Int
            
            if no_of_song == 0
            {
                 cell.lblSongCounter.text = "0 Video"
            }
            else if no_of_song == 1
            {
                 cell.lblSongCounter.text = "\(no_of_song) Video"
            }
            else
            {
                 cell.lblSongCounter.text = "\(no_of_song) Videos"
            }
            
            cell.lblTitle.text = arrNewAlbumList[indexPath.row]["album_name"] as? String

            cell.lblUnm.text = "by \(arrNewAlbumList[indexPath.row]["artist_name"]!)"

        }
        else
        {
            let thumImgUrl = arrPopularAlbumList[indexPath.row]["album_image"] as! String

            cell.ivAlbum.sd_setImage(with: NSURL(string: thumImgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)

            let no_of_song = arrPopularAlbumList[indexPath.row]["no_of_songs"] as! Int
            
            if no_of_song == 0
            {
                cell.lblSongCounter.text = "0 Video"
            }
            else if no_of_song == 1
            {
                cell.lblSongCounter.text = "\(no_of_song) Video"
            }
            else
            {
                cell.lblSongCounter.text = "\(no_of_song) Videos"
            }
            
            cell.lblTitle.text = arrPopularAlbumList[indexPath.row]["album_name"] as? String

            cell.lblUnm.text = "by \(arrPopularAlbumList[indexPath.row]["artist_name"]!)"
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        var Id = String()

        if collectionView.tag == 0
        {
            Id = arrNewAlbumList[indexPath.row]["album_id"] as! String
            selecetAlbum_name = arrNewAlbumList[indexPath.row]["album_name"] as! String
        }
        else
        {
            Id = arrPopularAlbumList[indexPath.row]["album_id"] as! String
            selecetAlbum_name = arrPopularAlbumList[indexPath.row]["album_name"] as! String
        }

        self.performSegue(withIdentifier: "AlbumtoPlaylist", sender: Id)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if (segue.identifier == "AlbumtoPlaylist") {
            let controller =  segue.destination as! VideoViewController
            controller.AlbumId = sender as! String
            controller.screenType = ""
            controller.screen_title = selecetAlbum_name
        }
        else if (segue.identifier == "ViewallAlbum") {

            let controller =  segue.destination as! ViewAllAlbumController
            controller.viewType = sender as! String
        }
    }

}


class AlbumAllListScreenColumnFlowLayout: UICollectionViewFlowLayout {

    let cellsPerRow: Int

    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()

        scrollDirection = .horizontal
        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepare() {
        super.prepare()

        guard let collectionView = collectionView else { return }

        if #available(iOS 11.0, *) {
            let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)

            let _: CGFloat = collectionView.superview!.frame.size.height

            itemSize = CGSize(width: itemWidth, height: itemWidth*0.6+120)
        } else {
            // Fallback on earlier versions
        }
    }

    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }

}
