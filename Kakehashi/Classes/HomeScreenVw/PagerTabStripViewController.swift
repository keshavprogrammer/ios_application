
import Foundation
import XLPagerTabStrip

class YoutubeWithLabelExampleViewController: BaseButtonBarPagerTabStripViewController<YoutubeIconWithLabelCell> {

    let redColor = UIColor(red: 221/255.0, green: 0/255.0, blue: 19/255.0, alpha: 1.0)
    let unselectedIconColor = UIColor(red: 73/255.0, green: 8/255.0, blue: 10/255.0, alpha: 1.0)

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        buttonBarItemSpec = ButtonBarItemSpec.nibFile(nibName: "YoutubeIconWithLabelCell", bundle: Bundle(for: YoutubeIconWithLabelCell.self), width: { _ in
            return 70.0
        })
    }

    @IBOutlet var btnFilter: UIButton!
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var PopupFilterVw: UIView!
    
    override func viewDidLoad() {
        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = UIColor.black
        settings.style.selectedBarHeight = 1
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.black
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        changeCurrentIndexProgressive = { [weak self] (oldCell: YoutubeIconWithLabelCell?, newCell: YoutubeIconWithLabelCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            //oldCell?.iconImage.tintColor = self?.unselectedIconColor
            oldCell?.iconLabel.textColor = UIColor.lightGray
            //newCell?.iconImage.tintColor = UIColor.white
            newCell?.iconLabel.textColor = UIColor.black
        }
        super.viewDidLoad()
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true

    }

    @IBAction func btnFilterAction(_ sender: Any) {
        if PopupFilterVw.isHidden
        {
            self.imgBlur.isHidden = false
            PopupFilterVw.isHidden = false
        }
        else{
            
            self.imgBlur.isHidden = true
            PopupFilterVw.isHidden = true
        }
    }
    
    @IBAction func BtnOldToNewTap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name("filterNotification"), object: self, userInfo: ["type": "old"])
    }
    
    @IBAction func btnNewToOldTap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name("filterNotification"), object: self, userInfo: ["type": "new"])
    }
    
    @IBAction func btnAtoZTap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name("filterNotification"), object: self, userInfo: ["type": "atoz"])
    }
    
    @IBAction func btnZtoATap(_ sender: Any) {
        
        PopupFilterVw.isHidden = true
        self.imgBlur.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name("filterNotification"), object: self, userInfo: ["type": "ztoa"])
    }
    
    @IBAction func btnMenu(_ sender: Any) {

        self.navigationController?.sideMenu.toggleLeftSideMenu()
    }

    @IBAction func btnFind(_ sender: Any) {
        
    }

    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {

        let viewController3:ProviderViewController = self.storyboard!.instantiateViewController(withIdentifier: "ProviderViewController") as! ProviderViewController

        let viewController2:AlbumVC = self.storyboard!.instantiateViewController(withIdentifier: "AlbumVC1") as! AlbumVC

        let viewController1:HomeViewController = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController

        return [viewController1,viewController2,viewController3]
    }

    override func configure(cell: YoutubeIconWithLabelCell, for indicatorInfo: IndicatorInfo) {
        //cell.iconImage.image = indicatorInfo.image?.withRenderingMode(.alwaysTemplate)
        cell.iconLabel.text = indicatorInfo.title?.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        if indexWasChanged && toIndex > -1 && toIndex < viewControllers.count {
            
            if toIndex == 1 || toIndex == 2
            {
                self.btnFilter.isHidden = true
            }
            else
            {
                self.btnFilter.isHidden = false
            }
            let child = viewControllers[toIndex] as! IndicatorInfoProvider // swiftlint:disable:this force_cast
            UIView.performWithoutAnimation({ [weak self] () -> Void in
                guard let me = self else { return }
                me.navigationItem.leftBarButtonItem?.title =  child.indicatorInfo(for: me).title
            })
        }
    }

    // MARK: - Actions
    @IBAction func closeAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

