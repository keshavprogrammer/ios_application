//
//  SocialSignViewController.swift
//  Kakehashi
//
//  Created by keshav on 5/31/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import Alamofire

class SocialSignViewController: UIViewController, DropDownViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAccountType: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!

    var vwDropdown: DropDownView!
    let userTypes = ["user","content provider"]
    var dictInsta = [String: AnyObject]()
    var dictTwitter = [String : String]()
    var socialMediaType = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setlayout()
        //Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - selayout method
    func setlayout() {

        txtAccountType.setPaddingImage(#imageLiteral(resourceName: "Black_dropdown"), SetPaddingType: .rightSide)

        txtAccountType.layer.cornerRadius = 5
        txtAccountType.clipsToBounds = true
        setBorder(txtAccountType, 1.0, UIColor.white)

        txtEmail.layer.cornerRadius = 5
        txtEmail.clipsToBounds = true
        setBorder(txtEmail, 1.0, UIColor.white)

        btnCancel.layer.cornerRadius = 5
        btnCancel.clipsToBounds = true

        btnSubmit.layer.cornerRadius = 5
        btnSubmit.clipsToBounds = true

        let paddingView2 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: self.txtAccountType.frame.size.height))
        txtAccountType.leftView = paddingView2
        txtAccountType.leftViewMode = .always

        let paddingView1 = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: self.txtEmail.frame.size.height))
        txtEmail.leftView = paddingView1
        txtEmail.leftViewMode = .always

        if socialMediaType == "fb"
        {
            txtEmail.text = dictTwitter["email"]!
        }
    }

    func setBorder(_ objName:AnyObject, _ borderWidth:CGFloat, _ borderColor:UIColor){

        objName.layer.borderColor = borderColor.cgColor
        objName.layer.borderWidth = borderWidth
    }

    //MARK:- textfield delegate method
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        if textField == txtAccountType
        {
            if self.vwDropdown == nil
            {

                self.vwDropdown = DropDownView.init(arrayData: userTypes, cellHeight: 30, heightTableView: 60, paddingTop: 3, paddingLeft: 3, paddingRight: 3, refView: textField as UIView, animation: BLENDIN, openAnimationDuration: 0.1, closeAnimationDuration: 1)

                self.vwDropdown.delegate = self
                self.view.addSubview(self.vwDropdown.view)

            }
            else
            {
                self.vwDropdown.view.removeFromSuperview()
                self.vwDropdown = nil

                self.vwDropdown = DropDownView.init(arrayData: userTypes, cellHeight: 30, heightTableView: 60, paddingTop: 3, paddingLeft: 3, paddingRight: 3, refView: textField as UIView, animation: BLENDIN, openAnimationDuration: 0.1, closeAnimationDuration: 1)

                self.vwDropdown.delegate = self
                self.view.addSubview(self.vwDropdown.view)
                self.vwDropdown.openAnimation()
            }
            return false
        }

        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        self.view.endEditing(true)

        if vwDropdown != nil{
            self.vwDropdown.closeAnimation()
        }
    }

    // MARK: - DropDown Delegate
    func dropDownCellSelected(_ returnIndex: Int, in viewDropDown: UIView!) {

        if returnIndex==0 {

            txtAccountType.text = "user"
        }
        else
        {
            txtAccountType.text = "content provider"
        }

        viewDropDown.removeFromSuperview()
    }

    // MARK: - Action method
    @IBAction func btnBackButton(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnCancelButton(_ sender: Any) {

        if self.vwDropdown == nil
        {

        }
        else
        {
            vwDropdown.closeAnimation()
        }

    self.navigationController?.popViewController(animated: true)
    }

    @IBAction func BtnSubmitTap(_ sender: Any) {

        if txtEmail.text != ""
        {
            if (txtEmail.text?.isEmail)!
            {
                if txtAccountType.text != ""
                {
                    if socialMediaType == "insta" {
                        self.InstaLogin()
                    }
                    else if socialMediaType == "twitter"
                    {
                        self.TwitterLogin(resultDict: self.dictTwitter)
                    }
                    else if socialMediaType == "fb"
                    {
                        self.loginWithFBService()
                    }
                }
                else
                {
                    SharedInstance.alertViewController(message: "Please select account type", inViewController: self)
                }
            }
            else
            {
                SharedInstance.alertViewController(message: "Please enter valid email address", inViewController: self)
            }
        }
        else
        {
            SharedInstance.alertViewController(message: "Please enter email address", inViewController: self)
        }
    }

    // MARK: - webservice called
    func loginWithFBService()
    {
        // http://keshavinfotechdemo.com/KESHAV/KG2/mladen/webservices/login_with_facebook.php?email_address=&fb_token=&fb_id=&full_name=&profile_image=&user_type=

        MBProgressHUD.showAdded(to: self.view, animated: true)

        var userType = String()
        if txtAccountType.text == "content provider"
        {
            userType = "provider"
        }
        else
        {
            userType = "user"
        }

        let todoEndpoint: String = "\(Constant.web_url)login_with_facebook.php?fb_id=\(dictTwitter["fb_id"]!)&fb_token=\(dictTwitter["accesstoken"]!)&full_name=\(dictTwitter["fullname"]!)&email_address=\(txtEmail.text!)&profile_image=\(dictTwitter["profilepicture"]!)&user_type=\(userType)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!

        Alamofire.request(todoEndpoint, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error in 1 guard")
                    print(response.result.error!)
                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("error in 2 guard")
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String{
                    if strResult == "0"{
                        print("Status:\(strResult)")
                        let strMessage = json["message"] as! String

                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }
                    else{
                        if let dictUser = json["responseData"] as? [String:String]{

                            let responseData = json["responseData"] as! [String:AnyObject]
                            print("Response Data : \(responseData)")

                            UserModel.sharedInstance().user_id = "\(responseData["user_id"]!)"
                            UserModel.sharedInstance().fullname = responseData["full_name"] as? String
                            UserModel.sharedInstance().profile_image = responseData["profile_image"] as? String
                            UserModel.sharedInstance().email = responseData["email_address"] as? String
                            UserModel.sharedInstance().user_type = responseData["user_type"] as? String

                            UserModel.sharedInstance().synchroniseData()

                            SharedInstance.appDelegate().setSlideRootNavigation()
                        }
                    }
                }
                MBProgressHUD.hide(for:self.view, animated: true)
        }
    }

    func TwitterLogin(resultDict: [String: String])
    {
        if CheckReachability.isConnectedToNetwork() == true
        {

            var userType = String()
            if txtAccountType.text == "content provider"
            {
                userType = "provider"
            }
            else
            {
                userType = "user"
            }

            MBProgressHUD.showAdded(to: self.view, animated: true)

            let URLString = "\(Constant.web_url)login_with_twitter.php?email_address=\(txtEmail.text!)&twitter_token=\(resultDict["twitter_token"]!)&twitter_id=\(resultDict["twitterid"]!)&full_name=\(resultDict["first_name"]!)&profile_image=\(resultDict["profilepicture"]!)&user_type=\(userType)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!

            Alamofire.request(URLString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in

                // check for errors
                guard response.result.error == nil else {
                    print("Error when calling service")
                    print(response.result.error!)

                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }

                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")

                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }

                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }
                    else
                    {
                        let responseData = json["responseData"] as! [String:AnyObject]
                        print("Response Data : \(responseData)")

                        UserModel.sharedInstance().user_id = responseData["user_id"] as? String
                        UserModel.sharedInstance().fullname = responseData["full_name"] as? String
                        UserModel.sharedInstance().profile_image = responseData["profile_image"] as? String
                        UserModel.sharedInstance().email = responseData["email_address"] as? String
                        UserModel.sharedInstance().user_type = responseData["user_type"] as? String

                        UserModel.sharedInstance().synchroniseData()

                        SharedInstance.appDelegate().setSlideRootNavigation()
                    }

                    MBProgressHUD.hide(for:self.view, animated: true)
                }
            }
        }
        else
        {
            MBProgressHUD.hide(for:self.view, animated: true)
            SharedInstance.alertViewController(message: "Please check your internet connection", inViewController: self)
        }
    }
    
    func InstaLogin()
    {
        if CheckReachability.isConnectedToNetwork() == true
        {
            
            var userType = String()
            if txtAccountType.text == "content provider"
            {
                userType = "provider"
            }
            else
            {
                userType = "user"
            }
            
            //let strName = "\(resultDict["first_name"]!) \(resultDict["last_name"]!)"
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            //login_with_twitter.php?email_address=&fb_token=&fb_id=&full_name=&profile_image=&user_type=
            let URLString = "http://keshavinfotechdemo.com/KESHAV/KG2/mladen/webservices/login_with_instagram.php?email_address=\(txtEmail.text!)&insta_token=\(String(describing: dictInsta["insta_token"]!))&insta_id=\(String(describing: dictInsta["id"]!))&full_name=\(String(describing: dictInsta["full_name"]!))&profile_image=\(String(describing: dictInsta["profile_picture"]!))&user_type=\(userType)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!
            
            Alamofire.request(URLString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    print("Error when calling service")
                    print(response.result.error!)
                    
                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    
                    MBProgressHUD.hide(for:self.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        SharedInstance.alertViewController(message: strMessage, inViewController: self)
                    }
                    else
                    {
                        let responseData = json["responseData"] as! [String:AnyObject]
                        print("Response Data : \(responseData)")
                        
                        UserModel.sharedInstance().user_id = responseData["user_id"] as? String
                        UserModel.sharedInstance().fullname = responseData["full_name"] as? String
                        UserModel.sharedInstance().profile_image = responseData["profile_image"] as? String
                        UserModel.sharedInstance().email = responseData["email_address"] as? String
                        UserModel.sharedInstance().user_type = responseData["user_type"] as? String
                        
                        UserModel.sharedInstance().synchroniseData()

                        SharedInstance.appDelegate().setSlideRootNavigation()
                    }
                    
                    MBProgressHUD.hide(for:self.view, animated: true)
                }
            }
        }
        else
        {
            MBProgressHUD.hide(for:self.view, animated: true)
            SharedInstance.alertViewController(message: "Please check your internet connection", inViewController: self)
        }
    }
}
