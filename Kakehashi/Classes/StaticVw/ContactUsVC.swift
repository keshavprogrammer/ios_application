//
//  ContactUsVC.swift
//  Kakehashi
//
//  Created by Keshav Infotech on 21/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import Alamofire

class ContactUsVC: UIViewController, UITextFieldDelegate {

    //MARK:- Outlets
    @IBOutlet var tfName: UITextField!
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfContactNo: UITextField!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var tvMessage: UITextView!
    @IBOutlet weak var btnCancel: UIButton!
    
    //MARK:- View Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- textfield delegate method
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        self.view.endEditing(true)

    }

    //MARK:- Other Methods
    func setLayout(){
        setTextFieldProperty(tfName)
        setTextFieldProperty(tfEmail)
        setTextFieldProperty(tfContactNo)

        tfEmail.delegate = self
        tfName.delegate = self
        tfContactNo.delegate = self

        tvMessage.layer.cornerRadius = 8
        tvMessage.clipsToBounds = true
        setBorder(tvMessage, 1.0, UIColor(red: 231/255, green: 229/255, blue: 230/255, alpha: 1.0))
        
        btnSubmit.layer.cornerRadius = 5
        btnSubmit.clipsToBounds = true
        
        btnCancel.layer.cornerRadius = 5
        btnCancel.clipsToBounds = true
    }
    
    func setTextFieldProperty(_ textField:UITextField){
        setCornerRadius(textField, 8)
        setBorder(textField, 1.0, UIColor(red: 231/255, green: 229/255, blue: 230/255, alpha: 1.0))
        setLeftPadding(textField)
    }
    
    func setBorder(_ objName:AnyObject, _ borderWidth:CGFloat, _ borderColor:UIColor){
        objName.layer.borderColor = borderColor.cgColor
        objName.layer.borderWidth = borderWidth
    }
    
    func setCornerRadius(_ textField:UITextField, _ cornerRadiusValue:CGFloat){
        textField.layer.cornerRadius = cornerRadiusValue
        textField.clipsToBounds = true
    }
    
    func setLeftPadding(_ textField:UITextField){
        let ivLeft = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textField.frame.size.height))
        textField.leftView = ivLeft
        textField.leftViewMode = .always
    }

    //MARK:- webservice method
    func ContactUs_Service(){
        let strURL : String = "\(Constant.web_url)contact-us.php?name=\(tfName.text!)&email_address=\(tfEmail.text!)&phone_number=\(tfContactNo.text!)&message=\(tvMessage.text!)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!
        
        Alamofire.request(strURL, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                //check for errors
                guard response.result.error == nil else {
                    print("Error when calling service")
                    print(response.result.error!)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    return
                }
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: AnyObject] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    return
                }
                
                let status = json["status"] as! String
                if status == "1"{
                    
                    let alert = UIAlertController(title: "Kakehashi", message: "Message sent successfully", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction (title: "Ok", style: UIAlertActionStyle.cancel, handler: { action -> Void in
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
//                    SharedInstance.alertViewController(message: "\(json["message"] as! String)", inViewController: self)
                    
                }else{
                    SharedInstance.alertViewController(message: "\(json["message"] as! String)", inViewController: self)
                    }

                }
    }
    
    
    //MARK:- Button Action
    @IBAction func btnCancelTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit_Action(_ sender: Any) {
        if tfName.text?.count == 0{
            
            SharedInstance.alertViewController(message: "Please enter your name", inViewController: self)
        }
        else if tfEmail.text?.count == 0 {
            SharedInstance.alertViewController(message: "Please enter your email address", inViewController: self)
        }
        else if !(tfEmail.text?.isEmail)!{
             SharedInstance.alertViewController(message: "Please enter valid email address", inViewController: self)
        }
        else if tfContactNo.text?.count == 0 {
            SharedInstance.alertViewController(message: "Please enter your contact number", inViewController: self)
        }
        else if tvMessage.text?.count == 0 {
            SharedInstance.alertViewController(message: "Please enter the message", inViewController: self)
        }
        else if !(tfEmail.text?.isEmail)! {
            SharedInstance.alertViewController(message: "Please enter valid email address", inViewController: self)
        }else{
            
            self.view.endEditing(true)
            ContactUs_Service()
        }
    }
    
    @IBAction func btnMenu_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
