//
//  StaticPageVC.swift
//  Kakehashi
//
//  Created by Keshav MacBook Pro on 24/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import Alamofire

class StaticPageVC: UIViewController {
    
    @IBOutlet weak var ivPic: UIImageView!
    @IBOutlet var tvContent: UITextView!
    @IBOutlet var lblNavigationTitle: UILabel!
    var text = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //type (about_us, terms, privacy)
        lblNavigationTitle.text = text
        if text == "About Us"{
            Statis_Service("about_us")
            ivPic.image = UIImage(named: "about-us")
        }else if text == "Terms & Conditions"{
            Statis_Service("terms")
            ivPic.image = UIImage(named: "t&c")
        }else{
            Statis_Service("privacy")
            ivPic.image = UIImage(named: "privacypolict")
        }
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    @IBAction func btnMenu_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }



    func Statis_Service(_ type : String){
        let strURL : String = "\(Constant.web_url)static_pages.php?type=\(type)"

         MBProgressHUD.showAdded(to: self.view, animated: true)

        Alamofire.request(strURL, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                //check for errors
                guard response.result.error == nil else {
                    print("Error when calling service")
                    print(response.result.error!)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    return
                }
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: AnyObject] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    return
                }
                
                let status = json["status"] as! String
                if status == "1"{
                    
                    self.tvContent.text = ((json["responseData"] as! [String:AnyObject])["content"] as! String).html2String
                    
                }else{
                    
                    SharedInstance.alertViewController(message: "\(json["message"] as! String)", inViewController: self)
                }
                
                MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    
    
    
}
