//
//  SearchVC.swift
//  Kakehashi
//
//  Created by Keshav on 22/05/18.
//  Copyright © 2018 Mladen. All rights reserved.
//

import UIKit
import Alamofire

class SearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet var btnVideoCp: UIButton!
    @IBOutlet var btnAlbumcp: UIButton!
    @IBOutlet var btnAllCp: UIButton!
    @IBOutlet var vwHeaderCp: UIView!
    @IBOutlet var tblTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var ImgSearchback: UIImageView!
    @IBOutlet weak var tblSearch: UITableView!
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var lblLeading: NSLayoutConstraint!
    @IBOutlet weak var lblIndicator: UILabel!
    @IBOutlet weak var btnProvider: UIButton!
    @IBOutlet weak var btnAlbums: UIButton!
    @IBOutlet weak var btnPlaylist: UIButton!
    @IBOutlet weak var btnAll: UIButton!
     @IBOutlet weak var btnVideos: UIButton!
    @IBOutlet weak var customSearchBar: UISearchBar!

    var isRecentSearch = Bool()
    var marrSearchResult = [String]()
    
    var marrRecentSearch = Array<Dictionary<String, AnyObject>>()
    var arrAllSearchList = Array<Dictionary<String, AnyObject>>()
    var searchType : String = "all"
    var selecet_name = String()
    var startSearch = Int()

    //MARK:- view lifecycle method
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnAllCp.setTitleColor(UIColor.black, for: .normal)
        self.btnVideoCp.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
        self.btnAlbumcp.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
        
        if UserModel.sharedInstance().user_type! == "provider"
        {
            vwHeader.isHidden = true
            vwHeaderCp.isHidden = false
            
            self.btnAllCp.setTitleColor(UIColor.black, for: .normal)
            self.btnVideoCp.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnAlbumcp.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
        }
        else
        {
            vwHeader.isHidden = false
            vwHeaderCp.isHidden = true
            
            self.btnAll.setTitleColor(UIColor.black, for: .normal)
            self.btnVideos.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnAlbums.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnPlaylist.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnProvider.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
        }
        
        customSearchBar.setImage(UIImage(named: "Search"), for: .search, state: .normal)
        customSearchBar.setStyleColor(UIColor.white)
        setLayout()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let weakSelf:SearchVC = self
        tblSearch.addInfiniteScrolling(actionHandler: {() -> Void in
            weakSelf.insertRowAtBottomOfSearch()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- other methods
    func setLayout()
    {
        vwHeader.isHidden = true
        vwHeaderCp.isHidden = true
        tblSearch.isHidden = true
    }

    //MARK:- keyboard methods
    @objc func keyboardDidShow(_ notification: Notification) {
        print("keyboard was shown")
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardRectangle.size.height, 0.0)
            tblSearch.contentInset = contentInsets
            tblSearch.scrollIndicatorInsets = contentInsets
        }
    }
    
    @objc func keyboardWillBeHidden(_ notification: Notification) {
        let contentInsets: UIEdgeInsets = .zero
        tblSearch.contentInset = contentInsets
        tblSearch.scrollIndicatorInsets = contentInsets
    }
    
    func deregisterFromKeyboardNotifications(){
        
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK:- pagination methods
    func insertRowAtBottomOfSearch()
    {
        if isRecentSearch
        {
            startSearch += 20
            getRecentSearch(start: startSearch)
            self.tblSearch.infiniteScrollingView.stopAnimating()
        }
    }
    
    //MARK:- searchBar delegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text == ""
        {
            print("open recent search data")
            isRecentSearch = true
            DispatchQueue.main.async {
                self.vwHeader.isHidden = true
                self.vwHeaderCp.isHidden = true
                self.startSearch = 0
                self.tblTopConstraint.constant = -45
                self.getRecentSearch(start: self.startSearch)
                self.tblSearch.separatorColor = UIColor.darkGray
                self.ImgSearchback.isHidden = true
            }
            self.view.setNeedsLayout()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //searchBar.text = ""
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText \(searchText)")
        isRecentSearch = false
        DispatchQueue.main.async {
            self.tblTopConstraint.constant = 0
            self.ImgSearchback.isHidden = true
        }
        self.view.setNeedsLayout()
        
        if customSearchBar.text != ""
        {
            if UserModel.sharedInstance().user_type! == "provider"
            {
                self.Search_cp(filterString: customSearchBar.text!, type: searchType)
            }
            else{
                self.Search(filterString: customSearchBar.text!, type: searchType)
            }
        }
        else
        {
            
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchText \(String(describing: searchBar.text))")

        self.view.endEditing(true)
        if !isRecentSearch
        {
            if UserModel.sharedInstance().user_type! == "provider"
            {
                self.Search_cp(filterString: customSearchBar.text!, type: searchType)
            }
            else{
                self.Search(filterString: customSearchBar.text!, type: searchType)
            }
            
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

        searchBar.text = ""
        searchBar.showsCancelButton = false
    }

    //MARK:- table view delegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if isRecentSearch
        {
            return self.marrRecentSearch.count
        }
        else
        {
            if (self.arrAllSearchList.count == 0) {
                
                tableView.isHidden = true
                //self.vwHeader.isHidden = true
                self.ImgSearchback.isHidden = false
            } else {
                
                tableView.isHidden = false
                if UserModel.sharedInstance().user_type! == "provider"
                {
                    vwHeader.isHidden = true
                    vwHeaderCp.isHidden = false
                }
                else{
                    vwHeader.isHidden = false
                    vwHeaderCp.isHidden = true
                }
                //self.vwHeader.isHidden = false
                self.ImgSearchback.isHidden = true
            }
            return self.arrAllSearchList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "search", for: indexPath as IndexPath) as! SearchTableViewCell
        
                DispatchQueue.main.async {
                    cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.size.height/2

                }
        
        if isRecentSearch && marrRecentSearch.count > 0
        {
            let thumImgUrl = marrRecentSearch[indexPath.row]["image"] as! String
            
            cell.imgProfile.sd_setImage(with: NSURL(string: thumImgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)
            
            cell.btnPlay.isHidden = true
            let type = marrRecentSearch[indexPath.row]["type"] as? String
            
            if  type == "playlist"
            {
                cell.lblTitle.text = (marrRecentSearch[indexPath.row]["name"] as! String)
                cell.lblSubtitle.text = "by \(marrRecentSearch[indexPath.row]["artist_name"] as! String)"
                
                let num_of_song = marrRecentSearch[indexPath.row]["no_of_songs"] as! String
                
                if num_of_song.isEmpty
                {
                    cell.lblSongs.text = "0 Video"
                }
                else if num_of_song == "1"
                {
                    cell.lblSongs.text = "\(marrRecentSearch[indexPath.row]["no_of_songs"] as! String) Video"
                }
                else
                {
                    cell.lblSongs.text = "\(marrRecentSearch[indexPath.row]["no_of_songs"] as! String) Videos"
                }
                
                cell.lblMinute.isHidden = true
                cell.lblSongs.isHidden = false
                
            }else if type == "album"
            {
                cell.lblTitle.text = (marrRecentSearch[indexPath.row]["name"] as! String)
                cell.lblSubtitle.text = "by \(marrRecentSearch[indexPath.row]["artist_name"] as! String)"
                
                let num_of_song = marrRecentSearch[indexPath.row]["no_of_songs"] as! String
                
                if num_of_song.isEmpty
                {
                    cell.lblSongs.text = "0 Video"
                }
                else if num_of_song == "1"
                {
                    cell.lblSongs.text = "\(marrRecentSearch[indexPath.row]["no_of_songs"] as! String) Video"
                }
                else
                {
                    cell.lblSongs.text = "\(marrRecentSearch[indexPath.row]["no_of_songs"] as! String) Videos"
                }
                
                cell.lblMinute.isHidden = true
                cell.lblSongs.isHidden = false
                
            }
            else
            {
                if type == "video" {
                    cell.btnPlay.isHidden = false
                }
                
                cell.lblTitle.text = (marrRecentSearch[indexPath.row]["name"] as! String)
                cell.lblSubtitle.text = "by \(marrRecentSearch[indexPath.row]["artist_name"] as! String)"
                
                let num_of_song = marrRecentSearch[indexPath.row]["no_of_songs"] as! String
                
                if num_of_song.isEmpty
                {
                    cell.lblSongs.text = "0 Video"
                }
                else if num_of_song == "1"
                {
                    cell.lblSongs.text = "\(marrRecentSearch[indexPath.row]["no_of_songs"] as! String) Video"
                }
                else
                {
                    cell.lblSongs.text = "\(marrRecentSearch[indexPath.row]["no_of_songs"] as! String) Videos"
                }
                
                cell.lblSongs.isHidden = true
                cell.lblMinute.isHidden = true
            }
        }
        else
        {
            let thumImgUrl = arrAllSearchList[indexPath.row]["image"] as! String
            
            cell.imgProfile.sd_setImage(with: NSURL(string: thumImgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image"), options: .progressiveDownload)
            
            let type = arrAllSearchList[indexPath.row]["type"] as? String
            cell.btnPlay.isHidden = true
            
            if  type == "playlist"
            {
                cell.lblTitle.text = (arrAllSearchList[indexPath.row]["name"] as! String)
                cell.lblSubtitle.text = "by \(UserModel.sharedInstance().fullname!)"
                
                let num_of_song = arrAllSearchList[indexPath.row]["no_of_songs"] as! String
                
                if num_of_song.isEmpty
                {
                    cell.lblSongs.text = "0 Video"
                }
                else if num_of_song == "1"
                {
                    cell.lblSongs.text = "\(arrAllSearchList[indexPath.row]["no_of_songs"] as! String) Video"
                }
                else
                {
                    cell.lblSongs.text = "\(arrAllSearchList[indexPath.row]["no_of_songs"] as! String) Videos"
                }
                
                cell.lblMinute.isHidden = true
                cell.lblSongs.isHidden = false
                
            }else if type == "album"
            {
                cell.lblTitle.text = (arrAllSearchList[indexPath.row]["name"] as! String)
                cell.lblSubtitle.text = "by \(arrAllSearchList[indexPath.row]["artist_name"] as! String)"
                
                let num_of_song = arrAllSearchList[indexPath.row]["no_of_songs"] as! String
                
                if num_of_song.isEmpty
                {
                    cell.lblSongs.text = "0 Video"
                }
                else if num_of_song == "1"
                {
                    cell.lblSongs.text = "\(arrAllSearchList[indexPath.row]["no_of_songs"] as! String) Video"
                }
                else
                {
                    cell.lblSongs.text = "\(arrAllSearchList[indexPath.row]["no_of_songs"] as! String) Videos"
                }
                
                cell.lblMinute.isHidden = true
                cell.lblSongs.isHidden = false
                
            }else
            {
                if type == "video" {
                    cell.btnPlay.isHidden = false
                }
                
                cell.lblTitle.text = (arrAllSearchList[indexPath.row]["name"] as! String)
                cell.lblSubtitle.text = "by \(arrAllSearchList[indexPath.row]["artist_name"] as! String)"
                
                let num_of_song = arrAllSearchList[indexPath.row]["no_of_songs"] as! String
                
                if num_of_song.isEmpty
                {
                    cell.lblSongs.text = "0 Video"
                }
                else if num_of_song == "1"
                {
                    cell.lblSongs.text = "\(arrAllSearchList[indexPath.row]["no_of_songs"] as! String) Video"
                }
                else
                {
                    cell.lblSongs.text = "\(arrAllSearchList[indexPath.row]["no_of_songs"] as! String) Videos"
                }
                
                cell.lblSongs.isHidden = true
                cell.lblMinute.isHidden = true
            }
        }
        
         tableView.tableFooterView = UIView()
         cell.separatorInset = UIEdgeInsets.zero
         cell.layoutMargins = UIEdgeInsets.zero
         cell.selectionStyle = .none
        
         return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if isRecentSearch
        {
            let type = marrRecentSearch[indexPath.row]["type"] as? String
            
            var Id = String()
            
            Id = marrRecentSearch[indexPath.row]["id"] as! String
            selecet_name = marrRecentSearch[indexPath.row]["name"] as! String
            
            if type == "playlist" {
                
                self.performSegue(withIdentifier: "SearchToPlaylist", sender: Id)
                
            }else if type == "album" {
                
                self.performSegue(withIdentifier: "SearchToAlbum", sender: Id)
                
            }else if type == "video" {
                
                let rowPath = "\(indexPath.row)"
                self.performSegue(withIdentifier: "searchToDetail", sender: rowPath)
                
            }else {
                
                SharedInstance.alertViewController(message: "This content provider section is coming soon", inViewController: self)
                addToRecentSearch(type: "provider", post_id: Id)
            }
        }
        else
        {
            let type = arrAllSearchList[indexPath.row]["type"] as? String
            
            var Id = String()
            
            Id = arrAllSearchList[indexPath.row]["id"] as! String
            selecet_name = arrAllSearchList[indexPath.row]["name"] as! String
            
            if  type == "playlist" {
                
                self.performSegue(withIdentifier: "SearchToPlaylist", sender: Id)
                
            }else if type == "album" {
                
                self.performSegue(withIdentifier: "SearchToAlbum", sender: Id)
                
            }else if type == "video" {
                
                let rowPath = "\(indexPath.row)"
                self.performSegue(withIdentifier: "searchToDetail", sender: rowPath)
                
            }else {
                
                SharedInstance.alertViewController(message: "This content provider section is coming soon", inViewController: self)
                addToRecentSearch(type: "provider", post_id: Id)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame:CGRect( x : 0, y : 0, width : self.tblSearch.frame.size.width,height : 80))
        headerView.backgroundColor = UIColor.white
        
        let headerLabel = UILabel(frame: CGRect( x : 15, y : 30, width : 150,height : 20))
        headerLabel.textColor = UIColor.gray
        headerLabel.font = UIFont.boldSystemFont(ofSize: 17)
        headerLabel.textAlignment = NSTextAlignment.left
        headerLabel.text = "Recently Searched"
        headerView.addSubview(headerLabel)
        
        let btnDelete = UIButton()
        DispatchQueue.main.async {
            btnDelete.frame = CGRect( x : self.tblSearch.frame.size.width-80, y : 30, width : 80,height : 20)
        }
        btnDelete.setTitleColor(UIColor(hex: "3E99B9"), for: .normal)
        btnDelete.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        btnDelete.titleLabel?.textAlignment = .right
        btnDelete.setTitle("Delete all", for: .normal)
        headerView.addSubview(btnDelete)
        
        btnDelete.addTarget(self, action: #selector(deleteRecentSearch), for: .touchUpInside)
        
        let seperator = UILabel(frame: CGRect( x : 0, y : 79, width : self.tblSearch.frame.size.width, height : 1))
        seperator.backgroundColor = UIColor.darkGray
        headerView.addSubview(seperator)
        
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isRecentSearch == true{
            return 80
        }else{
            return 0
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if (segue.identifier == "SearchToPlaylist") {

            let controller =  segue.destination as! VideoViewController
            controller.AlbumId = sender as! String
            controller.screenType = "fromplaylist"
            controller.isFromSearch = "yes"
            controller.screen_title = selecet_name
        }
        else if (segue.identifier == "SearchToAlbum") {

            let controller =  segue.destination as! VideoViewController
            controller.AlbumId = sender as! String
            controller.isFromSearch = "yes"
            controller.screen_title = selecet_name
        }
        else if (segue.identifier == "searchToDetail") {
            
            let controller =  segue.destination as! VideoDetailViewController
            controller.isFromSearch = "yes"
            let item = Int(sender as! String)
            if isRecentSearch
            {
                controller.videodetail = marrRecentSearch[item!]
            }
            else{
            
                controller.videodetail = arrAllSearchList[item!]
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        self.view.endEditing(true)

    }

    //MARK:- UIButton method
    
    //Client side method
    @IBAction func btnVideoAction(_ sender: Any) {
        
        DispatchQueue.main.async {
            
            self.tblSearch.infiniteScrollingView.stopAnimating()
            //self.lblLeading.constant = 0
            self.btnAll.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnVideos.setTitleColor(UIColor.black, for: .normal)
            self.btnAlbums.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnPlaylist.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnProvider.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            
            self.searchType = "video"
            
            self.Search(filterString: self.customSearchBar.text!, type: self.searchType)
            if self.isRecentSearch
            {
                if self.marrRecentSearch.count>0
                {
                    self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
                }
            }
            else
            {
                if self.arrAllSearchList.count>0
                {
                    self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
                }
            }
        }
    }
    
    @IBAction func btnAllAction(_ sender: Any) {
        DispatchQueue.main.async {
            
            self.tblSearch.infiniteScrollingView.stopAnimating()
            //self.lblLeading.constant = 0
            self.btnAll.setTitleColor(UIColor.black, for: .normal)
            self.btnVideos.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnAlbums.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnPlaylist.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnProvider.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)

            self.searchType = "all"

            self.Search(filterString: self.customSearchBar.text!, type: self.searchType)
            if self.isRecentSearch
            {
                if self.marrRecentSearch.count>0
                {
                    self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
                }
            }
            else
            {
                if self.arrAllSearchList.count>0
                {
                    self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
                }
            }
        }
    }
    
    @IBAction func btnPlaylistAction(_ sender: Any) {
        
        DispatchQueue.main.async {
            
            self.tblSearch.infiniteScrollingView.stopAnimating()
            //self.lblLeading.constant = ((self.view.frame.size.width * 0.25))
            self.btnAll.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnVideos.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnAlbums.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnPlaylist.setTitleColor(UIColor.black, for: .normal)
            self.btnProvider.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)

            self.searchType = "playlist"

            self.Search(filterString: self.customSearchBar.text!, type: self.searchType)
            if self.isRecentSearch
            {
                if self.marrRecentSearch.count>0
                {
                    self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
                }
            }
            else
            {
                if self.arrAllSearchList.count>0
                {
                    self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
                }
            }
        }
    }
    
    @IBAction func btnAlbumAction(_ sender: Any) {
        
        DispatchQueue.main.async {
            
            self.tblSearch.infiniteScrollingView.stopAnimating()
           // self.lblLeading.constant = ((self.view.frame.size.width * 0.25)*2)
            self.btnAll.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnVideos.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnAlbums.setTitleColor(UIColor.black, for: .normal)
            self.btnPlaylist.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnProvider.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)

            self.searchType = "album"

            self.Search(filterString: self.customSearchBar.text!, type: self.searchType)
            
            if self.isRecentSearch
            {
                if self.marrRecentSearch.count>0
                {
                     self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
                }
            }
            else
            {
                if self.arrAllSearchList.count>0
                {
                    self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
                }
            }
           
        }
    }
    
    @IBAction func btnProviderAction(_ sender: Any) {
        DispatchQueue.main.async {
            
            self.tblSearch.infiniteScrollingView.stopAnimating()
            //self.lblLeading.constant = ((self.view.frame.size.width * 0.25)*3)
            self.btnAll.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnVideos.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnAlbums.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnPlaylist.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
            self.btnProvider.setTitleColor(UIColor.black, for: .normal)

            self.searchType = "content_provider"

            self.Search(filterString: self.customSearchBar.text!, type: self.searchType)
            if self.isRecentSearch
            {
                if self.marrRecentSearch.count>0
                {
                    self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
                }
            }
            else
            {
                if self.arrAllSearchList.count>0
                {
                    self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
                }
            }
        }
    }
    
    //content provider side method
    @IBAction func btnAllCpAction(_ sender: Any) {
        self.tblSearch.infiniteScrollingView.stopAnimating()
        self.btnAllCp.setTitleColor(UIColor.black, for: .normal)
        self.btnVideoCp.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
        self.btnAlbumcp.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
        self.searchType = "all"
        self.Search_cp(filterString: self.customSearchBar.text!, type: self.searchType)
        if self.isRecentSearch
        {
//            if self.marrRecentSearch.count>0
//            {
//                self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
//            }
        }
        else
        {
            if self.arrAllSearchList.count>0
            {
                self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
            }
        }
    }
    
    @IBAction func btnAlbumCpAction(_ sender: Any) {
        self.tblSearch.infiniteScrollingView.stopAnimating()
        self.btnAllCp.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
        self.btnVideoCp.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
        self.btnAlbumcp.setTitleColor(UIColor.black, for: .normal)
        self.searchType = "album"
        self.Search_cp(filterString: self.customSearchBar.text!, type: self.searchType)
        if self.isRecentSearch
        {
            //            if self.marrRecentSearch.count>0
            //            {
            //                self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
            //            }
        }
        else
        {
            if self.arrAllSearchList.count>0
            {
                self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
            }
        }
    }
    
    @IBAction func btnVideoCpAction(_ sender: Any) {
        self.tblSearch.infiniteScrollingView.stopAnimating()
        self.btnAllCp.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
        self.btnVideoCp.setTitleColor(UIColor.black, for: .normal)
        self.btnAlbumcp.setTitleColor(UIColor(hex: "D2D5D9"), for: .normal)
        self.searchType = "video"
        self.Search_cp(filterString: self.customSearchBar.text!, type: self.searchType)
        if self.isRecentSearch
        {
            //            if self.marrRecentSearch.count>0
            //            {
            //                self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
            //            }
        }
        else
        {
            if self.arrAllSearchList.count>0
            {
                self.tblSearch.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
            }
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        //self.navigationController?.popViewController(animated: true)
        SharedInstance.appDelegate().setSlideRootNavigation()
    }

    //MARK:- Web service called
    @objc func deleteRecentSearch(){
        
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if CheckReachability.isConnectedToNetwork() == true
        {
            let url = "delete_recent_search.php?user_id=\(UserModel.sharedInstance().user_id!)&user_type=\(UserModel.sharedInstance().user_type!)"
            
            Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON { response in
                    
                    // check for errors
                    guard response.result.error == nil else {
                        // got an error in getting the data, need to handle it
                        print("error calling GET delete_recent_search.php")
                        print(response.result.error!)
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        return
                    }
                    
                    // make sure we got some JSON since that's what we expect
                    guard let json = response.result.value as? [String: Any] else {
                        print("didn't get todo object as JSON from API")
                        print("Error: \(String(describing: response.result.error))")
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        return
                    }
                    
                    if let strResult = json["status"] as? String
                    {
                        if strResult == "0"
                        {
                            print(strResult)
                            let strMessage = json["message"] as! String
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        }
                        else
                        {
                            let strMessage = json["message"] as! String
                            print(strMessage)
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                            self.marrRecentSearch.removeAll()
                            self.ImgSearchback.isHidden = false
                            self.tblSearch.isHidden = true
                            
                        }
                        
                        self.tblSearch.reloadData()
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    }
            }
        }
        else
        {
            print("no internet connection")
        }
    }
    
    func addToRecentSearch(type: String, post_id: String){
        
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if CheckReachability.isConnectedToNetwork() == true
        {
            let url = "add_recent_search.php?user_id=\(UserModel.sharedInstance().user_id!)&type=\(type)&type_id=\(post_id)&user_type=\(UserModel.sharedInstance().user_type!)"
            
            Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON { response in
                    
                    // check for errors
                    guard response.result.error == nil else {
                        // got an error in getting the data, need to handle it
                        print("error calling GET add_recent_search.php")
                        print(response.result.error!)
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        return
                    }
                    
                    // make sure we got some JSON since that's what we expect
                    guard let json = response.result.value as? [String: Any] else {
                        print("didn't get todo object as JSON from API")
                        print("Error: \(String(describing: response.result.error))")
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        return
                    }
                    
                    if let strResult = json["status"] as? String
                    {
                        if strResult == "0"
                        {
                            print(strResult)
                            let strMessage = json["message"] as! String
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        }
                        else
                        {
                            let strMessage = json["message"] as! String
                            print(strMessage)
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                            
                        }
                        
                        self.tblSearch.reloadData()
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    }
            }
        }
        else
        {
            print("no internet connection")
        }
    }
    
    func getRecentSearch(start: Int){
        
        if CheckReachability.isConnectedToNetwork() == true
        {
            if start == 0
            {
                marrRecentSearch.removeAll()
                //MBProgressHUD.showAdded(to: self.view, animated: true)
            }
            
            let url = "get_recent_search.php?user_id=\(UserModel.sharedInstance().user_id!)&start=\(start)&user_type=\(UserModel.sharedInstance().user_type!)"
            
            Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON { response in
                    
                    // check for errors
                    guard response.result.error == nil else {
                        // got an error in getting the data, need to handle it
                        print("error calling GET get_recent_search.php")
                        print(response.result.error!)
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        return
                    }
                    
                    // make sure we got some JSON since that's what we expect
                    guard let json = response.result.value as? [String: Any] else {
                        print("didn't get todo object as JSON from API")
                        print("Error: \(String(describing: response.result.error))")
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        return
                    }
                    
                    if let strResult = json["status"] as? String
                    {
                        if strResult == "0"
                        {
                            print(strResult)
                            let strMessage = json["message"] as! String
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        }
                        else
                        {
                            let responseData = json["responseData"] as! [[String:AnyObject]]
                            //self.marrRecentSearch = responseData
                            self.tblSearch.isHidden = false
                            
                            for i in 0..<responseData.count
                            {
                                self.marrRecentSearch.append(responseData[i])
                            }
                            
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                            
                        }
                        
                        if start == 0 && self.marrRecentSearch.count == 0
                        {
                            self.ImgSearchback.isHidden = false
                            self.tblSearch.isHidden = true
                        }
                        
                        self.tblSearch.reloadData()
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    }
            }
        }
        else
        {
            print("no internet connection")
        }
    }
    
    func Search(filterString:String,type:String){
        
        self.tblSearch.infiniteScrollingView.stopAnimating()
        
        let url = "search_content.php?search_text=\(filterString)&type=\(type)&user_id=\(UserModel.sharedInstance().user_id!)"
        
        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET search_content.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                        //SharedInstance.alertViewController(message: strMessage, inViewController: self)
                        self.vwHeader.isHidden = false
                        self.vwHeaderCp.isHidden = true
                        self.arrAllSearchList.removeAll()
                    }
                    else
                    {
                        self.vwHeader.isHidden = false
                        self.vwHeaderCp.isHidden = true
                        let responseData = json["responseData"] as! [[String:AnyObject]]
                        
                        self.arrAllSearchList = responseData
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                    }
                    
                    self.tblSearch.reloadData()
                    
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }
    
    func Search_cp(filterString:String,type:String){
        
        self.tblSearch.infiniteScrollingView.stopAnimating()
        
        let url = "search_content_provider.php?search_text=\(filterString)&type=\(type)&user_id=\(UserModel.sharedInstance().user_id!)"
        
        Alamofire.request(("\(Constant.web_url)\(url)"), method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET search_content.php")
                    print(response.result.error!)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    return
                }
                
                if let strResult = json["status"] as? String
                {
                    if strResult == "0"
                    {
                        print(strResult)
                        let strMessage = json["message"] as! String
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                        //SharedInstance.alertViewController(message: strMessage, inViewController: self)
                        self.vwHeader.isHidden = true
                        self.vwHeaderCp.isHidden = false
                        self.arrAllSearchList.removeAll()
                    }
                    else
                    {
                        self.vwHeader.isHidden = true
                        self.vwHeaderCp.isHidden = false
                        let responseData = json["responseData"] as! [[String:AnyObject]]
                        
                        self.arrAllSearchList = responseData
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
                    }
                    
                    self.tblSearch.reloadData()
                    
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                }
        }
    }
}
